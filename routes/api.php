<?php

use App\Http\Controllers\CoachingController;
use App\Http\Controllers\MMSController;
use App\Http\Controllers\OneDayAccessController;
use App\Http\Controllers\PaypalController;
use App\Http\Controllers\WebhookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Auth::routes(['verify' => true]);

Route::get('/mms', [MMSController::class, 'runSales']);
Route::get('/nurture', [MMSController::class, 'runNewNurtureCampaigns']);
Route::get('/coaching', [CoachingController::class, 'runCron']);
Route::get('/product-funnel', [MMSController::class, 'productLaunch']);
Route::get('/user-emails', [MMSController::class, 'usersEmail']);
Route::post('/paypal', [PaypalController::class, 'handleEvent']);
Route::post('/one-day-access', [OneDayAccessController::class, 'runCron']);
Route::get('/facebook',[WebhookController::class, 'verifyCallback']);
Route::post('/facebook',[WebhookController::class, 'handleFacebookLead']);
Route::get('/test',[MMSController::class, 'test']);
