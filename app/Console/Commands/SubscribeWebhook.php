<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\FacebookService;

class SubscribeWebhook extends Command
{
    protected $signature = 'webhook:subscribe';

    protected $description = 'Subscribe to Facebook leadgen webhook';

    protected FacebookService $facebookService;

    public function __construct(FacebookService $facebookService)
    {
        parent::__construct();
        $this->facebookService = $facebookService;
    }

    public function handle()
    {
        $this->info('Subscribing to Facebook leadgen webhook...');
        $response = $this->facebookService->subscribeToWebhook();
        $this->info('Response: ' . $response);
    }
}

