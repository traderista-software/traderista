<?php

namespace App\Console;

use App\Http\Controllers\CoachingController;
use App\Http\Controllers\MMSController;
use App\Http\Controllers\OneDayAccessController;
use App\Http\Controllers\SingleDayAccessController;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            MMSController::runNewNurtureCampaigns();
        })->dailyAt("09:00:00")->timezone('Europe/Berlin');;

        $schedule->call(function () {
            MMSController::runSales();
        })->dailyAt("09:00:00")->timezone('Europe/Berlin');;

        $schedule->call(function () {
            CoachingController::runCron();
        })->dailyAt("08:00:00")->timezone('Europe/Berlin');

        $schedule->call(function () {
            MMSController::productLaunch();
        })->dailyAt("01:00:00")->timezone('Europe/Berlin');

        $schedule->call(function () {
            MMSController::usersEmail();
        })->dailyAt("09:00:00")->timezone('Europe/Berlin');

        $schedule->call(function () {
            OneDayAccessController::runCron();
        })->cron("*/20 * * * *");
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected
    function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
