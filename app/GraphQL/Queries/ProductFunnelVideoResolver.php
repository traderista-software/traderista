<?php


namespace App\GraphQL\Queries;


use App\Models\ProductFunnelVideo;
use App\Models\UserProductVideo;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class ProductFunnelVideoResolver
{
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $user = $context->request()->user();

        $productVideosAll = ProductFunnelVideo::orderBy('priority', 'ASC')->get();
        $userProductVideos = UserProductVideo::with(["user", "productVideo"])->where(['user_id' => $user->id])
            ->get();

        $productVideosArray = $productVideosAll->toArray();

        foreach ($productVideosArray as $key => $item) {
            $item["unlocked"] = false;
            $productVideosArray[$key] = $item;
        }

        foreach ($productVideosArray as $key => $item) {
            foreach ($userProductVideos as $userProductVideo) {
                if ($item['id'] == $userProductVideo->product_video_id) {
                    $item['unlocked'] = true;
                    $productVideosArray[$key] = $item;
                }
            }
        }

        return $productVideosArray;
    }
}
