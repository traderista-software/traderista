<?php


namespace App\GraphQL\Queries;


use App\Models\UserInboxMessage;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class InboxMessagesResolver
{
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $user = $context->request()->user();

        $inboxMessages = UserInboxMessage::with(['inboxMessage'])->where(['user_id' => $user->id])
            ->get()->toArray();

        foreach ($inboxMessages as $key => $inboxMessage) {
            $inboxMessage['inboxMessage'] = $inboxMessage['inbox_message'];
            unset($inboxMessage['inbox_message']);
            $inboxMessage['inboxMessage']['cTimestamp'] = Carbon::parse($inboxMessage['inboxMessage']['cTimestamp']);
            $inboxMessage['inboxMessage']['mTimestamp'] = Carbon::parse($inboxMessage['inboxMessage']['mTimestamp']);
            $inboxMessages[$key] = $inboxMessage;
        }


        usort($inboxMessages, function ($a, $b) {
            if ($a['inboxMessage']['day'] === $b['inboxMessage']['day']) {
                return $a['inboxMessage']['priority'] > $b['inboxMessage']['priority'];
            }
            return $a['inboxMessage']['day'] > $b['inboxMessage']['day'];
        });
        return $inboxMessages;
    }
}
