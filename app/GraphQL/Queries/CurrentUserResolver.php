<?php


namespace App\GraphQL\Queries;


use App\Models\User;
use App\Utils\UserUtils;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class CurrentUserResolver
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array|Builder|Model
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {

        $user = $context->request()->user();

        $user = User::with(['subscriptions'])
            ->where(['id' => $user->id])
            ->firstOrFail();


        $user = UserUtils::setActiveSubscriptions($user);
        return $user;
    }
}
