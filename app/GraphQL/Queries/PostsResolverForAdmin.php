<?php


namespace App\GraphQL\Queries;

use App\GraphQL\PostObjectCreator;
use App\Models\Comment;
use App\Models\Company;
use App\Models\Trade;
use App\Models\User;
use App\Models\UserTrade;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\ValidationException;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class PostsResolverForAdmin
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws ValidationException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
//        DB::connection()->enableQueryLog();

        $input = collect($args)->toArray();

        $user = $context->request()->user();

        $posts = DB::table('sc_post')
            ->join('users', 'sc_post.user_id', '=', 'users.id')
            ->leftJoin('td_trade', 'td_trade.post_id', '=', 'sc_post.id')
            ->leftJoin('companies', 'td_trade.company_symbol', '=', 'companies.symbol')
            ->select('sc_post.id as postId', 'sc_post.content', 'sc_post.cTimestamp as postCTimestamp', 'sc_post.user_id',
                'sc_post.date_time as post_date_time', 'sc_post.isPinned as isPinned', 'sc_post.allUsers as allUsers', 'td_trade.*', 'companies.*')
            ->orderBy('sc_post.isPinned', 'desc')
            ->orderBy('sc_post.cTimestamp', 'desc')
            ->paginate(25, ['*'], 'page', $input['page']);

        $ids = array();
        foreach ($posts as $post) {
            $ids[] = $post->postId;
        }
        $userTrades = UserTrade::with(['user'])->whereIn('post_id', $ids)->get();
        return PostObjectCreator::createPost($posts, $userTrades);
    }
}
