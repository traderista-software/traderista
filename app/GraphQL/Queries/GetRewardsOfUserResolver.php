<?php


namespace App\GraphQL\Queries;


use App\Models\Reward;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GetRewardsOfUserResolver
{

    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $input = collect($args)->toArray()['id'];

        return Reward::with(['user'])->where(['user_id' => $input])
            ->get();
    }
}
