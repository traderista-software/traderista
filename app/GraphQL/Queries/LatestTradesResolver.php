<?php


namespace App\GraphQL\Queries;

use App\GraphQL\PostObjectCreator;
use App\Models\Comment;
use App\Models\Company;
use App\Models\Trade;
use App\Models\User;
use App\Models\UserSubscription;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\ValidationException;
use Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class LatestTradesResolver
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws ValidationException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
//        DB::connection()->enableQueryLog();

        $user = $context->request()->user();


        if ($user->one_day_access > Carbon::now()->tz('UTC')->toDateTimeString()) {
            $posts = DB::table('sc_post')
                ->join('users', 'sc_post.user_id', '=', 'users.id')
                ->leftJoin('td_trade', 'td_trade.post_id', '=', 'sc_post.id')
                ->leftJoin('companies', 'td_trade.company_symbol', '=', 'companies.symbol')
                ->select('sc_post.id as postId', 'sc_post.content', 'sc_post.cTimestamp as postCTimestamp', 'sc_post.date_time as post_date_time',
                    'sc_post.user_id', 'sc_post.isPinned as isPinned', 'sc_post.allUsers as allUsers',
                    'td_trade.*', 'companies.*')
                ->orderBy('sc_post.isPinned', 'desc')
                ->orderBy('sc_post.cTimestamp', 'desc')
                ->get();

        } else {


            $activeSubscriptions = UserSubscription::where('user_id', $user->id)
                ->where(
                    function ($q) {
                        $q->where('status', 'CONFIRMED')
                            ->orWhere([
                                ['status', 'CANCELED'],
                                ['expiryDate', '>=', Carbon::today()->toDateString()]]);
                    })->get();

            $stockTypes = array();
            $tradeType = array();


            if (isset($activeSubscriptions) && sizeof($activeSubscriptions) > 0) {
                for ($i = 0; $i < sizeof($activeSubscriptions); $i++) {
                    $package = $activeSubscriptions[$i]->package;
                    if ($package->type === 'ALL_INCLUSIVE') {
                        array_push($tradeType, 'DAILY');
                        array_push($tradeType, 'WEEKLY');
                    } else {
                        array_push($tradeType, $package->type);
                    }

                    if ($package->stocksSupported === 'ALL') {
                        array_push($stockTypes, 'US');
                        array_push($stockTypes, 'DEUTSCHE');
                        array_push($stockTypes, 'INDEX');
                    } else {
                        array_push($stockTypes, $package->stocksSupported);
                    }
                }
            }


            $posts = DB::table('sc_post')
                ->join('users', 'sc_post.user_id', '=', 'users.id')
                ->leftJoin('td_trade', 'td_trade.post_id', '=', 'sc_post.id')
                ->leftJoin('companies', 'td_trade.company_symbol', '=', 'companies.symbol')
                ->select('sc_post.id as postId', 'sc_post.content', 'sc_post.cTimestamp as postCTimestamp', 'sc_post.date_time as post_date_time',
                    'sc_post.user_id', 'sc_post.isPinned as isPinned', 'sc_post.allUsers as allUsers',
                    'td_trade.*', 'companies.*')
                ->whereIn('sc_post.tradeType', $tradeType)
                ->whereIn('sc_post.stockType', $stockTypes)
                ->orWhere('allUsers', true)
                //                ->where(function ($query) use ($user) {
//                    $query->whereNotExists(function ($query) use ($user) {
//                        $query->select(DB::raw(1))
//                            ->from('sc_user_post_seen')
//                            ->whereColumn('sc_user_post_seen.post_id', 'sc_post.id')
//                            ->where('sc_user_post_seen.user_id', $user->id);
//                    })->orWhere('td_trade.cTimestamp', '<', Carbon::today()->addDays(2)->toDateString());
//                })
                ->orderBy('sc_post.isPinned', 'desc')
                ->orderBy('sc_post.cTimestamp', 'desc')
                ->get();

        }

        if ($posts->isEmpty()) {
            return [];
        }

        return PostObjectCreator::createPost($posts);
    }
}
