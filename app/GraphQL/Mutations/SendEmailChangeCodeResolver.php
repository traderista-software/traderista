<?php


namespace App\GraphQL\Mutations;


use App\Models\CashOutRequest;
use App\Models\ChangeEmailRequest;
use App\Models\User;
use App\Services\MailService;
use GraphQL\Type\Definition\ResolveInfo;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\ValidationException;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class SendEmailChangeCodeResolver
{

    private MailService $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $user = $context->request()->user();
        $email = collect($args)->toArray()['email'];

        $exists = User::where(['email' => $email])->exists();

        if ($exists) {
            throw new ValidationException([
                'status' => 'EXISTS',
            ], 'Validation Error');
        }

        $digits = 4;
        $code = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

        ChangeEmailRequest::where(['user_id' => $user->id])->delete();

        ChangeEmailRequest::create([
            'user_id' => $user->id,
            'email' => $email,
            'verification_code' => $code
        ]);
        $unsubscribeUrl = env('APP_URL') . '/emails/unsubscribe?token=' . $user->emailToken;
        $this->mailService->sendEmailChangeEmail($email, $code, $unsubscribeUrl);

        return [
            'status' => 'CODE_SENT',
            'message' => 'Code sent to your email',
        ];
    }
}
