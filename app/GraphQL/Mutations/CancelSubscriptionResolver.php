<?php


namespace App\GraphQL\Mutations;


use App\Models\User;
use App\Models\UserSubscription;
use App\Utils\UserUtils;
use GraphQL\Type\Definition\ResolveInfo;
use Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Srmklive\PayPal\Services\PayPal as PayPalClient;
use Throwable;

class CancelSubscriptionResolver
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @throws Throwable
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $user = $context->request()->user();
        $id = collect($args)->toArray()['input'];

        // CREATE PAYPAL CLIENT
        $provider = new PayPalClient;
        $provider->setApiCredentials(config('paypal'));
        $provider->setCurrency('EUR');
        $provider->getAccessToken();

        // CANCEL EXISTING SUBSCRIPTION
        $activeSubscription = UserSubscription::whereId($id)->first();

        Log::info($id);

        if (isset($activeSubscription)) {
            $createSubscriptionResponse = $provider->cancelSubscription($activeSubscription->paypal_subscription_id, "Subscription was cancelled");

            UserSubscription::whereId($activeSubscription->id)
                ->update([
                    'status' => 'CANCELED',
                    'expiryDate' => $activeSubscription->calculateExpiryDateFromLastPayment(),
                ]);
        }

        // SET THE NEW ACTIVE SUBSCRIPTION AND UPDATE MIN CAPITAL
//        User::whereId($user->id)
//            ->update([
//                'active_subscription_id' => null,
//            ]);

        $user = User::with(['subscriptions'])->where('id', $user->id)->first();
        $user = UserUtils::setActiveSubscriptions($user);
        return $user;
    }
}
