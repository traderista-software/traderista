<?php


namespace App\GraphQL\Mutations;

use App\Utils\RequestUtils;
use Carbon\Carbon;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\ValidationException;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class VerifyPhoneNumberResolver
{
    private RequestUtils $requestUtils;

    public function __construct(RequestUtils $requestUtils)
    {
        $this->requestUtils = $requestUtils;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws ValidationException
     * @throws Exception
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $user = $this->requestUtils->getUserDetails();
        $code = collect($args)->toArray()['code'];


        if (!((string)$user->phone_number_code === (string)$code)) {
            throw new ValidationException([
                'code' => __('The code is invalid'),
            ], 'Validation Error');
        }

        $user->phone_number_verified_at = Carbon::now();
        $user->phone_number_code = null;
        $user->save();

        return [
            'status' => 'PHONE_NUMBER_VERIFIED',
            'phone_number' => $user->phone_number,
        ];
    }
}
