<?php

namespace App\GraphQL\Mutations;

use App\Models\Lead;
use App\Services\MailService;
use App\Utils\TokenGenerator;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class SendMailForOneDayAccessResolver
{

    private MailService $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {


        $email = collect($args)->toArray()['input'];

        $lead = Lead::whereEmail($email)->first();

        if (!$lead) {
            $id = Lead::insertGetId(['email' => $email, 'type' => 'SUBSCRIPTION', 'token' => TokenGenerator::generateToken()]);
            $lead = Lead::whereId($id)->first();
        }

        $registerUrl = env('APP_URL') . '/auth/sign-up?oneDayTrial=true';

        $list = array();
        $list[] = [$lead->email, $lead->cTimestamp, $registerUrl . '&email=' . $lead->email . '&token=' . $lead->token];

        $this->mailService->sendOneDayAccessEmail($list);

        return [
            'status' => 'SENT',
            'message' => 'One day free trial sent successfully'
        ];
    }
}
