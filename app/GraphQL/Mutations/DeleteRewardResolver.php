<?php


namespace App\GraphQL\Mutations;


use App\Models\Reward;
use App\Models\User;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class DeleteRewardResolver
{

    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $id = collect($args)->toArray()['id'];

        $reward = Reward::with(['user'])->whereId($id)->first();

        $oldCoinAmount = $reward->coin_amount;
        $userId = $reward->user->id;

        $user = User::whereId($userId)->first();
        $user->coin_amount -= $oldCoinAmount;
        $user->coins_fiat_amount -= $oldCoinAmount;
        $user->save();

        Reward::whereId($id)->delete();

        return ['status' => 'SUCCESS'];
    }

}
