<?php


namespace App\GraphQL\Mutations;

use App\Models\Email;
use App\Models\InboxMessage;
use App\Models\Lead;
use App\Models\User;
use App\Models\UserInboxMessage;
use App\Models\UserReferral;
use App\Services\MailService;
use App\Services\UserService;
use App\Utils\TokenGenerator;
use Carbon\Carbon;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Joselfonseca\LighthouseGraphQLPassport\GraphQL\Mutations\BaseAuthResolver;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use RuntimeException;

class RegisterResolver extends BaseAuthResolver
{
    private UserService $userService;

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws Exception
     *
     */

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }


    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $model = $this->createAuthModel($args);

        $this->validateAuthModel($model);

//        if ($model instanceof \App\Mail\MustVerifyEmail) {
//            $model->sendEmailVerificationNotification();
//            event(new Registered($model));
//        }

//            return [
//                'tokens' => [],
//                'status' => 'MUST_VERIFY_EMAIL',
//            ];
//        }
        $credentials = $this->buildCredentials([
            'username' => $args[config('lighthouse-graphql-passport.username')],
            'password' => $args['password'],
            'scope' => 'user'
        ]);
        $user = $model->where(config('lighthouse-graphql-passport.username'), $args[config('lighthouse-graphql-passport.username')])->first();
        $response = $this->makeRequest($credentials);
        $response['user'] = $user;
        event(new Registered($user));

        $token = TokenGenerator::generateToken();
        User::whereId($user->id)->update(['emailToken' => $token]);

        try {
            $emailsToSend = array();
            $emails = Email::where(['timePeriodInDays' => 0, 'type' => 'USER'])->get();
            foreach ($emails as $email) {
                $emailsToSend[] = [
                    'email' => $model->email,
                    'emailObj' => $email,
                    'name' => $model->name,
                    'unsubscribeUrl' => env('APP_URL') . '/emails/unsubscribe?token=' . $token
                ];
            }
            $mailService = new MailService();
            $mailService->sendMarketingEmail($emailsToSend);
        } catch (Exception $exception) {
            \Log::error($exception);
        }


        return [
            'tokens' => $response,
            'status' => 'SUCCESS',
        ];
    }

    protected function validateAuthModel($model): void
    {
        $authModelClass = $this->getAuthModelFactory()->getClass();

        if ($model instanceof $authModelClass) {
            return;
        }

        throw new RuntimeException("Auth model must be an instance of {$authModelClass}");
    }

    protected function createAuthModel(array $data): Model
    {
        $input = collect($data)->except('password_confirmation')->toArray();
        $input['password'] = Hash::make($input['password']);

        $str_result = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz';
        $referral_code = 'TRADERISTA-' . substr(str_shuffle($str_result), 0, 4);

        $input['referral_code'] = $referral_code;

        $model = $this->getAuthModelFactory()->create($input);

        $referrer = User::where('referral_code', $input['friend_referral_code'])->first();

        $messages = InboxMessage::where('day', 0)->get();
        foreach ($messages as $message) {
            UserInboxMessage::insert([
                'user_id' => $model->id,
                'message_id' => $message->id,
                'status' => 'UNREAD',
            ]);
        }

        $this->userService->applyOneDayAccess($input, $model->id);

//        if ($referrer !== null) {
//            UserReferral::insert(
//                [
//                    'referrer_id' => $referrer->id,
//                    'invited_user_id' => $model->id,
//                    'coin_reward' => 0,
//                    'fiat_reward' => 0,
//                    'stage' => 1,
//                ]
//            );
//        }
        return $model;
    }
}
