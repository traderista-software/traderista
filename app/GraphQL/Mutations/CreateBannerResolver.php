<?php


namespace App\GraphQL\Mutations;


use App\Models\Banner;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class CreateBannerResolver
{
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $input = collect($args)->toArray();

        $isVideo = $input['isVideo'];

        Log::info($input);
        if ($isVideo == true) {
            $url = $input['url'];
        } else {
            $image64 = $input['image'];
            @list($type, $file_data) = explode(';', $image64);
            @list(, $file_data) = explode(',', $file_data);
            $imageName = Str::random(32) . '.' . 'png';
            Storage::put('public/' . $imageName, base64_decode($file_data));
            $url = env('APP_URL') . '/storage/' . $imageName;
        }

        $status = $input['status'];
        $link = $input['link'];
        $priority = $input['priority'];


        $id = Banner::insertGetId([
            'status' => $status,
            'url' => $url,
            'isVideo' => $isVideo,
            'priority' => $priority,
            'link' => $link
        ]);
        return Banner::findOrFail($id);
    }
}
