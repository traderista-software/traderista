<?php


namespace App\GraphQL\Mutations;


use App\Models\User;
use App\Services\MailService;
use GraphQL\Type\Definition\ResolveInfo;
use Joselfonseca\LighthouseGraphQLPassport\GraphQL\Mutations\BaseAuthResolver;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class SendCoachingQuestionEmail extends BaseAuthResolver
{
    private MailService $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $user = $context->request()->user();

        $input = collect($args)->toArray()['input'];
        $title = $input['title'];
        $content = $input['content'];

        $user = User::find($user->id);
        $fullName = $user->name . ' ' . $user->surname;

        $this->mailService->sendCoachingQuestionEmail(
            'member@traderista-trading.de',
            $title,
            $fullName,
            $user->email,
            $content);

        return [
            'status' => 'CODE_SENT',
            'message' => 'Email sent.',
        ];
    }
}
