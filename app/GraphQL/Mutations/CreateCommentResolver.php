<?php


namespace App\GraphQL\Mutations;


use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Services\MailService;
use App\Services\TwilioService;
use App\Services\UserService;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\ValidationException;
use Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;

class CreateCommentResolver
{
    private TwilioService $twilioService;
    private MailService $mailService;

    public function __construct(TwilioService $twilioService, MailService $mailService)
    {
        $this->twilioService = $twilioService;
        $this->mailService = $mailService;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws ValidationException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
//        DB::connection()->enableQueryLog();

        $user = $context->request()->user();
        $input = collect($args)->toArray();

        $id = Comment::insertGetId([
            'content' => $input['content'],
            'date_time' => $input['date_time'],
            'user_id' => $user->id,
            'post_id' => $input['post']['connect']
        ]);

        // notify
        $relatedComments = Comment::where(['post_id' => $input['post']['connect']])->get();
        foreach ($relatedComments as $relatedComment) {
            $relatedUser = User::whereId($relatedComment->user_id)->first();

            if (isset($relatedUser) && $relatedUser->id !== $user->id) {
                if ($user->emailNotification === 1 && $user->email_verified_at !== null) {
                    $unsubscribeUrl = env('APP_URL') . '/emails/unsubscribe?token=' . $user->emailToken;
                    $this->mailService->sendCreateCommentEmail($relatedUser->email, $unsubscribeUrl);
                }

//                if ($user->smsNotification === 1 && $user->phone_number_verified_at !== null) {
//                    try {
//                        $this->twilioService->sendMessage($relatedUser->phone_number,
//                            'Ein neuer Kommentar wurde in einem Beitrag erstellt, den Sie kommentiert haben. Sehen Sie es sich hier an: ' .
//                            env('API_URL') . '/app/posts');
//                    } catch (ConfigurationException | TwilioException $e) {
//                        Log::error($e->getCode());
//                    }
//                }
            }
        }

        // notify admins
        $admins = User::where(['role' => 'admin'])->get();
        foreach ($admins as $admin) {
            $unsubscribeUrl = env('APP_URL') . '/emails/unsubscribe?token=' . $admin->emailToken;
            $this->mailService->sendCreateCommentEmail($admin->email, $unsubscribeUrl);
//            try {
//                $this->twilioService->sendMessage($admin->phone_number,
//                    'Ein neuer Kommentar wurde in einem Beitrag erstellt, den Sie kommentiert haben.' .
//                    env('API_URL') . '/admin/posts');
//            } catch (ConfigurationException | TwilioException $e) {
//                Log::error($e->getCode());
//            }
        }

        $comment = DB::table('comments')
            ->where(['id' => $id])
            ->first();

        $comment->cTimestamp = date("d M Y, H:i", strtotime($comment->cTimestamp));
        $comment->date_time = date("d M Y, H:i", strtotime($comment->date_time));
        $comment->author = ['id' => $user->id, 'name' => $user->name, 'surname' => $user->surname];

        return json_decode(json_encode($comment), true);
    }
}
