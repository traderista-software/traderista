<?php


namespace App\GraphQL\Mutations;


use App\Models\UserInboxMessage;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\DB;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class UpdateUserInboxMessageStatus
{
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = collect($args)->toArray();
        $user_id = $input['input']['user_id'];
        $message_id = $input['input']['message_id'];
        $status = $input['input']['status'];

        $userInboxMessage = UserInboxMessage::with(['user', 'inboxMessage'])->where(['user_id' => $user_id], ['message_id' => $message_id])->first();

        $userInboxMessage->status = $status;

        DB::table('mk_user_inbox_message')
            ->where(['user_id' => $user_id])
            ->where(['message_id' => $message_id])
            ->update(['status' => $status]);


        $array = $userInboxMessage->toArray();

        $array['inboxMessage'] = $array['inbox_message'];
        return $array;
    }
}
