<?php


namespace App\GraphQL\Mutations;


use App\Models\Lead;
use App\Services\MailService;
use App\Utils\TokenGenerator;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class GenerateQRCodeResolver
{

    private MailService $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }


    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $user = $context->request()->user();

        $email = $user->email;

        $referralCode = $user->referral_code;

        $urlHome = 'https://traderista-trading.de?referral_code=' . $referralCode;
        $url5MinuteWebinar = 'https://traderista-trading.de/webinar?referral_code=' . $referralCode;
        $urlFunnelLink = 'https://traderista-trading.de/survey?referral_code=' . $referralCode;
        $urlRegister = 'https://traderista-trading.de/auth/sign-up?referral_code=' . $referralCode;

        $qrCodes = array();

        $qrCodeHome = QrCode::format('png')->size(400)->generate($urlHome);
        $qrCodeWebinar = QrCode::format('png')->size(400)->generate($url5MinuteWebinar);
        $qrCodeFunnel = QrCode::format('png')->size(400)->generate($urlFunnelLink);
        $qrCodeRegister = QrCode::format('png')->size(400)->generate($urlRegister);

        $qrCodes['home'] = $qrCodeHome;
        $qrCodes['webinar'] = $qrCodeWebinar;
        $qrCodes['funnel'] = $qrCodeFunnel;
        $qrCodes['register'] = $qrCodeRegister;

        $unsubscribeUrl = env('APP_URL') . '/emails/unsubscribe?token=' . $user->emailToken;
        $this->mailService->sendQRCodeEmail($email, $qrCodes, $unsubscribeUrl);
        return [
            'status' => 'SENT'
        ];
    }
}
