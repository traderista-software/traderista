<?php


namespace App\GraphQL\Mutations;


use App\Models\ChangeEmailRequest;
use App\Models\User;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Auth\Events\Verified;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Carbon;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\ValidationException;
use Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class ChangeEmailResolver
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws Exception
     *
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $user = $context->request()->user();
        $input = collect($args)->toArray()['input'];

        $request = ChangeEmailRequest::where(['user_id' => $user->id])->firstOrFail();

        Log::info($input);

        $result = false;
        if ($request->email === $input['email'] && $request->verification_code === $input['code']) {
            $user->email = $request->email;
            $user->email_verified_at = now();
            $user->save();

            $result = true;
        }

        if (!$result) {
            throw new ValidationException([
                'status' => 'NOT_VERIFIED',
            ], 'Validation Error');
        }

        ChangeEmailRequest::where(['user_id' => $user->id])->delete();

        return ['status' => 'VERIFIED', 'message' => 'Email was verified!'];
    }
}
