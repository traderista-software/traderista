<?php


namespace App\GraphQL\Mutations;


use App\Models\Banner;
use App\Models\User;
use App\Services\MailService;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class UpdateIndividualSupportResolver
{
    private MailService $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $input = collect($args)->toArray();

        $id = $input['id'];

        User::whereId($id)->update(['individualSupport' => true]);
        $user = User::findOrFail($id);

        try {
            $this->mailService->sendIndividualSupportMail(env('ADMIN_EMAIL'), $user->email);
        } catch (\Exception $exception) {
            Log::error($exception);
        }
        return $user;
    }
}
