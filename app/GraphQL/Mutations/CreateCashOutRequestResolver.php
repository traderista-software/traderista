<?php


namespace App\GraphQL\Mutations;


use App\Auth\User;
use App\Models\CashOutRequest;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\ValidationException;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class CreateCashOutRequestResolver
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws ValidationException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $user = $context->request()->user();
        $input = collect($args)->toArray()['paypal_email'];

        if($user->coin_amount === 0) {
            throw new ValidationException([
                'status' => 'NO_BALANCE',
            ], 'Validation Error');
        }

        $hasActiveCashOutRequest = CashOutRequest::where([
            'user_id' => $user->id,
        ])->whereNull('approvedTimestamp')->exists();

        if($hasActiveCashOutRequest) {
            throw new ValidationException([
                'status' => 'HAS_ACTIVE_REQUEST',
            ], 'Validation Error');
        }

        $model = CashOutRequest::insert([
            'user_id' => $user->id,
            'paypal_email' => $input,
            'coin_amount' => $user->coin_amount,
            'fiat_amount' => $user->coins_fiat_amount,
        ]);

        \App\Models\User::whereId($user->id)->update(['paypal_email' => $input]);

        return [
            'status' => 'SENT',
        ];
    }
}
