<?php

namespace App\GraphQL\Mutations;

use App\Models\Lead;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class LeadUpdateResolver
{

    public function __construct()
    {
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws Exception
     *
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = collect($args)->toArray();

        $email = $input['email'];
        $surveyResults = $input['surveyResults'];
        $recommendedPackageId = $input['recommendedPackageId'];

        $lead = Lead::where(['email' => $email])->orderBy('cTimestamp', 'DESC')->firstOrFail();

        Log::info($lead->id);

        $lead = Lead::where(['id' => $lead->id])->update(
            ['email' => $email, 'surveyResults' => $surveyResults, 'recommendedPackageId' => $recommendedPackageId]
        );

        return [
            'status' => 'UPDATE_SUCCESS',
            'message' => 'LEAD UPDATED SUCCESSFULLY'
        ];
    }
}
