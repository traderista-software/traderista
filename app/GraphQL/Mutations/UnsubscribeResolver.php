<?php

namespace App\GraphQL\Mutations;

use App\Models\FormMessage;
use App\Models\Lead;
use App\Models\User;
use App\Services\MailService;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class UnsubscribeResolver
{

    private MailService $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }


    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = collect($args)->toArray();

        $user = User::where('emailToken', $input['token'])->first();
        if ($user) {
            User::whereId($user->id)->update(['emailNotification' => false]);
            Log::info($user);
            $this->mailService->sendUnsubscribeMailConfirmation($user->email);
        } else {
            $lead = Lead::where('token', $input['token'])->firstOrFail();
            if ($lead) {
                Log::info($lead);
                Lead::whereId($lead->id)->update(['has_unsubscribed' => true]);
                $this->mailService->sendUnsubscribeMailConfirmation($lead->email);
            }
        }

        return [
            'status' => 'SUCCESS',
            'message' => 'Unsubscribed'
        ];
    }
}
