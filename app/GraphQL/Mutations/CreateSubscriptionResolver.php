<?php


namespace App\GraphQL\Mutations;


use App\Models\Package;
use App\Models\User;
use App\Models\UserSubscription;
use App\Services\PackageService;
use App\Services\UserService;
use App\Utils\UserUtils;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Srmklive\PayPal\Services\PayPal as PayPalClient;
use Throwable;

class CreateSubscriptionResolver
{
    private PackageService $packageService;

    public function __construct(PackageService $packageService)
    {
        $this->packageService = $packageService;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @throws Throwable
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $user = $context->request()->user();
        $input = collect($args)->toArray();

        $package = Package::whereId($input["id"])->first();
        $packagePrice = $this->packageService->getPackagePrice($package, $input['type']);

        $subscriptions = UserSubscription::where('user_id', $user->id)->get();
        $hasSubscriptionHistory = (isset($subscriptions) && count($subscriptions) > 0);

        $pricePaid = $hasSubscriptionHistory || $input['type'] === "ANNUAL" ? $packagePrice : $package->testPhasePrice;

        UserSubscription::create([
            'user_id' => $user->id,
            'package_id' => $package->id,
            'paypal_subscription_id' => $input['subscription_id'],
            'duration' => $input['type'],
            'price' => $pricePaid,
            'lastPaymentDate' => Carbon::now(),
        ]);

        $user = User::with(['subscriptions'])->where('id', $user->id)->first();
        return UserUtils::setActiveSubscriptions($user);
    }
}
