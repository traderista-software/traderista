<?php


namespace App\GraphQL\Mutations;


use App\Models\UserPostSeen;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class UserSeePostResolver
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $user = $context->request()->user();
        $input = collect($args)->toArray();

        $postId = $input['post']['connect'];
        $userId = $input['user']['connect'];

        $userPostSeen = UserPostSeen::where(['user_id' => $userId, 'post_id' => $postId])->first();

        if ($userPostSeen !== null) {
            ++$userPostSeen->counter;
        } else {
            $userPostSeen = new UserPostSeen();
            $userPostSeen->user_id = $userId;
            $userPostSeen->post_id = $postId;
            $userPostSeen->counter = 1;
        }

        $userPostSeen->save();

        return ['counter' => $userPostSeen->counter];

    }
}
