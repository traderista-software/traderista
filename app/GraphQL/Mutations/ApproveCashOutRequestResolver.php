<?php


namespace App\GraphQL\Mutations;


use App\Models\CashOutRequest;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\ValidationException;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class ApproveCashOutRequestResolver
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws ValidationException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = collect($args)->toArray()['id'];

        $model = CashOutRequest::find($input);
        $model->approvedTimestamp = Carbon::now();
        $model->save();

        $user = $model->user;
        $user->coin_amount -= $model->coin_amount;
        $user->coins_fiat_amount -= $model->fiat_amount;
        $user->save();

        return [
            'status' => 'SUCCESS',
        ];
    }
}
