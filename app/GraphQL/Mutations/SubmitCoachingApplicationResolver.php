<?php


namespace App\GraphQL\Mutations;


use App\Models\CashOutRequest;
use App\Models\FormMessage;
use App\Services\MailService;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\ValidationException;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class SubmitCoachingApplicationResolver
{

    private MailService $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws ValidationException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = collect($args)->toArray();

        FormMessage::insert([
            'name' => $input['name'],
            'email' => $input['email'],
            'message' => $input['message'],
            'phone_number' => $input['phone_number'] ?? null,
            'type' => $input['type']
        ]);

        $this->mailService->sendEmailCoachingApplication($input['name'], $input['email'], $input['phone_number'] ?? null, $input['message'], $input['type']);

        return [
            'status' => 'SUCCESS',
            'message' => 'Message was saved.'
        ];
    }
}
