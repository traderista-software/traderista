<?php

namespace App\GraphQL\Mutations;

use App\Models\CashOutRequest;
use App\Models\User;
use App\Services\UserService;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class ActivateOneDayAccessForUser
{

    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = collect($args)->toArray()['input'];

        $user = User::find($input['id']);


        if ($input['activate_one_day_access'] === true && $user->one_day_access === null) {
            if ($user->emailToken === $input['token']) {
                User::where(['id' => $input['id']])->update(
                    ['one_day_access' => Carbon::now()->addHours(24)->tz('UTC')->toDateTimeString()]
                );
            }
        }

        return [
            'status' => 'SUCCESS',
        ];
    }
}
