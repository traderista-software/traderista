<?php

namespace App\GraphQL\Mutations;


use App\Models\InboxMessage;
use App\Models\UserInboxMessage;
use App\Models\UserPostSeen;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class DeleteMessageResolver
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = collect($args)->toArray();

        $id = $input['id'];

        UserInboxMessage::where('message_id', $id)->delete();

        InboxMessage::where('id', $id)->delete();

        return [
            'id' => $id
        ];
    }
}
