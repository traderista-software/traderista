<?php


namespace App\GraphQL\Mutations;


use App\Models\Banner;
use App\Models\User;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class UpdateBannerResolver
{
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $input = collect($args)->toArray();

        $id = $input['id'];

        $isVideo = $input['isVideo'];

        if ($isVideo === true) {
            $url = $input['url'];
        } else {

            $image64 = $input['image'];

            @list($type, $file_data) = explode(';', $image64);
            @list(, $file_data) = explode(',', $file_data);
            $imageName = Str::random(32) . '.' . 'png';

            Storage::put('public/' . $imageName, base64_decode($file_data));

            $url = env('APP_URL') . '/storage/' . $imageName;
        }
        $status = $input['status'];
        $priority = $input['priority'];
        $link = $input['link'];

        Banner::whereId($id)->update(['status' => $status, 'isVideo' => $isVideo, 'url' => $url, 'link' => $link, 'priority' => $priority]);
        return Banner::findOrFail($id);
    }
}
