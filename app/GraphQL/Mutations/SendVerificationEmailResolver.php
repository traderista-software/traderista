<?php


namespace App\GraphQL\Mutations;


use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Joselfonseca\LighthouseGraphQLPassport\GraphQL\Mutations\BaseAuthResolver;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Models\User;
use RuntimeException;

class SendVerificationEmailResolver extends BaseAuthResolver
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws Exception
     *
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = collect($args)->toArray()['email'];

        $model = User::where([
            ['email', $input],
            ['email_verified_at', null]
        ])->firstOrFail();

        $this->validateAuthModel($model);

        if ($model instanceof MustVerifyEmail) {
            $model->sendEmailVerificationNotification();

            return [
                'status' => 'EMAIL_SENT',
            ];
        }
        return [
            'status' => 'EMAIL_NOT_SENT',
        ];
    }

    protected function validateAuthModel($model): void
    {
        $authModelClass = $this->getAuthModelFactory()->getClass();

        if ($model instanceof $authModelClass) {
            return;
        }

        throw new RuntimeException("Auth model must be an instance of {$authModelClass}");
    }
}
