<?php

namespace App\GraphQL\Mutations;

use App\Models\UserTrade;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class EnterTradeResolver
{
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $input = collect($args)->toArray();

        $user_id = $input['user_id'];
        $post_id = $input['post_id'];

        UserTrade::insertOrIgnore([
            'user_id' => $user_id,
            'post_id' => $post_id,
        ]);
        return UserTrade::where(['user_id' => $user_id, 'post_id' => $post_id])->get()->first();
    }
}
