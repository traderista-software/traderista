<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use App\Utils\TokenGenerator;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GenerateEmailTokenResolver
{
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
        $users = User::whereNull('emailToken')->get();

        foreach ($users as $user) {
            $token = TokenGenerator::generateToken();
            User::whereId($user->id)->update(['emailToken' => $token]);
        }
        return [];
    }
}
