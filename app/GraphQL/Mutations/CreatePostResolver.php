<?php


namespace App\GraphQL\Mutations;


use App\Models\Post;
use App\Models\Trade;
use App\Services\MailService;
use App\Services\TwilioService;
use App\Services\UserService;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use JsonException;
use Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;

class CreatePostResolver
{
    private TwilioService $twilioService;
    private MailService $mailService;
    private UserService $userService;

    public function __construct(TwilioService $twilioService, MailService $mailService, UserService $userService)
    {
        $this->twilioService = $twilioService;
        $this->mailService = $mailService;
        $this->userService = $userService;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array|Builder|Model|object
     * @throws JsonException
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {
//        DB::connection()->enableQueryLog();

        $user = $context->request()->user();
        $input = collect($args)->toArray();

        $ids = array();

        foreach ($input['input'] as $post) {
            $postId = Post::insertGetId([
                'user_id' => $user->id,
                'content' => $post['content'],
                'date_time' => $post['date_time'],
                'tradeType' => $post['tradeType'] ?? null,
                'allUsers' => $post['allUsers'] ?? false,
                'isPinned' => $post['isPinned'] ?? false,
                'stockType' => $post['stockType'] ?? null,
            ]);

            $tradeId = null;

            if (isset($post['trade']) && $post['trade']['position'] != null) {
                $tradeInput = $post['trade'];
                $tradeId = Trade::insertGetId([
                    'post_id' => $postId,
                    'company_symbol' => $tradeInput['company']['symbol'],
                    'position' => $tradeInput['position'],
                    'entryDate' => $tradeInput['entryDate'],
                    'tradeType' => $tradeInput['tradeType'],
                    'stockType' => $tradeInput['stockType'],
                    'leverage' => $tradeInput['leverage'],
                    'price' => $tradeInput['price'],
                    'amountPercentage' => $tradeInput['amountPercentage'],
                    'stopLoss' => $tradeInput['stopLoss'],
                    'takeProfit' => $tradeInput['takeProfit'],
                    'status' => 'OPEN',
                ]);
            }

            $ids[] = $postId;
        }

        $posts = Post::with(['author', 'trade'])->whereIn('id', $ids)->get();
        Log::info($posts);


//        $post = Post::with(['author'])->where(['id' => $postId])->first();
//
//        if ($tradeId !== null) {
//            $trade = Trade::with(['company'])->where(['id' => $tradeId])->first();
//            $post->trade = $trade;
//        }
//
        $users = $this->userService->getSubscribingUsersWithPackages();

        foreach ($users as $user) {
            $userTradeType = $user->type;
            $userStockSupported = $user->stocksSupported;
            $count = 0;
            foreach ($posts as $post) {
                if ($post->trade != null) {
                    if (($userTradeType === $post->trade->tradeType && $userStockSupported === $post->trade->stockType) ||
                        ($userTradeType === $post->trade->tradeType && $userStockSupported === 'ALL') ||
                        ($userTradeType === 'ALL_INCLUSIVE' && $userStockSupported === 'ALL')) {
                        $count = $count + 1;
                    }
                }
            }

            $message = $count == 1 ? 'Ein neuer Beitrag wurde in Traderista erstellt. Sehen Sie es sich hier an: ' . env('API_URL') . '/app/posts' :
                $count . ' neue Beiträge wurden in Traderista erstellt. Sehen Sie es sich hier an: ' . env('API_URL') . '/app/posts';

            if ($user->emailNotification === 1 && $count > 0) {

                $unsubscribeUrl = env('APP_URL') . '/emails/unsubscribe?token=' . $user->emailToken;
                $this->mailService->sendCreatePostEmail($user->email, $message, $unsubscribeUrl);
            }
            if ($user->smsNotification === 1 && $user->phone_number_verified_at !== null && $count > 0) {
                try {
                    $this->twilioService->sendMessage($user->phone_number, $message);
                } catch (ConfigurationException|TwilioException $e) {
                    Log::error($e->getCode());
                }
            }
        }

        return $posts;
    }
}
