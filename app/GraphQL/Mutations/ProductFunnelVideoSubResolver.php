<?php


namespace App\GraphQL\Mutations;

use App\Models\ProductFunnelVideo;
use App\Models\User;
use App\Models\UserProductVideo;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class ProductFunnelVideoSubResolver
{
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo)
    {

        $user = $context->request()->user();
        $input = collect($args)->toArray();

        User::whereId($user->id)
            ->update(['productLaunchSubscription' => true]);

        $user_id = $input['user_id'];
        $video_id = $input['product_video_id'];

        UserProductVideo::insert([
            'user_id' => $user_id,
            'product_video_id' => $video_id
        ]);

        $productVideosAll = ProductFunnelVideo::orderBy('priority', 'ASC')->get();
        $userProductVideos = UserProductVideo::with(["user", "productVideo"])->where(['user_id' => $user->id])
            ->get();

        $productVideosArray = $productVideosAll->toArray();

        foreach ($productVideosArray as $key => $item) {
            $item["unlocked"] = false;
            $productVideosArray[$key] = $item;
        }

        foreach ($productVideosArray as $key => $item) {
            foreach ($userProductVideos as $userProductVideo) {
                if ($item['id'] == $userProductVideo->product_video_id) {
                    $item['unlocked'] = true;
                    $productVideosArray[$key] = $item;
                }
            }
        }


        return $productVideosArray;
    }
}
