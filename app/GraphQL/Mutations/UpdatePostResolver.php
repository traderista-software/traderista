<?php


namespace App\GraphQL\Mutations;


use App\Models\Post;
use App\Models\Trade;
use GraphQL\Type\Definition\ResolveInfo;
use Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class UpdatePostResolver
{
    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $user = $context->request()->user();
        $input = collect($args)->toArray();

        $tradeInput = $input['trade'];

        $post = Post::with(['user'])->where(['id' => $input['id']])->first();

        $post->content = $input['content'];
        $post->tradeType = $input['tradeType'];
        $post->stockType = $input['stockType'];

        if (isset($input['allUsers'])){
            $post->allUsers = $input['allUsers'];
        }

        if (isset($input['isPinned'])){
            $post->isPinned = $input['isPinned'];
        }

        $post->save();

        if (array_key_exists('tradeId', $input) && $input['tradeId'] !== null) {

            $tradeId = $input['tradeId'];
            $trade = Trade::find($input['tradeId']);

            $trade->position = $tradeInput['position'];
            $trade->stockType = $tradeInput['stockType'];
            $trade->tradeType = $tradeInput['tradeType'];
            $trade->entryDate = $tradeInput['entryDate'];
            $trade->leverage = $tradeInput['leverage'];
            $trade->price = $tradeInput['price'];
            $trade->amountPercentage = $tradeInput['amountPercentage'];
            $trade->stopLoss = $tradeInput['stopLoss'];
            $trade->takeProfit = $tradeInput['takeProfit'];
            $trade->company_symbol = $tradeInput['company']['symbol'];

            $trade->save();
        }
        $post->author = $post->user;
        return json_decode(json_encode($post), true);
    }
}
