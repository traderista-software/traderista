<?php


namespace App\GraphQL\Mutations;


use App\Models\FormMessage;
use App\Models\Reward;
use App\Models\User;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class UpdateRewardResolver
{

    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = collect($args)->toArray();

        $coin_amount = $input['coin_amount'];

        $reward = Reward::with(['user'])->whereId($input['id'])->first();
        $oldCoinAmount = $reward->coin_amount;


        $reward->coin_amount = $coin_amount;
        $reward->note = $input['note'];
        $reward->save();

        $user = User::whereId($reward->user->id)->first();
        $user->coin_amount -= $oldCoinAmount;
        $user->coin_amount += $reward->coin_amount;

        $user->coins_fiat_amount -= $oldCoinAmount;
        $user->coins_fiat_amount += $reward->coin_amount;
        $user->save();

        return $reward->toArray();
    }

}
