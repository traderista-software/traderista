<?php


namespace App\GraphQL\Mutations;


use App\Models\Email;
use App\Models\EmailSending;
use App\Models\Lead;
use App\Models\User;
use App\Services\MailService;
use App\Utils\TokenGenerator;
use DB;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class SendEmailTemplateToUsers
{

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = $args['input'];
        $emailId = $input['emailId'];
        $userTargets = $input['userTargets'];
        $subscriberTargets = $input['subscriberTargets'];

        $users = [];
        $subscribers = [];

        if ($userTargets !== 'allUsers') {
            $userTargets = array_map('intval', explode(',', $userTargets, -1));
            $users = User::whereIn('id', $userTargets)->where('emailNotification', 1)->get();
        } else {
            $users = User::where('emailNotification', 1)->get();
        }

        if($subscriberTargets !== 'allSubscribers') {
            $subscriberTargets = array_map('intval', explode(',', $subscriberTargets, -1));
            $subscribers = Lead::whereIn('id', $subscriberTargets)->where('has_unsubscribed', false)->get();
        } else {
            $subscribers = Lead::where('has_unsubscribed', false)->get();
        }

        $email = Email::whereId($emailId)->first();
        $emailsToSend = array();

        $sentEmails = array();
        foreach ($users as $user) {
            $token = TokenGenerator::generateToken();
            User::whereId($user->id)->update(['emailToken' => $token]);

            $emailsToSend[] = [
                'email' => $user->email,
                'emailObj' => $email,
                'name' => $user->name,
                'unsubscribeUrl' => env('APP_URL') . '/emails/unsubscribe?token=' . $token
            ];

            $sentEmails[] = $user->email;
        }
        foreach ($subscribers as $subscriber) {
            if(!in_array($subscriber->email, $sentEmails, true)) {
                $token = TokenGenerator::generateToken();
                Lead::whereId($subscriber->id)->update(['token' => $token]);

                $emailsToSend[] = [
                    'email' => $subscriber->email,
                    'emailObj' => $email,
                    'name' => 'Trader',
                    'unsubscribeUrl' => env('APP_URL') . '/emails/unsubscribe?token=' . $token
                ];

                $sentEmails[] = $subscriber->email;
            }
        }

        \Log::info($emailsToSend);

        $mailService = new MailService();
        $mailService->sendMarketingEmail($emailsToSend);

        EmailSending::insert([
            'email_id' => $emailId,
            'userTargets' => $userTargets === 'allUsers' ? $userTargets : implode(',', $userTargets),
            'subscriberTargets' => $subscriberTargets === 'allSubscribers' ? $subscriberTargets : implode(',', $subscriberTargets)
        ]);

        return [
            'status' => 'SENT',
            'message' => 'Emails sent.',
        ];
    }
}
