<?php

namespace App\GraphQL\Mutations;

use App\Models\Lead;
use App\Services\MailService;
use App\Utils\TokenGenerator;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class DoubleOptInResolver
{
    private MailService $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws Exception
     *
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = collect($args)->toArray();

        $email = $input['email'];
        $type = $input['type'];
        $surveyResults = $input['surveyResults'];
        $recommendedPackageId = $input['recommendedPackageId'];

        $token = TokenGenerator::generateToken();

        Lead::insert(['email' => $email, 'type' => $type, 'token' => $token, 'surveyResults' => $surveyResults, 'recommendedPackageId' => $recommendedPackageId]);

        $url = env('API_URL') . '/email-opt-in/success?token=' . $token;
        $unsubscribeUrl = env('APP_URL') . '/emails/unsubscribe?token=' . $token;

        if ($type === 'SURVEY') {
            $this->mailService->sendSurveyDoubleOptInEmail($email, $url, $unsubscribeUrl);
        } else if ($type === 'SUBSCRIPTION') {
            $url .= '&type=subscription&redirect=e-book';
//            $this->mailService->sendSubscriptionDoubleOptInEmail($email, $url, $unsubscribeUrl);
            $this->mailService->sendEBookSubscriptionEmail($email, $url);
        } else if ($type === 'WEBINAR') {
            $url .= '&type=subscription&redirect=webinar';
//            $this->mailService->sendWebinarSuccessRegistrationEmail($email, $url);
        }

        return [
            'status' => 'EMAIL_SENT',
            'message' => __('Opt-in email sent'),
            'token' => $token
        ];
    }
}
