<?php


namespace App\GraphQL\Mutations;


use App\Models\CashOutRequest;
use App\Models\FormMessage;
use App\Models\Reward;
use App\Models\User;
use App\Models\UserSubscription;
use Carbon\Carbon;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class CreateRewardResolver
{
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $input = collect($args)->toArray();

        $coin_amount = $input['coin_amount'];
        $user_id = $input['user_id'];

        $id = Reward::insertGetId([
            'user_id' => $user_id,
            'coin_amount' => $coin_amount,
            'note' => $input['note'],
        ]);

        $user = User::whereId($user_id)->first();

        $new_coin_amount = $user['coin_amount'] + $coin_amount;
        $new_coins_fiat_amount = $user['coins_fiat_amount'] + $coin_amount;
        User::whereId($user_id)
            ->update(['coin_amount' => $new_coin_amount, 'coins_fiat_amount' => $new_coins_fiat_amount]);

//        $user->coin_amount += $coin_amount;
//        $user->coins_fiat_amount += $coin_amount;
//        $user->save();

        $reward = Reward::with(['user'])->whereId($id)->first()->toArray();
        return $reward;
    }
}
