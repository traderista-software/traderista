<?php


namespace App\GraphQL\Mutations;


use App\Services\TwilioService;
use App\Utils\RequestUtils;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\ValidationException;
use Log;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;

class SendPhoneNumberCodeResolver
{
    private TwilioService $twilioService;
    private RequestUtils $requestUtils;

    public function __construct(TwilioService $twilioService, RequestUtils $requestUtils)
    {
        $this->twilioService = $twilioService;
        $this->requestUtils = $requestUtils;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws ValidationException
     * @throws Exception
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $user = $this->requestUtils->getUserDetails();
        $retrievedNumber = collect($args)->toArray()['phone_number'];

        if ($retrievedNumber === $user->phone_number) {
            throw new ValidationException([
                'code' => __('This phone number is already activated'),
            ], 'Validation Error');
        }

        $digits = 4;
        $code = rand(pow(10, $digits - 1), pow(10, $digits) - 1);

        $user->phone_number = $retrievedNumber;
        $user->phone_number_code = $code;
        $user->save();

        $message = "Ihr Traderista-Aktivierungscode lautet: " . $code;
        try {
            $this->twilioService->sendMessage($retrievedNumber, $message);
        } catch (TwilioException | ConfigurationException $e) {
            Log::error($e);
        }

        return [
            'status' => 'CODE_SENT',
            'message' => __('Code sent to your phone number'),
        ];
    }
}
