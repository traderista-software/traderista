<?php


namespace App\GraphQL;


use App\Models\Company;
use App\Models\Trade;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Log;

class PostObjectCreator
{
    public static function createPost($posts, $userTrades)
    {
        foreach ($posts as $post) {
            $post->cTimestamp = date("d M Y, H:i", strtotime($post->postCTimestamp));
            $post->date_time = date("d M Y, H:i", strtotime($post->post_date_time));

            if ($post->position !== null) {
                $trade = new Trade();

                $trade->id = $post->id; //this is a bit ambigous

                $trade->position = $post->position;
                $post->position = null;

                $trade->entryDate = $post->entryDate;
                $post->entryDate = null;

                $trade->tradeType = $post->tradeType;
                $post->tradeType = null;

                $trade->stockType = $post->stockType;
                $post->stockType = null;

                $trade->price = $post->price;
                $post->price = null;

                $trade->amountPercentage = $post->amountPercentage;
                $post->amountPercentage = null;

                $trade->leverage = $post->leverage;
                $post->leverage = null;

                $trade->stopLoss = $post->stopLoss;
                $post->stopLoss = null;

                $trade->takeProfit = $post->takeProfit;
                $post->takeProfit = null;

                $trade->status = $post->status;
                $post->status = null;

                $company = new Company();

                $company->symbol = $post->symbol;
                $company->company_symbol = $post->symbol;
                $post->symbol = null;

                $company->name = $post->name;
                $post->name = null;

                $company->logo = $post->logo;
                $post->logo = null;

                $trade->company = $company->toArray();
                $post->trade = $trade->toArray();
            }

            $post->id = $post->postId;

            $post->user_trades = PostObjectCreator::getUserTradesByPostId($userTrades, $post->id);

            $user = User::whereId($post->user_id)->get();
            $post->author = $user->toArray()[0];

            $comments = DB::table('comments')
                ->join('users', 'comments.user_id', '=', 'users.id')
                ->where(['post_id' => $post->id])
                ->select(['comments.*', 'users.name', 'users.surname'])
                ->get();
            foreach ($comments as $comment) {
                $comment->author = ['id' => $comment->user_id, 'name' => $comment->name, 'surname' => $comment->surname];
                $comment->name = null;
                $comment->surname = null;
                $comment->cTimestamp = date("d M Y, H:i", strtotime($comment->cTimestamp));
                $comment->date_time = date("d M Y, H:i", strtotime($comment->date_time));
            }
            $post->comments = $comments->toArray();
        }

        $posts = $posts->sortByDesc('postCTimestamp');

        $posts = $posts->toArray();

        usort($posts, function ($a, $b) {
//            return $a->isPinned <=> $b->isPinned ?: $b->postCTimestamp <=> $a->postCTimestamp;

            if ($a->isPinned === $b->isPinned) {
                return $a->postCTimestamp > $b->postCTimestamp ? -1 : 1;
            }
            return $a->isPinned > $b->isPinned ? -1 : 1;
        });
        return $posts;
    }

    public static function getUserTradesByPostId($userTrades, $id)
    {
        return $userTrades->filter(function ($item) use ($id) {
            return $item->post_id == $id;
        })->values();
    }
}
