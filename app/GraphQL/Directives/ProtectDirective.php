<?php

namespace App\GraphQL\Directives;

use App\Exceptions\AuthorizedException;
use App\Models\Guest;
use Auth;
use Closure;
use GraphQL\Language\AST\TypeDefinitionNode;
use GraphQL\Language\AST\TypeExtensionNode;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Support\Carbon;
use Joselfonseca\LighthouseGraphQLPassport\Exceptions\ValidationException;
use Nuwave\Lighthouse\Exceptions\AuthenticationException;
use Nuwave\Lighthouse\Exceptions\DefinitionException;
use Nuwave\Lighthouse\Schema\AST\ASTHelper;
use Nuwave\Lighthouse\Schema\AST\DocumentAST;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Support\Contracts\FieldMiddleware;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class ProtectDirective extends BaseDirective implements FieldMiddleware
{

    /**
     * @var AuthFactory
     */
    protected AuthFactory $auth;

    public function __construct(AuthFactory $auth)
    {
        $this->auth = $auth;
    }

    public static function definition(): string
    {
        return /** @lang GraphQL */ <<<'GRAPHQL'
"""
Run authentication through one or more guards.
This is run per field and may allow unauthenticated
users to still receive partial results.
"""
directive @protect(
  """
  Specify which guards to use, e.g. ["api"].
  When not defined, the default from `lighthouse.php` is used.
  """
  with: [String!]

  """
    Apply scopes to the underlying query.
    """
    scopes: [String]
) on FIELD_DEFINITION | OBJECT
GRAPHQL;
    }

    public function handleField(FieldValue $fieldValue, Closure $next): FieldValue
    {

        $previousResolver = $fieldValue->getResolver();
        // TODO remove cast in v6
        $with = (array)(
            $this->directiveArgValue('with')
            ?? [config('lighthouse.guard')]
        );

        $fieldValue->setResolver(
            function ($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo) use ($with, $previousResolver) {

                $this->authenticate($with, $context->request()->header('Authorization'));

                return $previousResolver($root, $args, $context, $resolveInfo);
            }
        );

        return $next($fieldValue);
    }

    /**
     * Determine if the user is logged in to any of the given guards.
     *
     * @param array<string> $guards
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function authenticate(array $guards, string $authorizationHeader): void
    {
        foreach ($guards as $guard) {
            $this->auth->shouldUse($guard);
            $flag = $this->authorize($guard, $guards, $authorizationHeader);

            if ($flag) {
                return;
            }
        }

        $this->unauthenticated($guards);
    }

    /**
     * @throws AuthorizedException
     * @throws AuthenticationException
     */
    protected function authorize($guard, array $guards, string $authorizationHeader): bool
    {
        if ($guard === 'api') {
            $scopes = $this->directiveArgValue('scopes');

            if (isset($scopes)) {
                $authenticated = Auth::user();
                if(isset($authenticated)) {
                    $role = $authenticated->getAttribute('role');

                    $flag = false;
                    foreach ($scopes as $scope) {
                        if ($scope === $role) {
                            $flag = true;
                            break;
                        }
                    }

                    return $flag;
                }
            }
            return false;
        }

        if ($guard === 'guest') {
            $guest = Guest::where(['api_token' => $authorizationHeader])->first();

            if(isset($guest)) {
                return !(Carbon::parse($guest->created_at)->addDays(1) < now());
            }
            return false;
        }
    }

    /**
     * Handle an unauthenticated user.
     *
     * @param array<string|null> $guards
     * @throws AuthenticationException
     */
    protected function unauthenticated(array $guards): void
    {
        throw new AuthenticationException(
            AuthenticationException::MESSAGE,
            $guards
        );
    }

    /**
     * Handle an unauthorized user.
     *
     * @param array<string|null> $guards
     * @throws AuthorizedException
     */
    protected function unauthorized(array $guards): void
    {
        throw new AuthorizedException(
            AuthorizedException::MESSAGE,
            $guards
        );
    }

    /**
     * @throws DefinitionException
     */
    public function manipulateTypeDefinition(DocumentAST &$documentAST, TypeDefinitionNode &$typeDefinition): void
    {
        ASTHelper::addDirectiveToFields($this->directiveNode, $typeDefinition);
    }

    /**
     * @throws DefinitionException
     */
    public function manipulateTypeExtension(DocumentAST &$documentAST, TypeExtensionNode &$typeExtension): void
    {
        ASTHelper::addDirectiveToFields($this->directiveNode, $typeExtension);
    }
}
