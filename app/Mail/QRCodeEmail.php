<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QRCodeEmail extends Mailable
{
    use Queueable, SerializesModels;


    public string $text;

    public array $qrCodes;

    public string $unsubscribeUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($text, $qrCodes, $unsubscribeUrl)
    {
        $this->text = $text;
        $this->qrCodes = $qrCodes;
        $this->unsubscribeUrl = $unsubscribeUrl;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        $email = $this->view('emails.marketing-email')
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('QR-Code Ihres Traderista-Empfehlungslinks')
            ->with([
                'text' => $this->text,
//                'unsubscribeUrl' => $this->unsubscribeUrl
            ]);

        foreach ($this->qrCodes as $key => $value) {
            $email->attachData($value, 'traderista_' . $key . '_qr_code.png');
        }
        return $email;
    }
}
