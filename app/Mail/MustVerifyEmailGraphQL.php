<?php

namespace App\Mail;

use App\Mail\VerifyEmail;

/**
 * Trait MustVerifyEmailGraphQL
 * @package App\Mail
 */
trait MustVerifyEmailGraphQL
{
    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail());
    }
}
