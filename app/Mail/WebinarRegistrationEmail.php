<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WebinarRegistrationEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $actionUrl;

    /**
     * Create a new message instance.
     *
     * @param $actionUrl
     */
    public function __construct($actionUrl)
    {
        $this->actionUrl = $actionUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('emails.webinar-first-mail')
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Bestätige jetzt Deine Webinar Anmeldung!')
            ->with([
                'actionUrl' => $this->actionUrl
            ]);
    }
}
