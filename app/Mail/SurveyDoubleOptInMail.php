<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SurveyDoubleOptInMail extends Mailable
{
    use Queueable, SerializesModels;

    private $actionUrl;

    private $unsubscribeUrl;

    /**
     * Create a new message instance.
     *
     * @param $actionUrl
     */
    public function __construct($actionUrl, $unsubscribeUrl)
    {
        $this->actionUrl = $actionUrl;
        $this->unsubscribeUrl = $unsubscribeUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('emails.e-book-opt-in')
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Bitte bestätige Deine Anfrage für Deine Quiz-Ergebnisse von Traderista 🔥📈')
            ->with([
                'actionUrl' => $this->actionUrl,
                'unsubscribeUrl' => $this->unsubscribeUrl
            ]);
    }
}
