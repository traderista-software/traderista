<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CreatePostEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $message;
    private $unsubscribeUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message, $unsubscribeUrl)
    {
        $this->message = $message;
        $this->unsubscribeUrl = $unsubscribeUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('emails.create-post')
            ->with([
                'msg' => $this->message,
                'actionUrl' => env('API_URL') . '/app/posts',
                'unsubscribeUrl' => $this->unsubscribeUrl
            ])
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Neuer Posten ' . Carbon::now()->toDateTimeString());
    }
}
