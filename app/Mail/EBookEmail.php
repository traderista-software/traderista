<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EBookEmail extends Mailable
{
    use Queueable, SerializesModels;

    public string $guestUrl;

    public string $packagesUrl;

    public string $unsubscribeUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($guestUrl, $packagesUrl, $unsubscribeUrl)
    {
        $this->guestUrl = $guestUrl;
        $this->packagesUrl = $packagesUrl;
        $this->unsubscribeUrl = $unsubscribeUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('emails.email-subscription')
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Traderista E-Book')
            ->with([
                'guestUrl' => $this->guestUrl,
                'packagesUrl' => $this->packagesUrl,
                'unsubscribeUrl' => $this->unsubscribeUrl
            ])
            ->attach(public_path() . '/Traderista_E-book.pdf', [
                'as' => 'Traderista_E-book.pdf',
                'mime' => 'application/pdf',
            ]);
    }
}
