<?php


namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CoachingApplicationEmail extends Mailable
{

    use Queueable, SerializesModels;

    /**
     * @var
     */
    public string $name;

    public string $email;

    public string $message;

    public string $type;

    public $phoneNumber;

//    public string $unsubscribeUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $phoneNumber, $message, $type)
    {
        $this->name = $name;
        $this->email = $email;
        $this->message = $message;
        $this->type = $type;
        $this->phoneNumber = $phoneNumber;
//        $this->unsubscribeUrl = $unsubscribeUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.coaching-application')
            ->with([
                'name' => $this->name,
                'email' => $this->email,
                'msg' => $this->message,
                'type' => $this->type,
                'phoneNumber' => $this->phoneNumber,
//                'unsubscribeUrl' => $this->unsubscribeUrl
            ])
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject($this->type === 'LIMITLESS_POTENTIAL_COACHING_APPLICATION' ? 'Neue Anwendung für Limitless Potential Coaching' : 'Neues Kontaktformular eingereicht');
    }
}
