<?php


namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CoachingQuestionEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    public $title;
    public $fullName;
    public $userEmail;
    public $content;
    public $unsubscribeUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $fullName, $userEmail, $content)
    {
        $this->title = $title;
        $this->fullName = $fullName;
        $this->userEmail = $userEmail;
        $this->content = $content;
        $this->unsubscribeUrl = '';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.coaching-question')
            ->with([
                'fullName' => $this->fullName,
                'email' => $this->userEmail,
                'content' => $this->content,
                'unsubscribeUrl' => $this->unsubscribeUrl
            ])
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Frage zum Member Coaching: ' . $this->title);
    }
}
