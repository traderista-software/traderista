<?php


namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ChangeEmailEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var
     */
    public $code;

    public $unsubscribeUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code, $unsubscribeUrl)
    {
        $this->code = $code;
        $this->unsubscribeUrl = $unsubscribeUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.change-email')
            ->with([
                'code' => $this->code,
                'unsubscribeUrl' => $this->unsubscribeUrl
            ])
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Ihr Prüfcode lautet: ' . $this->code);
    }
}
