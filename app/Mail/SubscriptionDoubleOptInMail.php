<?php


namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubscriptionDoubleOptInMail extends Mailable
{
    use Queueable, SerializesModels;

    private $actionUrl;

    private $unsubscribeUrl;

    /**
     * Create a new message instance.
     *
     * @param $actionUrl
     */
    public function __construct($actionUrl, $unsubscribeUrl)
    {
        $this->actionUrl = $actionUrl;
        $this->unsubscribeUrl = $unsubscribeUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('emails.subscription-double-opt-in')
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Willkommen bei Traderista!')
            ->with([
                'actionUrl' => $this->actionUrl,
                'unsubscribeUrl' => $this->unsubscribeUrl
            ]);
    }
}
