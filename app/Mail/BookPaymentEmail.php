<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookPaymentEmail extends Mailable
{
    use Queueable, SerializesModels;

    public string $actionUrl;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($actionUrl)
    {
        $this->actionUrl = $actionUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('emails.book-payment')
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Ihre Zahlung war erfolgreich')
            ->with([
                'actionUrl' => $this->actionUrl
            ]);
    }
}
