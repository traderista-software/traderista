<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EBookSubscriptionMail extends Mailable
{
    use Queueable, SerializesModels;

    private $actionUrl;

    /**
     * Create a new message instance.
     *
     * @param $actionUrl
     */
    public function __construct($actionUrl)
    {
        $this->actionUrl = $actionUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('emails.e-book-subscription-email')
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Dein gratis PDF Guide "Die Ultimative Anleitung zum Aktienhandel - Entspannt Erfolge
            erzielen mit Null-Verlust-Garantie & Copy-Trading"')
            ->with([
                'actionUrl' => $this->actionUrl
            ]);
    }
}
