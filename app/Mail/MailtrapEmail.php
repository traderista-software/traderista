<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailtrapEmail extends Mailable
{
    use Queueable, SerializesModels;

    public function build()
    {
        return $this
            ->from('psimixhiu.team@gmail.com', 'Mailtrap')
            ->subject('Email Confirmation')
            ->markdown('mails.exmpl')
            ->view('emails.email-subscription')
            ->with([
                'name' => 'New User',
                'link' => 'https://mailtrap.io/inboxes'
            ]);
    }
}
