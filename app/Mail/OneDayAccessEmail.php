<?php

namespace App\Mail;

use App\Models\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OneDayAccessEmail extends Mailable
{
    use Queueable, SerializesModels;

    public string $actionUrl;

    /**
     * OneDayAccessEmail constructor.
     * @param $actionUrl
     */
    public function __construct($actionUrl)
    {
        $this->actionUrl = $actionUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('emails.one-day-access')
            ->with([
                'actionUrl' => $this->actionUrl
            ])
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject("Dein 24h Gratis Zugang");
    }

}
