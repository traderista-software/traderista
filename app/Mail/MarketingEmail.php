<?php


namespace App\Mail;


use App\Models\Email;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MarketingEmail extends Mailable
{
    use Queueable, SerializesModels;

    public Email $emailObj;
    public string $unsubscribeUrl;

    /**
     * MarketingEmail constructor.
     * @param Email $emailObj
     */
    public function __construct(Email $emailObj, $unsubscribeUrl)
    {
        $this->emailObj = $emailObj;
        $this->unsubscribeUrl = $unsubscribeUrl;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        $this->view('emails.marketing-email')
            ->with([
                'text' => $this->emailObj->text,
                'unsubscribeUrl' => $this->unsubscribeUrl
            ])
            ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject($this->emailObj->subject);
        if ($this->emailObj->attachmentPath !== null) {
            $this->attach(public_path() . $this->emailObj->attachmentPath);
        }
        return $this;
    }
}
