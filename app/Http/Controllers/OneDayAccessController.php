<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\User;
use App\Services\MailService;
use App\Utils\TokenGenerator;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OneDayAccessController extends Controller
{

    public static function runCron(): void
    {

        // leads that have not already subscribed
        $firstTime = Carbon::now()->subMinutes(40)->tz('UTC')->toISOString();
        $secondTime = Carbon::now()->subMinutes(20)->tz('UTC')->toISOString();


        $date = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->subMinutes(40), 'Europe/Berlin');
        $date2 = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->subMinutes(20), 'Europe/Berlin');

        $leads = Lead::where(function ($query) {
            $query->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('users')
                    ->join('us_user_subscription', 'users.id', 'us_user_subscription.user_id')
                    ->whereColumn(['users.email' => 'mk_lead.email']);
            });
        })->where('cTimestamp', ">=", $firstTime)
            ->where('cTimestamp', "<", $secondTime)
            ->get();

        // users that have no subscription history
        $users = User::where(function ($query) {
            $query->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('us_user_subscription')
                    ->whereColumn(['users.id' => 'us_user_subscription.user_id']);
            });
        })->where('cTimestamp', ">=", $date)
            ->where('cTimestamp', "<", $date2)
            ->get();

        $registerUrl = env('APP_URL') . '/auth/sign-up?oneDayTrial=true';
        $loginUrl = env('APP_URL') . '/auth/sign-in?oneDayTrial=true';

        $list = array();

        foreach ($users as $user) {
            if (!array_key_exists($user->email, $list)) {
                $list[] = [$user->email, $user->cTimestamp, $loginUrl . '&email=' . $user->email . '&token=' . $user->emailToken];
            }
        }

        foreach ($leads as $lead) {
            if (!array_key_exists($lead->email, $list)) {
                $list[] = [$lead->email, $lead->cTimestamp, $registerUrl . '&email=' . $lead->email . '&token=' . $lead->token];
            }
        }

        $mailService = new MailService();
        $mailService->sendOneDayAccessEmail($list);
    }
}
