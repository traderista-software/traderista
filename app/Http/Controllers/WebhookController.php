<?php

namespace App\Http\Controllers;

use App\Models\FacebookEvent;
use App\Models\Lead;
use App\Services\MailService;
use App\Utils\TokenGenerator;
use FacebookAds\Api;
use FacebookAds\Exception\Exception;
use FacebookAds\Object\LeadgenForm;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    protected $api;

    private MailService $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
        $this->api = Api::init(
            env('FACEBOOK_APP_ID'),
            env('FACEBOOK_APP_SECRET'),
            env('FACEBOOK_ACCESS_TOKEN')
        );
    }

    public function verifyCallback(Request $request)
    {
        $mode = $request->get('hub_mode');
        $token = $request->get('hub_verify_token');
        $challenge = $request->get('hub_challenge');

        if ($mode === "subscribe" && $token === 'traderista') {
            return response($challenge, 200);
        }
        return response("Invalid token!", 400);
    }

    public function handleFacebookLead(Request $request)
    {
        \Log::info($request);

        $entry = $request->input('entry');
        if (!empty($entry)) {
            foreach ($entry as $item) {
                if (!empty($item['changes'])) {
                    foreach ($item['changes'] as $change) {
                        if ($change['field'] === 'leadgen') {
                            $this->insertFacebookEvent($change['value']);

                            $leadgenId = $change['value']['leadgen_id'];

                            \Log::info($leadgenId);
                            $leadData = $this->getLeadData($leadgenId);
                            \Log::info(json_encode($leadData));

                            if ($leadData == null) {
                                break;
                            }

                            $fieldData = $leadData->field_data;
                            \Log::info($fieldData);
                            $email = '';

                            foreach ($fieldData as $field) {
                                if ($field['name'] === 'email') {
                                    $email = $field['values'][0];
                                    break;
                                }
                            }


                            // Process lead data as needed
                            $this->optInLead($email);
                        }
                    }
                }
            }
        }

        return response('Webhook received', 200);
    }

    public function getLeadData($leadgenId): ?\FacebookAds\Object\Lead
    {
        try {
            $lead = new \FacebookAds\Object\Lead($leadgenId);
            return $lead->getSelf(['id', 'created_time', 'field_data']);
//            return $lead->getSelf();
        } catch (Exception $e) {
            \Log::error($e);
            // Error handling
            return null;
        }
    }

    public function optInLead($email)
    {
        if ($email == '' || $email == null) return;
        $type = 'SUBSCRIPTION';

        $token = TokenGenerator::generateToken();

        Lead::insert(['email' => $email, 'type' => $type, 'token' => $token]);

        $url = env('API_URL') . '/email-opt-in/success?token=' . $token;
        $unsubscribeUrl = env('APP_URL') . '/emails/unsubscribe?token=' . $token;

        \Log::info("Lead from Facebook ad successfully inserted");

        $this->mailService->sendEBookSubscriptionEmail($email, $url);


    }

    public function insertFacebookEvent(array $event)
    {
        try {
            FacebookEvent::insert([
                'leadgen_id' => $event['leadgen_id'],
                'page_id' => $event['page_id'],
                'form_id' => $event['form_id'],
                'adgroup_id' => $event['adgroup_id'],
                'ad_id' => $event['ad_id'],
                'created_time' => $event['created_time']
            ]);
        } catch (\Exception $e) {
            \Log::error($e);
        }
    }
}
