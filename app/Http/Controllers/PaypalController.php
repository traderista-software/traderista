<?php

namespace App\Http\Controllers;

use App\Models\UserSubscription;
use App\Services\MailService;
use App\Services\PackageService;
use App\Services\UserService;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PaypalController extends Controller
{
    private UserService $userService;
    private PackageService $packageService;
    private MailService $mailService;


    public function __construct(UserService $userService, PackageService $packageService, MailService $mailService)
    {
        $this->userService = $userService;
        $this->packageService = $packageService;
        $this->mailService = $mailService;
    }

    public function handleEvent(Request $request)
    {
//        DB::connection()->enableQueryLog();

        $content = $request->getContent();
        $eventId = $request->get('id');
        $eventType = $request->get('event_type');

        Log::info($request);
        $exists = DB::table('paypal_webhook_events')->where('event_id', $eventId)->exists();

        if (!$exists) {
            if ($eventType === 'BILLING.SUBSCRIPTION.CANCELLED') {
                $subscriptionId = $request->get('resource')['id'];
                $this->handleSubscriptionCancelledEvent($subscriptionId);
            } else if ($eventType === 'PAYMENT.SALE.COMPLETED') {
                $subscriptionId = $request->get('resource')['billing_agreement_id'];
                $this->handlePaymentSuccessEvent($subscriptionId, $request->get('resource')['id']);
            } else if ($eventType === 'CHECKOUT.ORDER.APPROVED') {
                $email = $request->get('resource')['payer']['email_address'];
                $item = $request->get('resource')['purchase_units'][0]['description'];
                if ($item === "Grundkurs \"Wahrer Wohlstand\"" || $item === "Trader's most efficient Coaching") {
                    $this->handleBookPaymentEvent($email, $item);
                }
                $subscriptionId = null;
            } else {
                $subscriptionId = $request->get('resource')['id'];
            }

            $this->persistEvent($eventId, $subscriptionId, $eventType, $content);
        } else {
            \Log::info('Event twice triggered' . $content);
        }

//        foreach (DB::getQueryLog() as $q) {
//            \Log::info($q);
//        }

        return response()->json([]);
    }

    /**
     * @param $subscriptionId
     */
    public function handleSubscriptionCancelledEvent($subscriptionId): void
    {
        $subscription = UserSubscription::where('paypal_subscription_id', $subscriptionId)->first();
        if (isset($subscription) && $subscription->status !== 'CANCELED') {
            $subscription->update([
                'status' => 'CANCELED',
                'expiryDate' => $subscription->calculateExpiryDateFromLastPayment(),
            ]);
        }
    }

    /**
     * @param $subscriptionId
     */
    public function handlePaymentSuccessEvent($subscriptionId, $paymentId): void
    {
        $subscription = UserSubscription::where('paypal_subscription_id', $subscriptionId)->first();

        try {
            $this->mailService->sendSuccessfulPaymentEmail($subscription->user()->first()->email);
        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }

        if (isset($subscription) && $subscription->status === 'CONFIRMED') {
            $packagePrice = $subscription['price'];
            $this->userService->giveRewardsToReferralNetwork($subscription->user()->first(), $subscription, $packagePrice, $subscription['duration'], $paymentId);
        }
    }

    /**
     * @param $eventId
     * @param $subscriptionId
     * @param $eventType
     * @param $content
     */
    public function persistEvent($eventId, $subscriptionId, $eventType, $content): void
    {
        try {
            DB::table('paypal_webhook_events')->insert([
                'event_id' => $eventId,
                'subscription_id' => $subscriptionId,
                'event_type' => $eventType,
                'event' => $content,
            ]);
        } catch (QueryException $ex) {
            \Log::info($ex->getMessage());
        }
    }

    public function handleBookPaymentEvent($email, $item)
    {
        if ($item === "Grundkurs \"Wahrer Wohlstand\"") {
            Log::info($email);
            $this->mailService->sendBookPaymentEmail($email, "https://drive.google.com/file/d/1jSSMCwnrqPOkuSDPxmkz5jJeIJrUSUyT/view?usp=sharing");
        } else {
            $this->mailService->sendBookPaymentEmail($email, "https://drive.google.com/file/d/1FeMLwXvPCpLz7ZyL2VjOrx5lDVcL7DlQ/view?usp=sharing");
        }
    }
}
