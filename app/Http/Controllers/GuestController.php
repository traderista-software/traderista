<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Guest;
use Illuminate\Http\JsonResponse;
use Request;
use Validator;

class GuestController extends \Illuminate\Routing\Controller
{
    private Company $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    public function guestDashboard(): JsonResponse
    {
        $company = $this->company->getCompany(2);
        $success = $company;

        return response()->json($success);
    }

    public function guestLogin(Request $request): JsonResponse
    {


//        $validator = Validator::make($request->all(), [
//            'email' => 'required|email',
//            'password' => 'required',
//        ]);

//        if ($validator->fails()) {
//            return response()->json(['error' => $validator->errors()->all()]);
//        }

        if (auth()->guard('admin')->attempt(['email' => request('email'), 'password' => request('password')])) {

            config(['auth.guards.api.provider' => 'guest']);

            $admin = Guest::select('guests.*')->find(auth()->guard('admin')->user()->id);
            $success = $admin;
            $success['token'] = $admin->createToken('MyApp', ['guest'])->accessToken;

            return response()->json($success);
        } else {
            return response()->json(['error' => ['Email and Password are Wrong.']]);
        }
    }
}
