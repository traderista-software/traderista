<?php

namespace App\Http\Controllers;

use App\Models\Email;
use App\Models\Lead;
use App\Models\User;
use App\Models\UserProductVideo;
use App\Services\MailService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Log;

class MMSController extends Controller
{

    public static function runNewNurtureCampaigns(): JsonResponse
    {
        $emails = Email::where(['type' => 'NURTURE'])->get();

        // users that have no subscription history
        $users = User::where('emailNotification', true)
            ->get();

        $set = array();

        foreach ($users as $user) {
            if (!array_key_exists($user->email, $set) && $user->emailNotification === 1) {
                $set[] = [$user->email, $user->cTimestamp, $user->name, env('APP_URL') . '/emails/unsubscribe?token=' . $user->emailToken];
            }
        }

        $nurtureEmails = array();
        foreach ($emails as $email) {
            $nurtureEmails[] = $email;
        }

        // sort by order number
        usort($nurtureEmails, static function ($a, $b) {
            return $a->orderNumber > $b->orderNumber;
        });

        $emailsToSend = array();

        $daysFromStart = 0;
        foreach ($nurtureEmails as $iValue) {
            $daysFromStart += $iValue->timePeriodInDays;

            foreach ($set as $recipient) {
                $diff = now()->diffInDays($recipient[1]->startOfDay());
                if ($diff === $daysFromStart) {
                    $emailsToSend[] = [
                        'email' => $recipient[0],
                        'emailObj' => $iValue,
                        'name' => $recipient[2],
                        'unsubscribeUrl' => $recipient[3]
                    ];
                }
            }
        }

        // Log all emails to be sent
        Log::info('Nurture Emails to be sent: ' . json_encode($emailsToSend));

        $mailService = new MailService();
        $mailService->sendMarketingEmail($emailsToSend);

        return response()->json([]);
    }

    public static function runSales(): JsonResponse
    {
        $emails = Email::where(['type' => 'SALES'])->get();

        // leads that have not already subscribed
        $leads = Lead::where('has_unsubscribed', false)
            ->get();

        $set = array();

        foreach ($leads as $lead) {
            if (!array_key_exists($lead->email, $set) && !$lead->has_unsubscribed) {
                $set[] = [$lead->email, $lead->cTimestamp, 'Trader', env('APP_URL') . '/emails/unsubscribe?token=' . $lead->token];
            }
        }

        $salesEmails = array();
        foreach ($emails as $email) {
            $salesEmails[] = $email;
        }

        usort($salesEmails, static function ($a, $b) {
            if ($a->type !== $b->type) {
                return $a->type !== "SALES";
            }
            return $a->orderNumber > $b->orderNumber;
        });

        $emailsToSend = array();


        $daysFromStart = 0;
        foreach ($salesEmails as $iValue) {
            $daysFromStart += $iValue->timePeriodInDays;

            foreach ($set as $recipient) {
                $diff = now()->diffInDays($recipient[1]->startOfDay());
                if ($diff === $daysFromStart) {
                    $emailsToSend[] = [
                        'email' => $recipient[0],
                        'emailObj' => $iValue,
                        'name' => $recipient[2],
                        'unsubscribeUrl' => $recipient[3]
                    ];
                }
            }
        }
//        Log::info($emailsToSend);

        $mailService = new MailService();
        $mailService->sendMarketingEmail($emailsToSend);

        return response()->json([]);
    }

    public static function productLaunch(): void
    {
        $users = User::all();
        foreach ($users as $user) {
            $userProductVideo = UserProductVideo::where(['user_id' => $user->id])->orderBy('product_video_id', 'DESC')->first();
            if (!$userProductVideo) {
                UserProductVideo::insert([
                    'user_id' => $user->id,
                    'product_video_id' => 1
                ]);
            } else {
                if ($userProductVideo->product_video_id < 4) {
                    UserProductVideo::insert([
                        'user_id' => $user->id,
                        'product_video_id' => $userProductVideo->product_video_id + 1
                    ]);
                }
            }
        }
    }

    public static function usersEmail(): JsonResponse
    {
        $emails = Email::where(['type' => 'USER'])
            ->where('timePeriodInDays', '!=', 0)
            ->get();

        $users = User::where("emailNotification", "=", true)->get();

        $set = array();


        foreach ($users as $user) {
            $set[] = [$user->email, $user->cTimestamp, $user->name, $user->name, env('APP_URL') . '/emails/unsubscribe?token=' . $user->emailToken];
        }


        $userEmails = array();
        foreach ($emails as $email) {
            $userEmails[] = $email;
        }

        $emailsToSend = array();

        $daysFromStart = 0;
        for ($i = 0, $iMax = count($userEmails); $i < $iMax; $i++) {
            $daysFromStart += $userEmails[$i]->timePeriodInDays;

            foreach ($set as $recipient) {
                $diff = now()->diffInDays($recipient[1]->startOfDay());
                if ($diff === $daysFromStart) {
                    $emailsToSend[] = [
                        'email' => $recipient[0],
                        'emailObj' => $userEmails[$i],
                        'name' => $recipient[2],
                        'unsubscribeUrl' => $recipient[3]
                    ];
                }
            }
        }

        $mailService = new MailService();
        $mailService->sendMarketingEmail($emailsToSend);

        return response()->json([]);
    }


    public static function test(): JsonResponse
    {

        $datetime = new Carbon('2023-06-11 09:00:00');
        // users that have no subscription history
        $users = User::where('emailNotification', true)
            ->get();

        foreach ($users as $user) {
            $diff = $datetime->diffInDays($user->cTimestamp->startOfDay());
            Log::info($diff, [$user->cTimestamp->startOfDay()]);
        }
        return response()->json([]);
    }


}
