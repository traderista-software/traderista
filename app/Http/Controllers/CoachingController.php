<?php


namespace App\Http\Controllers;


use App\Models\InboxMessage;
use App\Models\User;
use App\Models\UserInboxMessage;

class CoachingController extends Controller
{
    public static function runCron(): void
    {

        $users = User::all();
        $messages = InboxMessage::all();

        foreach ($messages as $message) {
            $day = $message->day;

            foreach ($users as $user) {
                $diff = now()->diffInDays($user->cTimestamp);
                if ($diff === $day - 1) {
                    $model = UserInboxMessage::insert([
                        'user_id' => $user->id,
                        'message_id' => $message->id,
                        'status' => 'UNREAD',
                    ]);
                }
            }
        }


    }

}

