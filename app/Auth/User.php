<?php


namespace App\Auth;


class User extends \Laravel\Passport\Bridge\User
{
    /**
     * @var string
     */
    protected $role;

    /**
     * User constructor.
     * @param  string|int  $identifier
     * @param string $role
     */
    public function __construct($identifier, string $role)
    {
        parent::__construct($identifier);
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param mixed $identifier
     */
    public function setRole($identifier): void
    {
        $this->role = $identifier;
    }
}
