<?php


namespace App\Auth;

class PersonalAccessToken extends \Laravel\Sanctum\PersonalAccessToken
{
    /**
     * Find the token instance matching the given token.
     *
     * @param string $token
     */
    public static function findToken($token)
    {
        if (strpos($token, '|') === false) {
            return static::where('token', $token)->first();
        }

        [$id, $token] = explode('|', $token, 2);

        if ($instance = static::find($id)) {
            return hash_equals($instance->token, hash('sha256', $token)) ? $instance : null;
        }
        return null;
    }
}
