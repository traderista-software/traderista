<?php


namespace App\Services;


use App\Models\Guest;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Http\JsonResponse;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class GuestAuthService extends ParentGraphqlResolver
{

    public function __construct()
    {
    }

    /**
     *
     * @return JsonResponse
     *
     */
    public function getGuestToken(): string
    {
        $guest = new Guest();
        $token = $guest->createToken('Guest', ['guest'])->token;
        $tokenValue = $token->getAttribute('id');
//        $scopes = $token->getAttribute('scopes');

        $toBeCreated = array(
            'name' => 'guest',
            'api_token' => $tokenValue,
//            'scopes' => $scopes
        );

        Guest::create($toBeCreated);

        return $tokenValue;
    }
}
