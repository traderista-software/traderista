<?php

namespace App\Services;


use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class TwilioService
{

    /**
     * @throws ConfigurationException|TwilioException
     */
    public function sendMessage($phoneNumber, $message)
    {
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv('TWILIO_NUMBER');
        $client = new Client($account_sid, $auth_token);
        $client->messages->create($phoneNumber, [
            'from' => $twilio_number,
            'body' => $message
        ]);
    }
}
