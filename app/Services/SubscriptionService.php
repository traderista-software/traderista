<?php


namespace App\Services;

use App\Models\UserSubscription;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class SubscriptionService
{
    private UserSubscription $userSubscription;

    public function __construct(UserSubscription $userSubscription)
    {
        $this->userSubscription = $userSubscription;
    }

    /**
     * @param $rootValue
     * @param array $subscription
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     * @throws Exception
     *
     */
    public function resolve($rootValue, array $subscription, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {
        $createdSubscription = $this->userSubscription->createSubscription($subscription);

        return array(
            'id' => $createdSubscription['id'],
            'userId' => $createdSubscription['userId']
        );
    }

    public function getSubscriptionByUserId(int $userId)
    {
        return UserSubscription::where('userId', $userId)->get();
    }
}
