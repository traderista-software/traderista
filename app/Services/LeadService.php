<?php

namespace App\Services;

use App\Models\Lead;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class LeadService
{
    private MailService $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return array
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): array
    {

        $token = collect($args)->toArray()['token'];
        $lead = Lead::query()->where(['token' => $token])->first();

        if ($lead === null) {
            return [
                'status' => 'TOKEN_NOT_FOUND',
                'message' => __('TOKEN NOT FOUND'),
            ];
        }

        if ($lead->hasOptIn === true) {
            return [
                'status' => 'SUCCESS',
                'message' => __('Already opted in'),
            ];
        }


        Lead::whereId($lead->id)
            ->update([
                'hasOptIn' => true,
            ]);

        return [
            'status' => 'EMAIL_SENT',
            'message' => __('Successfully opted in.'),
        ];
    }
}
