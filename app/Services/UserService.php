<?php

namespace App\Services;

use App\Models\Lead;
use App\Models\User;
use App\Models\UserReferral;
use Carbon\Carbon;
use DB;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Support\Collection;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class UserService
{

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return Collection
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): Collection
    {
        return $this->getSubscribingUsersWithPackages();
    }

    public function getSubscribingUsersWithPackages(): Collection
    {
        return DB::table('users')
            ->join('us_user_subscription', 'users.id', '=', 'us_user_subscription.user_id')
            ->where('status', '=', 'CONFIRMED')
            ->join('fi_package', 'us_user_subscription.package_id', '=', 'fi_package.id')
            ->select('users.*', 'us_user_subscription.user_id', 'us_user_subscription.package_id', 'fi_package.id', 'fi_package.type', 'fi_package.stocksSupported')
            ->get();
    }

    public function giveRewardsToReferralNetwork($user, $subscription, $packagePrice, $type, $paymentId): void
    {
        $referred = $user;
        $users = User::all();
        for ($i = 1; $i <= 6; $i++) {
            if ($referred !== null) {
                $referred = $this->applyMlmReward($users, $user, $referred, $packagePrice, $i, $subscription->id, $type, $paymentId);
            }
        }
    }

    private function applyMlmReward($users, $buyer, $referred, $price, $stage, $subscriptionId, $type, $paymentId)
    {
        $validFrom = $type === 'MONTHLY' ? Carbon::now()->addDays(30) : Carbon::now();

        foreach ($users as $user) {
            if ($user->referral_code === $referred->friend_referral_code) {
                // multiplied by 0.8 to remove taxes and other fees
                $amount = $price * $this->getMlmStagePercentage($stage);

                if ($amount >= 1) {
                    if ($stage === 1) {
                        UserReferral::insert([
                            'payment_id' => $paymentId,
                            'referrer_id' => $user->id,
                            'invited_user_id' => $referred->id,
                            'coin_reward' => $amount,
                            'fiat_reward' => $amount,
                            'verified' => 1,
                            'stage' => $stage,
                            'user_subscription_id' => $subscriptionId,
                            'valid_from_date' => $validFrom
                        ]);
                    } else {
                        UserReferral::insert([
                            'payment_id' => $paymentId,
                            'referrer_id' => $user->id,
                            'invited_user_id' => $buyer->id,
                            'coin_reward' => $amount,
                            'fiat_reward' => $amount,
                            'verified' => 1,
                            'stage' => $stage,
                            'user_subscription_id' => $subscriptionId,
                            'valid_from_date' => $validFrom,
                            'description' => 'Gewonnen, weil ' . $referred->name . ', der Teil Ihres Netzwerks ist, einen Neuzugang in seinem Empfehlungsnetzwerk hat'
                        ]);
                    }
                }

                $user->coin_amount += $amount;
                $user->coins_fiat_amount += $amount;
                $user->save();

                return $user;
            }
        }
        return null;
    }

    private function getMlmStagePercentage($stage)
    {
        if ($stage === 1) {
            return 0.4;
        }
        if ($stage === 2) {
            return 0.08;
        }
        if ($stage === 3) {
            return 0.016;
        }
        if ($stage === 4) {
            return 0.0032;
        }
        if ($stage === 5) {
            return 0.00064;
        }
        if ($stage === 6) {
            return 0.000128;
        }
        return 0;
    }

    public function applyOneDayAccess($input, $userId)
    {
        $activate_one_day_access = $input['activate_one_day_access'] ?? false;
        if ($activate_one_day_access === true) {

            $lead = Lead::whereEmail($input['email'])->first();

            \Log::info($lead);
            if ($lead->token === $input['token']) {
                User::where(['id' => $userId])->update(
                    ['one_day_access' => Carbon::now()->addHours(24)->tz('UTC')->toDateTimeString()]
                );
            }
        }
    }
}
