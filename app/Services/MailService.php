<?php

namespace App\Services;

use App\Mail\BookPaymentEmail;
use App\Mail\CoachingQuestionEmail;
use App\Mail\CreateCommentEmail;
use App\Mail\CreatePostEmail;
use App\Mail\ChangeEmailEmail;
use App\Mail\CoachingApplicationEmail;
use App\Mail\EBookSubscriptionMail;
use App\Mail\OneDayAccessEmail;
use App\Mail\SubscriptionDoubleOptInMail;
use App\Mail\SuccessfulPaymentEmail;
use App\Mail\SurveyDoubleOptInMail;
use App\Mail\EBookEmail;
use App\Mail\MailtrapEmail;
use App\Mail\MarketingEmail;
use App\Mail\QRCodeEmail;
use App\Mail\UnsubscribeEmailConfirmation;
use App\Mail\WebinarRegistrationEmail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailService
{
    public function sendEmail(string $target): void
    {
        Mail::to($target)->send(new MailtrapEmail());
    }

    public function sendEBookEmail($email, $guestUrl, $packagesUrl, $unsubscribeUrl): void
    {
        Mail::to($email)->queue(new EBookEmail($guestUrl, $packagesUrl, $unsubscribeUrl));
    }

    public function sendSurveyDoubleOptInEmail($email, $actionUrl, $unsubscribeUrl): void
    {
        Mail::to($email)->queue(new SurveyDoubleOptInMail($actionUrl, $unsubscribeUrl));
    }

    public function sendSubscriptionDoubleOptInEmail($email, $actionUrl, $unsubscribeUrl): void
    {
        Mail::to($email)->queue(new SubscriptionDoubleOptInMail($actionUrl, $unsubscribeUrl));
    }

    public function sendCreatePostEmail($email, $message, $unsubscribeUrl): void
    {
        Mail::to($email)->queue(new CreatePostEmail($message, $unsubscribeUrl));
    }

    public function sendCreateCommentEmail($email, $unsubscribeUrl): void
    {
        Mail::to($email)->queue(new CreateCommentEmail($unsubscribeUrl));
    }

    public function sendIndividualSupportMail($email, $userEmail): void
    {
        Mail::raw('Der Benutzer mit der E-Mail-Adresse ' . $userEmail . ' hat gerade für den individuellen Support bezahlt.',
            function ($message) use ($email) {
                $message->to($email);
                $message->subject('Neue individuelle Support-Anfrage');
            });
    }

    public function sendEmailChangeEmail($email, $code, $unsubscribeUrl): void
    {
        Mail::to($email)->queue(new ChangeEmailEmail($code, $unsubscribeUrl));
    }

    public function sendCoachingQuestionEmail($email, $title, $fullName, $userEmail, $content): void
    {
        Mail::to($email)->queue(new CoachingQuestionEmail($title, $fullName, $userEmail, $content));
    }

    public function sendEmailCoachingApplication($name, $email, $phoneNumber, $message, $type): void
    {
        Mail::to(env('ADMIN_EMAIL'))->queue(new CoachingApplicationEmail($name, $email, $phoneNumber, $message, $type));
    }

    public function sendQRCodeEmail($email, $qrCodes, $unsubscribeUrl): void
    {
        Mail::to($email)->send(new QRCodeEmail('<p>Hallo,</p>
<p>Dies sind Ihre Traderista-Empfehlungslinks als QR-CODE.<br />Sie k&ouml;nnen sie herunterladen und verwenden, um f&uuml;r Ihr Konto zu werben.</p>', $qrCodes, $unsubscribeUrl));
    }


    public function sendUnsubscribeMailConfirmation($email): void
    {
        Mail::to($email)->queue(new UnsubscribeEmailConfirmation());
    }

    public function sendEBookSubscriptionEmail($email, $actionUrl): void
    {
        Mail::to($email)->queue(new EBookSubscriptionMail($actionUrl));
    }

    public function sendWebinarSuccessRegistrationEmail($email, $actionUrl): void
    {
        Mail::to($email)->queue(new WebinarRegistrationEmail($actionUrl));
    }

    public function sendMarketingEmail($emails): void
    {
        foreach ($emails as $email) {
            try {
                $text = str_replace("{{name}}", $email['name'], $email['emailObj']->text);
                $emailObj = $email['emailObj'];
                $emailObj->text = $text;
                Mail::to($email['email'])->send(new MarketingEmail($emailObj, $email['unsubscribeUrl']));
            } catch (\Exception $e) {
                \Log::error($e);
            }
        }
    }

    public function sendOneDayAccessEmail($list): void
    {
        foreach ($list as $account) {
            try {
                Mail::to($account[0])->send(new OneDayAccessEmail($account[2]));
            } catch (\Exception $e) {
                \Log::error($e);
            }
        }
    }

    public function sendBookPaymentEmail($email, $url): void
    {
        Mail::to($email)->queue(new BookPaymentEmail($url));
    }

    public function sendSuccessfulPaymentEmail($email): void
    {
        Mail::to($email)->queue(new SuccessfulPaymentEmail());
    }

}
