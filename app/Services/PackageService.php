<?php

namespace App\Services;

use App\Exceptions\AuthorizationException;
use App\Exceptions\UserSubscriptionException;
use App\Models\Package;
use App\Models\User;
use App\Utils\RequestUtils;
use Exception;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class PackageService
{
    private SubscriptionService $subscriptionService;
    private RequestUtils $requestUtils;
    private Package $packageDao;

    public function __construct(Package $package, RequestUtils $requestUtils, SubscriptionService $subscriptionService)
    {
        $this->packageDao = $package;
        $this->requestUtils = $requestUtils;
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * @param $rootValue
     * @param array $args
     * @param GraphQLContext|null $context
     * @param ResolveInfo $resolveInfo
     *
     * @return Package
     * @throws Exception
     */
    public function resolve($rootValue, array $args, GraphQLContext $context = null, ResolveInfo $resolveInfo): Package
    {
        $userDetails = $this->requestUtils->getUserDetails();
        $userId = $userDetails->id;

        // Get the subscription of the authenticated user
        $userSubscriptions = $this->subscriptionService->getSubscriptionByUserId($userId);

        // Check if there is more than one subscription
        if (count($userSubscriptions) > 1) {
            throw new UserSubscriptionException;
        }
        $subscription = $userSubscriptions['0'];

        // Check if the requested package is not the one that user has already subscribed
        $packageId = (int)$args['id'];
        $this->canUserAccessResource($packageId, $subscription->packageId);

        return $this->packageDao->getPackage($args['id']);
    }

    /**
     * @throws AuthorizationException
     */
    public function canUserAccessResource(int $packageId, int $subPackageId)
    {
        if ($subPackageId != $packageId) {
            throw new AuthorizationException();
        }
    }

    public function getPackagePrice($package, $type) {
        $numUsers = User::count();

        $packagePrice = null;
        if ($type === "MONTHLY") {
            $packagePrice = ($numUsers < 1001) ? $package->discountedMonthlyPrice : $package->monthlyPrice;
        } else if ($type === "ANNUAL") {
            $packagePrice = ($numUsers < 1001) ? $package->discountedAnnualPrice : $package->annualPrice;
        }

        return $packagePrice;
    }
}
