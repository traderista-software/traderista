<?php

// app/Services/FacebookService.php
namespace App\Services;

use FacebookAds\Api;
use FacebookAds\Exception\Exception;
use FacebookAds\Object\LeadgenForm;
use FacebookAds\Object\Page;
use Illuminate\Support\Facades\Log;

class FacebookService
{
    protected $api;

    public function __construct()
    {
        $this->api = Api::init(
            env('FACEBOOK_APP_ID'),
            env('FACEBOOK_APP_SECRET'),
            env('FACEBOOK_ACCESS_TOKEN')
        );
    }

    public function subscribeToWebhook()
    {
        try {
            $page = new Page(env('FACEBOOK_PAGE_ID'));
            $page->createSubscribedApp([
                'subscribed_fields' => 'leadgen',
            ],[
                'subscribed_fields' => 'leadgen',
            ]);

            return "Webhook subscribed successfully.";
        } catch (Exception $e) {
            // Error handling
            Log::info($e);
        }
    }
}
