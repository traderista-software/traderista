<?php

namespace App\Utils;

use Illuminate\Support\Str;

class TokenGenerator
{


    public static function generateToken(): String
    {
        return (string) Str::uuid();
    }
}
