<?php


namespace App\Utils;


use App\Models\User;
use Carbon\Carbon;

class UserUtils
{
    static public function setActiveSubscriptions($user): User
    {

        $dateNow = Carbon::now();

        $userSubscriptions = $user->subscriptions;

        $activeSubscriptions = array();
        foreach ($userSubscriptions as $sub) {
            if ($sub['status'] == 'CONFIRMED' || ($sub['status'] == 'CANCELED' && $dateNow->isBefore(Carbon::parse($sub['expiryDate'])))) {
                array_push($activeSubscriptions, $sub);
            }
        }
        $user->activeSubscriptions = $activeSubscriptions;
        return $user;
    }

}
