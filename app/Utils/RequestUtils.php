<?php

namespace App\Utils;

use Exception;
use Illuminate\Http\Request;

class RequestUtils
{
    protected Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @throws Exception
     *
     */
    public function getUserDetails()
    {
        return $this->request->user();
    }

}
