<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Trade
 *
 * @property int $id
 * @property int|null $postId
 * @property string $companySymbol
 * @property string $position
 * @property string $entryDate
 * @property string|null $tradeType
 * @property string|null $stockType
 * @property float $price
 * @property int|null $amountPercentage
 * @property string $leverage
 * @property float $stopLoss
 * @property float $takeProfit
 * @property string $status
 * @property Carbon|null $cTimestamp
 * @property Carbon|null $mTimestamp
 * @method static Builder|Trade newModelQuery()
 * @method static Builder|Trade newQuery()
 * @method static Builder|Trade query()
 * @method static Builder|Trade whereAmountPercentage($value)
 * @method static Builder|Trade whereCTimestamp($value)
 * @method static Builder|Trade whereCompanySymbol($value)
 * @method static Builder|Trade whereEntryDate($value)
 * @method static Builder|Trade whereId($value)
 * @method static Builder|Trade whereLeverage($value)
 * @method static Builder|Trade whereMTimestamp($value)
 * @method static Builder|Trade wherePosition($value)
 * @method static Builder|Trade wherePostId($value)
 * @method static Builder|Trade wherePrice($value)
 * @method static Builder|Trade whereStatus($value)
 * @method static Builder|Trade whereStockType($value)
 * @method static Builder|Trade whereStopLoss($value)
 * @method static Builder|Trade whereTakeProfit($value)
 * @method static Builder|Trade whereTradeType($value)
 * @mixin Eloquent
 * @property int|null $post_id
 * @property string $company_symbol
 * @property-write  ParentPackage $company
 * @property-read Post|null $post
 */
class Trade extends Model
{
    protected $table = 'td_trade';
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class, 'company_symbol', 'symbol');
    }
}
