<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\BlogLike
 *
 * @property int $id
 * @property int $postId
 * @property string|null $ip
 * @property-read BlogPost $blogPost
 * @method static Builder|BlogLike newModelQuery()
 * @method static Builder|BlogLike newQuery()
 * @method static Builder|BlogLike query()
 * @method static Builder|BlogLike whereId($value)
 * @method static Builder|BlogLike whereIp($value)
 * @method static Builder|BlogLike wherePostId($value)
 * @mixin Eloquent
 * @property int $blog_post_id
 * @property int $user_id
 * @property-read User $user
 * @method static Builder|BlogLike whereBlogPostId($value)
 * @method static Builder|BlogLike whereUserId($value)
 */
class BlogLike extends Model
{
    protected $table = 'sc_blog_like';
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    public function blogPost(): BelongsTo
    {
        return $this->belongsTo(BlogPost::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
