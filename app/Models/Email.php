<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * App\Models\Email
 *
 * @property int $id
 * @property string $subject
 * @property string $text
 * @property string $type
 * @property int|null $orderNumber
 * @property-read Collection|UserEmail[] $userEmails
 * @property-read int|null $user_emails_count
 * @method static Builder|Email newModelQuery()
 * @method static Builder|Email newQuery()
 * @method static Builder|Email query()
 * @method static Builder|Email whereId($value)
 * @method static Builder|Email whereOrderNumber($value)
 * @method static Builder|Email whereSubject($value)
 * @method static Builder|Email whereText($value)
 * @method static Builder|Email whereType($value)
 * @mixin Eloquent
 */
class Email extends Model
{
    use HasFactory;

    protected $table = "mk_email";
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    /**
     * Get all users of the corresponding email.
     */
    public function userEmails(): MorphToMany
    {
        return $this->morphToMany(UserEmail::class, 'emails');
    }
}
