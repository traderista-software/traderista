<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ChangeEmailRequest
 *
 * @method static Builder|ChangeEmailRequest newModelQuery()
 * @method static Builder|ChangeEmailRequest newQuery()
 * @method static Builder|ChangeEmailRequest query()
 * @mixin Eloquent
 */
class ChangeEmailRequest extends Model
{
    use HasFactory;
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    protected $fillable = [
        'user_id',
        'email',
        'verification_code'
    ];
}
