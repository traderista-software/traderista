<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Banner
 *
 * @property int $id
 * @property string $url
 * @method static Builder|Banner newModelQuery()
 * @method static Builder|Banner newQuery()
 * @method static Builder|Banner query()
 * @method static Builder|Banner whereId($value)
 * @method static Builder|Banner whereUrl($value)
 * @method static Builder|Banner whereStatus($value)
 * @mixin Eloquent
 */
class Banner extends Model
{

    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';
    use HasFactory;
}
