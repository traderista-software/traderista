<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * App\Models\UserTrade
 *
 * @property-read Collection|Post[] $posts
 * @property-read int|null $posts_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|UserPost newModelQuery()
 * @method static Builder|UserPost newQuery()
 * @method static Builder|UserPost query()
 * @mixin Eloquent
 */
class UserTrade extends Model
{
    protected $table = 'td_user_trade';
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';
    use HasFactory;

    /**
     * Get all the trades that are assigned this relation.
     */
    public function trade(): BelongsTo
    {
        return $this->belongsTo(Post::class, 'post_id');
    }

    /**
     * Get all the users that are assigned this relation.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
