<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\UserSubscription
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $package_id
 * @property string $paypal_subscription_id
 * @property string|null $startDate
 * @property string|null $endDate
 * @property string $duration
 * @property int|null $price
 * @property string|null $type
 * @property string|null $paypalDate
 * @property string $status
 * @property Carbon $cTimestamp
 * @property Carbon $mTimestamp
 * @property-read Package $package
 * @property-read User|null $user
 * @method static Builder|UserSubscription newModelQuery()
 * @method static Builder|UserSubscription newQuery()
 * @method static Builder|UserSubscription query()
 * @method static Builder|UserSubscription whereCTimestamp($value)
 * @method static Builder|UserSubscription whereDuration($value)
 * @method static Builder|UserSubscription whereEndDate($value)
 * @method static Builder|UserSubscription whereId($value)
 * @method static Builder|UserSubscription whereMTimestamp($value)
 * @method static Builder|UserSubscription wherePackageId($value)
 * @method static Builder|UserSubscription wherePaypalDate($value)
 * @method static Builder|UserSubscription wherePaypalSubscriptionId($value)
 * @method static Builder|UserSubscription wherePrice($value)
 * @method static Builder|UserSubscription whereStartDate($value)
 * @method static Builder|UserSubscription whereStatus($value)
 * @method static Builder|UserSubscription whereType($value)
 * @method static Builder|UserSubscription whereUserId($value)
 * @mixin Eloquent
 */
class UserSubscription extends Model
{
    protected $table = "us_user_subscription";

    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    protected $dates = ['lastPaymentDate'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'startDate',
        'endDate',
        'paypal_subscription_id',
        'duration',
        'package_id',
        'price',
        'type',
        'paypalDate',
        'lastPaymentDate',
        'expiryDate',
        'status'
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * UserSubscription Model Functions
     */

    /**
     * @param array $subscription
     *
     * @return UserSubscription
     */
    public function createSubscription(array $subscription): UserSubscription
    {
        $subscription = $subscription['input'];
        return UserSubscription::create($subscription);
    }

    /**
     * @return BelongsTo
     */
    public function package(): BelongsTo
    {
        return $this->belongsTo(Package::class);
    }

    public function calculateExpiryDateFromLastPayment(): Carbon
    {
        $extraDays = 0;
        if ($this->duration === 'MONTHLY') {
            $extraDays = 30;
        } else if ($this->duration === 'ANNUAL') {
            $extraDays = 365;
        }

        if($this->lastPaymentDate !== null) {
            $expiryDate = $this->lastPaymentDate->addDays($extraDays);
        } else {
            $expiryDate = $this->cTimestamp->addDays($extraDays);
        }

        return $expiryDate;
    }
}
