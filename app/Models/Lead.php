<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Lead
 *
 * @property int $id
 * @property string $email
 * @method static Builder|Lead newModelQuery()
 * @method static Builder|Lead newQuery()
 * @method static Builder|Lead query()
 * @method static Builder|Lead whereEmail($value)
 * @method static Builder|Lead whereId($value)
 * @mixin Eloquent
 * @property int $has_unsubscribed
 * @property Carbon $cTimestamp
 * @property Carbon|null $mTimestamp
 * @method static Builder|Lead whereCTimestamp($value)
 * @method static Builder|Lead whereHasUnsubscribed($value)
 * @method static Builder|Lead whereMTimestamp($value)
 */
class Lead extends Model
{
    use HasFactory;

    protected $table = "mk_lead";
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';
}
