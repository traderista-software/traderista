<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\ParentPackage
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $image_path
 * @property-read Collection|Package[] $packages
 * @property-read int|null $packages_count
 * @method static Builder|ParentPackage newModelQuery()
 * @method static Builder|ParentPackage newQuery()
 * @method static Builder|ParentPackage query()
 * @method static Builder|ParentPackage whereDescription($value)
 * @method static Builder|ParentPackage whereId($value)
 * @method static Builder|ParentPackage whereName($value)
 * @mixin Eloquent
 */
class ParentPackage extends Model
{
    protected $table = "fi_parent_package";
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    public function packages(): HasMany
    {
        return $this->hasMany(Package::class, 'parentId', 'id');
    }
}
