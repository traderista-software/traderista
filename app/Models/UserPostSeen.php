<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\UserPostSeen
 *
 * @method static Builder|UserPostSeen newModelQuery()
 * @method static Builder|UserPostSeen newQuery()
 * @method static Builder|UserPostSeen query()
 * @mixin Eloquent
 * @property int $id
 * @property int $user_id
 * @property int $post_id
 * @property int|null $counter
 * @property Carbon|null $cTimestamp
 * @property Carbon $mTimestamp
 * @property-read Post $post
 * @property-read User $user
 * @method static Builder|UserPostSeen whereCTimestamp($value)
 * @method static Builder|UserPostSeen whereCounter($value)
 * @method static Builder|UserPostSeen whereId($value)
 * @method static Builder|UserPostSeen whereMTimestamp($value)
 * @method static Builder|UserPostSeen wherePostId($value)
 * @method static Builder|UserPostSeen whereUserId($value)
 */
class UserPostSeen extends Model
{
    protected $table = 'sc_user_post_seen';
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';
    use HasFactory;

    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
