<?php


namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\UserProductVideo
 * @method static Builder|UserInboxMessage newModelQuery()
 * @method static Builder|UserInboxMessage newQuery()
 * @method static Builder|UserInboxMessage query()
 * @mixin Eloquent
 */
class UserProductVideo extends Model
{
    protected $table = "mk_product_funnel_user_video";
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function productVideo(): BelongsTo
    {
        return $this->belongsTo(ProductFunnelVideo::class, 'product_video_id', 'id');
    }
}
