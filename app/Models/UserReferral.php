<?php

namespace App\Models;

use Database\Factories\UserFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Support\Carbon;

/**
 * App\Models\User
 *
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static UserFactory factory(...$parameters)
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @mixin Eloquent
 * @property-read User $invitedUser
 * @property-read User $referrer
 *  * @method static Builder|User whereActiveSubscriptionId($value)
 * @method static Builder|User whereAvatar($value)
 * @method static Builder|User whereCTimestamp($value)
 * @method static Builder|User whereCoinAmount($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailNotification($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereFriendReferralCode($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereMTimestamp($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePhoneNumber($value)
 * @method static Builder|User wherePhoneNumberCode($value)
 * @method static Builder|User wherePhoneNumberVerifiedAt($value)
 * @method static Builder|User whereReferralCode($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRole($value)
 * @method static Builder|User whereSmsNotification($value)
 * @method static Builder|User whereSurname($value)
 * @property int $referrer_id
 * @property int $invited_user_id
 * @property int $coin_reward
 * @property int $fiat_reward
 * @property int $verified
 * @property Carbon $cTimestamp
 * @method static Builder|UserReferral whereCoinReward($value)
 * @method static Builder|UserReferral whereFiatReward($value)
 * @method static Builder|UserReferral whereInvitedUserId($value)
 * @method static Builder|UserReferral whereReferrerId($value)
 * @method static Builder|UserReferral whereVerified($value)
 * @property int $id
 * @property Carbon $mTimestamp
 */
class UserReferral extends Model
{
    use HasFactory;

    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    protected $dates = ['valid_from_date'];

    protected $fillable = [
        'payment_id',
        'coin_reward',
        'fiat_reward',
        'referrer_id',
        'invited_user_id',
        'stage',
        'description',
        'valid_from_date',
        'verified'
    ];

    public function referrer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'referrer_id');
    }

    public function invitedUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'invited_user_id');
    }
}
