<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\UserInboxMessage

 * @method static Builder|UserInboxMessage newModelQuery()
 * @method static Builder|UserInboxMessage newQuery()
 * @method static Builder|UserInboxMessage query()
 * @mixin Eloquent
 */
class UserInboxMessage extends Model
{
    protected $table = "mk_user_inbox_message";
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function inboxMessage(): BelongsTo
    {
        return $this->belongsTo(InboxMessage::class, 'message_id', 'id');
    }
}
