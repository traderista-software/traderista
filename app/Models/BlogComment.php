<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\BlogComment
 *
 * @property int $id
 * @property int $postId
 * @property string|null $content
 * @property string|null $ip
 * @property-read BlogPost $blogPost
 * @method static Builder|BlogComment newModelQuery()
 * @method static Builder|BlogComment newQuery()
 * @method static Builder|BlogComment query()
 * @method static Builder|BlogComment whereContent($value)
 * @method static Builder|BlogComment whereId($value)
 * @method static Builder|BlogComment whereIp($value)
 * @method static Builder|BlogComment wherePostId($value)
 * @mixin Eloquent
 * @property int $blog_post_id
 * @property int $user_id
 * @property string $date_time
 * @property Carbon $cTimestamp
 * @property Carbon $mTimestamp
 * @property-read User $user
 * @method static Builder|BlogComment whereBlogPostId($value)
 * @method static Builder|BlogComment whereCTimestamp($value)
 * @method static Builder|BlogComment whereDateTime($value)
 * @method static Builder|BlogComment whereMTimestamp($value)
 * @method static Builder|BlogComment whereUserId($value)
 */
class BlogComment extends Model
{
    protected $table = 'sc_blog_comment';
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    public function blogPost(): BelongsTo
    {
        return $this->belongsTo(BlogPost::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
