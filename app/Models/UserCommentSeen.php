<?php

namespace App\Models;

use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * App\Models\UserCommentSeen
 *
 * @property int $userId
 * @property int $commentId
 * @property int|null $counter
 * @property-read Collection|Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|UserCommentSeen newModelQuery()
 * @method static Builder|UserCommentSeen newQuery()
 * @method static Builder|UserCommentSeen query()
 * @method static Builder|UserCommentSeen whereCommentId($value)
 * @method static Builder|UserCommentSeen whereCounter($value)
 * @method static Builder|UserCommentSeen whereUserId($value)
 * @mixin Eloquent
 */
class UserCommentSeen extends Model
{
    protected $table = 'sc_user_comment_seen';
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    /**
     * Get all of the users that are assigned this relation.
     */
    public function users(): MorphToMany
    {
        return $this->morphedByMany(User::class, "users");
    }

    /**
     * Get all of the comments that are assigned this relation.
     */
    public function comments(): MorphToMany
    {
        return $this->morphedByMany(Comment::class, "comments");
    }
}
