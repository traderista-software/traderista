<?php

namespace App\Models;

use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\UserEmail
 *
 * @property int $userId
 * @property int $emailId
 * @property Carbon $cTimestamp
 * @property-read Collection|Email[] $emails
 * @property-read int|null $emails_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|UserEmail newModelQuery()
 * @method static Builder|UserEmail newQuery()
 * @method static Builder|UserEmail query()
 * @method static Builder|UserEmail whereCTimestamp($value)
 * @method static Builder|UserEmail whereEmailId($value)
 * @method static Builder|UserEmail whereUserId($value)
 * @mixin Eloquent
 */
class UserEmail extends Model
{
    use HasFactory;

    protected $table = "mk_user_email";
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    /**
     * Get all of the emails that are assigned this relation.
     */
    public function emails(): MorphToMany
    {
        return $this->morphedByMany(Email::class, "emails");
    }

    /**
     * Get all of the users that are assigned this relation.
     */
    public function users(): MorphToMany
    {
        return $this->morphedByMany(User::class, "users");
    }
}
