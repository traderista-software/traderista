<?php


namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\InboxMessage
 *
 * @property int $id
 * @property int|null $parentId
 * @property string $title
 * @property string|null $body
 * @property int $day
 * @property int $priority
 * @property Carbon $cTimestamp
 * @property Carbon $mTimestamp
 * @method static Builder|InboxMessage newModelQuery()
 * @method static Builder|InboxMessage newQuery()
 * @method static Builder|InboxMessage query()
 * @mixin Eloquent
 */
class InboxMessage extends Model
{
    protected $table = "mk_inbox_message";
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;
}
