<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Package
 *
 * @property int $id
 * @property int|null $parentId
 * @property string $name
 * @property string|null $description
 * @property string $type
 * @property int $minCapital
 * @property int|null $monthlyPrice
 * @property int|null $discountedMonthlyPrice
 * @property int|null $testPhasePrice
 * @property int $annualPrice
 * @property int|null $discountedAnnualPrice
 * @property int $priority
 * @property Carbon $cTimestamp
 * @property Carbon $mTimestamp
 * @property string|null $monthlyPlanId
 * @property string|null $annuallyPlanId
 * @property string|null $image_path
 * @property-read ParentPackage|null $parentPackage
 * @method static Builder|Package newModelQuery()
 * @method static Builder|Package newQuery()
 * @method static Builder|Package query()
 * @method static Builder|Package whereAnnualPrice($value)
 * @method static Builder|Package whereAnnuallyPlanId($value)
 * @method static Builder|Package whereCTimestamp($value)
 * @method static Builder|Package whereDescription($value)
 * @method static Builder|Package whereDiscountedAnnualPrice($value)
 * @method static Builder|Package whereDiscountedMonthlyPrice($value)
 * @method static Builder|Package whereId($value)
 * @method static Builder|Package whereMTimestamp($value)
 * @method static Builder|Package whereMinCapital($value)
 * @method static Builder|Package whereMonthlyPlanId($value)
 * @method static Builder|Package whereMonthlyPrice($value)
 * @method static Builder|Package whereName($value)
 * @method static Builder|Package whereParentId($value)
 * @method static Builder|Package wherePriority($value)
 * @method static Builder|Package whereTestPhasePrice($value)
 * @method static Builder|Package whereType($value)
 * @mixin Eloquent
 * @property string|null $stocksSupported
 * @property string $color_variant
 * @method static Builder|Package whereColorVariant($value)
 * @method static Builder|Package whereStocksSupported($value)
 */
class Package extends Model
{
    protected $table = "fi_package";
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    public function parentPackage(): BelongsTo
    {
        return $this->belongsTo(ParentPackage::class, 'parentId', 'id');
    }

    /**
     * Package Model Functions
     */

    public function getPackage(int $packageId)
    {
        return Package::find($packageId);
    }
}
