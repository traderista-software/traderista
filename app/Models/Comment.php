<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Comment
 *
 * @property int $id
 * @property int $postId
 * @property string $content
 * @property-read Post $post
 * @property-read Collection|UserCommentSeen[] $userSeenComments
 * @property-read int|null $user_seen_comments_count
 * @method static Builder|Comment newModelQuery()
 * @method static Builder|Comment newQuery()
 * @method static Builder|Comment query()
 * @method static Builder|Comment whereContent($value)
 * @method static Builder|Comment whereId($value)
 * @method static Builder|Comment wherePostId($value)
 * @mixin Eloquent
 * @property int $post_id
 * @property int $user_id
 * @property string $date_time
 * @property Carbon $cTimestamp
 * @property Carbon $mTimestamp
 * @property-read User $author
 * @method static Builder|Comment whereCTimestamp($value)
 * @method static Builder|Comment whereDateTime($value)
 * @method static Builder|Comment whereMTimestamp($value)
 * @method static Builder|Comment whereUserId($value)
 */
class Comment extends Model
{
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content',
        'date_time'
    ];

    public function post(): BelongsTo
    {
        return $this->belongsTo(Post::class);
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function userSeenComments(): MorphToMany
    {
        return $this->morphToMany(UserCommentSeen::class, "userSeenComments");
    }
}
