<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * App\Models\Post
 *
 * @property int $id
 * @property int $userId
 * @property string $title
 * @property string $content
 * @property-read Collection|Comment[] $comments
 * @property-read int|null $comments_count
 * @method static Builder|Post newModelQuery()
 * @method static Builder|Post newQuery()
 * @method static Builder|Post query()
 * @method static Builder|Post whereContent($value)
 * @method static Builder|Post whereId($value)
 * @method static Builder|Post whereTitle($value)
 * @method static Builder|Post whereUserId($value)
 * @mixin Eloquent
 * @property int $user_id
 * @property string|null $date_time
 * @property string|null $tradeType
 * @property string|null $stockType
 * @property Carbon $cTimestamp
 * @property-read Collection|Trade[] $tradings
 * @property-read int|null $tradings_count
 * @method static Builder|Post whereCTimestamp($value)
 * @method static Builder|Post whereDateTime($value)
 * @method static Builder|Post whereStockType($value)
 * @method static Builder|Post whereTradeType($value)
 * @property-read User $author
 */
class Post extends Model
{
    protected $table = 'sc_post';
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function trade(): HasOne
    {
        return $this->hasOne(Trade::class);
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
