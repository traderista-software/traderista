<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\BlogPost
 *
 * @property int $id
 * @property string $title
 * @property string|null $text
 * @property-read Collection|BlogComment[] $blogComments
 * @property-read int|null $blog_comments_count
 * @property-read Collection|BlogLike[] $blogLikes
 * @property-read int|null $blog_likes_count
 * @method static Builder|BlogPost newModelQuery()
 * @method static Builder|BlogPost newQuery()
 * @method static Builder|BlogPost query()
 * @method static Builder|BlogPost whereId($value)
 * @method static Builder|BlogPost whereText($value)
 * @method static Builder|BlogPost whereTitle($value)
 * @mixin Eloquent
 * @property string|null $text1
 * @property string|null $text2
 * @property string|null $text3
 * @property string|null $text4
 * @property string|null $text5
 * @property string $date_time
 * @property Carbon|null $cTimestamp
 * @property Carbon|null $mTimestamp
 * @method static Builder|BlogPost whereCTimestamp($value)
 * @method static Builder|BlogPost whereDateTime($value)
 * @method static Builder|BlogPost whereMTimestamp($value)
 * @method static Builder|BlogPost whereText1($value)
 * @method static Builder|BlogPost whereText2($value)
 * @method static Builder|BlogPost whereText3($value)
 * @method static Builder|BlogPost whereText4($value)
 * @method static Builder|BlogPost whereText5($value)
 */
class BlogPost extends Model
{
    protected $table = 'sc_blog_post';
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    public function blogLikes(): HasMany
    {
        return $this->hasMany(BlogLike::class);
    }

    public function blogComments(): HasMany
    {
        return $this->hasMany(BlogComment::class);
    }
}
