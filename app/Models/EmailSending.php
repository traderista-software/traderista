<?php


namespace App\Models;


use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\EmailSending
 *
 * @property int $id
 * @property int $email_id
 * @property string $text
 * @method static Builder|Email newModelQuery()
 * @method static Builder|Email newQuery()
 * @method static Builder|Email query()
 * @method static Builder|Email whereId($value)
 * @method static Builder|Email whereOrderNumber($value)
 * @method static Builder|Email whereSubject($value)
 * @method static Builder|Email whereText($value)
 * @method static Builder|Email whereType($value)
 * @mixin Eloquent
 */
class EmailSending extends Model
{
    protected $table = "mk_email_sending";

}
