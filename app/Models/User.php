<?php

namespace App\Models;

use Database\Factories\UserFactory;
use Eloquent;
use App\Mail\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Joselfonseca\LighthouseGraphQLPassport\HasLoggedInTokens;
use Joselfonseca\LighthouseGraphQLPassport\HasSocialLogin;
use App\Mail\MustVerifyEmailGraphQL;
use Joselfonseca\LighthouseGraphQLPassport\Models\SocialProvider;
use Laravel\Passport\Client;
use Laravel\Passport\HasApiTokens;
use Laravel\Passport\Token;

/**
 * App\Models\User
 *
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static UserFactory factory(...$parameters)
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @mixin Eloquent
 * @property int $id
 * @property int|null $active_subscription_id
 * @property string $name
 * @property string $surname
 * @property string|null $phone_number
 * @property string $email
 * @property string $password
 * @property string|null $role
 * @property string|null $referral_code
 * @property string|null $friend_referral_code
 * @property int $coin_amount
 * @property string|null $remember_token
 * @property string|null $phone_number_code
 * @property Carbon|null $email_verified_at
 * @property string|null $phone_number_verified_at
 * @property string|null $avatar
 * @property int $smsNotification
 * @property int $emailNotification
 * @property Carbon $cTimestamp
 * @property Carbon $mTimestamp
 * @property   UserSubscription|null $activeSubscription
 * @property-read Collection|Client[] $clients
 * @property-read int|null $clients_count
 * @property-read Collection|UserReferral[] $referrals
 * @property-read int|null $referrals_count
 * @property-read Collection|SocialProvider[] $socialProviders
 * @property-read int|null $social_providers_count
 * @property-read Collection|UserSubscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @property-read Collection|Token[] $tokens
 * @property-read int|null $tokens_count
 * @property-read Collection|UserEmail[] $userEmails
 * @property-read int|null $user_emails_count
 * @property-read Collection|UserPost[] $userPosts
 * @property-read int|null $user_posts_count
 * @property-read Collection|UserCommentSeen[] $userSeenComments
 * @property-read int|null $user_seen_comments_count
 * @property-read Collection|UserPostSeen[] $userSeenPosts
 * @property-read int|null $user_seen_posts_count
 * @method static Builder|User whereActiveSubscriptionId($value)
 * @method static Builder|User whereAvatar($value)
 * @method static Builder|User whereCTimestamp($value)
 * @method static Builder|User whereCoinAmount($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailNotification($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereFriendReferralCode($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereMTimestamp($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePhoneNumber($value)
 * @method static Builder|User wherePhoneNumberCode($value)
 * @method static Builder|User wherePhoneNumberVerifiedAt($value)
 * @method static Builder|User whereReferralCode($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRole($value)
 * @method static Builder|User whereSmsNotification($value)
 * @method static Builder|User whereSurname($value)
 * @property int $capital
 * @property int $coins_fiat_amount
 * @property-read Collection|CashOutRequest[] $cashOutRequests
 * @property-read int|null $cash_out_requests_count
 * @method static Builder|User whereCapital($value)
 * @method static Builder|User whereCoinsFiatAmount($value)
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, HasSocialLogin, MustVerifyEmailGraphQL, HasLoggedInTokens;

    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'role',
        'email',
        'paypal_email',
        'phone_number',
        'emailToken',
        'password',
        'referral_code',
        'friend_referral_code',
        'coin_amount',
        'coins_fiat_amount'
    ];

    protected $attributes = array(
        'role' => 'user',
        'SMSNotification' => 1,
        'EmailNotification' => 1
    );

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_number_verified_at' => 'datetime',
    ];

    public function subscriptions(): HasMany
    {
        return $this->hasMany(UserSubscription::class);
    }

    public function activeSubscription(): BelongsTo
    {
        return $this->belongsTo(UserSubscription::class, 'active_subscription_id');
    }

    public function referrals(): HasMany
    {
        return $this->hasMany(UserReferral::class, 'referrer_id');
    }

    public function cashOutRequests(): HasMany
    {
        return $this->hasMany(CashOutRequest::class, 'user_id');
    }

    /**
     * Get all emails of the corresponding user.
     */
    public function userEmails(): MorphToMany
    {
        return $this->morphToMany(UserEmail::class, "userEmails");
    }

    public function userPosts(): MorphToMany
    {
        return $this->morphToMany(UserPost::class, "userPosts");
    }

    public function userSeenComments(): MorphToMany
    {
        return $this->morphToMany(UserCommentSeen::class, "userSeenComments");
    }

    public function userSeenPosts(): MorphToMany
    {
        return $this->morphToMany(UserPostSeen::class, "userSeenPosts");
    }

}
