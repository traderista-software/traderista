<?php

namespace App\Models;

use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * App\Models\UserPost
 *
 * @property-read Collection|Post[] $posts
 * @property-read int|null $posts_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|UserPost newModelQuery()
 * @method static Builder|UserPost newQuery()
 * @method static Builder|UserPost query()
 * @mixin Eloquent
 */
class UserPost extends Model
{
    protected $table = 'sc_user_post';
    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';
    use HasFactory;

    /**
     * Get all of the posts that are assigned this relation.
     */
    public function posts(): MorphToMany
    {
        return $this->morphedByMany(Post::class, 'posts');
    }

    /**
     * Get all of the users that are assigned this relation.
     */
    public function users(): MorphToMany
    {
        return $this->morphedByMany(User::class, 'users');
    }
}
