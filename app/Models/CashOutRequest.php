<?php

namespace App\Models;

use App\Models\User;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\CashOutRequest
 *
 * @property-read User $user
 * @method static Builder|CashOutRequest newModelQuery()
 * @method static Builder|CashOutRequest newQuery()
 * @method static Builder|CashOutRequest query()
 * @mixin Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $paypal_email
 * @property int $coin_amount
 * @property int $fiat_amount
 * @property string|null $approvedTimestamp
 * @property Carbon $cTimestamp
 * @property Carbon|null $mTimestamp
 * @method static Builder|CashOutRequest whereApprovedTimestamp($value)
 * @method static Builder|CashOutRequest whereCTimestamp($value)
 * @method static Builder|CashOutRequest whereCoinAmount($value)
 * @method static Builder|CashOutRequest whereFiatAmount($value)
 * @method static Builder|CashOutRequest whereId($value)
 * @method static Builder|CashOutRequest whereMTimestamp($value)
 * @method static Builder|CashOutRequest wherePaypalEmail($value)
 * @method static Builder|CashOutRequest whereUserId($value)
 */
class CashOutRequest extends Model
{

    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'paypal_email',
        'coin_amount',
        'fiat_amount',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
