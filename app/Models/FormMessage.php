<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormMessage extends Model
{
    use HasFactory;

    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';
}
