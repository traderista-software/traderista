<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Lead
 *
 * @property int $id
 * @mixin Eloquent
 */
class FacebookEvent extends Model
{
    use HasFactory;

    protected $table = "facebook_events";
}
