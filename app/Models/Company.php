<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Company
 *
 * @method static Builder|Company newModelQuery()
 * @method static Builder|Company newQuery()
 * @method static Builder|Company query()
 * @mixin Eloquent
 * @property string $symbol
 * @property string $name
 * @property string|null $logo
 * @property string $type
 * @property string $company_symbol
 * @method static Builder|Company whereCompanySymbol($value)
 * @method static Builder|Company whereLogo($value)
 * @method static Builder|Company whereName($value)
 * @method static Builder|Company whereSymbol($value)
 * @method static Builder|Company whereType($value)
 */
class Company extends Model
{
    use HasFactory;

    public function getCompany(int $id)
    {
        return Company::find($id);
    }
    protected $table = 'companies';
    protected $primaryKey = 'symbol';

    public const CREATED_AT = 'cTimestamp';
    public const UPDATED_AT = 'mTimestamp';

     use HasFactory;
}
