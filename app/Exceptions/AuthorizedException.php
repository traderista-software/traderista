<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;

class AuthorizedException extends AuthenticationException
{
    public const MESSAGE = 'Unauthorized.';
    public const CATEGORY = 'authorization';

    public function isClientSafe(): bool
    {
        return true;
    }

    public function getCategory(): string
    {
        return self::CATEGORY;
    }
}
