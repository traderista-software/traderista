<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

</head>

<body>
{!! $text !!}
<hr/>
<table>
    @component('emails/unsubscribe', ['url' => $unsubscribeUrl])
    @endcomponent
</table>
</body>

</html>
<script>
    import ContactButton from "./ContactButton";

    export default {
        components: {ContactButton}
    }
</script>
