<br><br>
<tr class="mt-2">
    <td bgcolor="#ffffff" align="left" class="">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td bgcolor="#ffffff" align="center">
                    <table cellspacing="0" cellpadding="0">
                        <tr style="border-radius: 10px !important;">
                            <td align="center" style="border-radius: 10px;" bgcolor="#DAA520"><a
                                    href="https://traderista-trading.de/contact"
                                    target="_blank"
                                    style="font-size: 14px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; padding: 10px 10px; border-radius: 10px; border: 1px solid #FFA73B; display: inline-block;">JETZT
                                    KONTAKTIEREN</a></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td bgcolor="#ffffff" align="center" valign="top"
                    style="padding: 10px 5px 5px 5px; border-radius: 4px 4px 0 0; color: #111111; font-family: 'Lato', Helvetica, Arial, sans-serif; font-weight: 400; font-size: 20px">
                    <div class="row justify-content-center text-center">
                        <div class="text-center font-20" style="letter-spacing: 4px;">
                            #traderista
                        </div>
                        <div class="text-center font-14 mt-2"
                             style="color: #DAA520 !important; margin-top: 10px !important; font-size: 22px !important;">
                            <a href="https://wa.me/15551234567"
                               style="color: #666666; font-size: 14px !important;text-decoration: none; margin-right: 5px">Whatsapp
                            </a>
                            <a href="mailto:info@traderista-trading.com"
                               style="color: #666666; font-size: 14px !important;text-decoration: none; margin-right: 5px !important;">Email
                            </a>
                            <a href="https://www.instagram.com/traderista/"
                               style="color: #666666; font-size: 14px !important;text-decoration: none; margin-right: 5px !important;">Instagram
                            </a>
                            <a href="https://facebook.com/Traderista-104665101894659/"
                               style="color: #666666; font-size: 14px !important;text-decoration: none; margin-right: 5px !important;">Facebook
                            </a>
                        </div>
                        <div class="text-center text-light font-14"
                             style="color: #666666; margin-top: 10px !important; font-size: 18px !important;">
                            Copyright C 2022 Traderista Trading Service UG, All rights reserved.
                        </div>
                        <div class="text-center text-light font-14" style="color: #666666; font-size: 14px !important;">
                            Sternstr. 5, 53111 Bonn, Germany
                        </div>
                        <div class="text-center text-light font-14" style="color: #666666; font-size: 14px !important;">
                            Du möchtest diese E-Mails nicht mehr erhalten? <a href="{{$url}}">Vom Newsletter
                                abmelden</a>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </td>
</tr>
