<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <style>
        .button {
            border-radius: 2px;
        }

        .button a {
            padding: 8px 12px;
            border: 1px solid #DAA520;
            border-radius: 2px;
            font-family: Helvetica, Arial, sans-serif;
            font-size: 14px;
            color: #ffffff;
            text-decoration: none;
            font-weight: bold;
            display: inline-block;
        }
    </style>
</head>

<body>
Herzlich Willkommen in der Welt von Traderista! 😊
<br><br>
Mit der Anmeldung für dieses Webinar hast Du den Erfolg an der Börse für Dich vorprogrammiert (Achtung: Wortspiel - doch
hierzu später mehr auf weiteren Seiten und in Webinaren 😁)
<br><br>
Wichtig: Damit ich auch sicherstellen kann, dass die Einladung an die richtige Mailadresse versendet wurde, bestätige
Deine Anmeldung bitte hier:
<br><br>
<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td style="border-radius: 2px;" bgcolor="#DAA520">
                        <a href="{{$actionUrl}}" target="_blank"
                           style="padding: 8px 12px; border: 1px solid #DAA520;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
                            Registrierung jetzt abschließen
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br><br>
Außerdem erhältst Du als Geschenk von mir mein E-Book „Stock Market Secrets - extended edition“. Damit kannst Du die
Zeit bis zum Webinar nutzen, um bereits etwas zu lernen. Auf diesen Inhalten bauen wir dann im Online-Training auf! 💪🏽
<br><br>
Anschließend freue ich mich auf unser gemeinsames Webinar 😊
<br><br>
Nochmal zur Erinnerung: Darin lernst Du, wie Du mit 5 Minuten Aktienhandel pro Tag große ETFs und Fondgesellschaften an
der Börse schlagen OHNE Dir vorher Wissen anzueignen oder Erfahrung zu haben.
<br><br>
Stell also sicher, dass Du Deine Anmeldung abschließt!
<br><br>
<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table cellspacing="0" cellpadding="0">
                <tr>
                    <td style="border-radius: 2px;" bgcolor="#DAA520">
                        <a href="{{$actionUrl}}" target="_blank"
                           style="padding: 8px 12px; border: 1px solid #DAA520;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
                            Registrierung jetzt abschließen
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<br><br>
Wichtig! Wenn Du Deine Anmeldung nicht bestätigst, wird Dein Platz wieder freigegeben und Du verpasst diese einmalige
Chance, von einem wahren Profi zu lernen.
Was Du in meinem Webinar erfahren wirst:
<br><br>
👉🏽 "Die Wahrheit über Börsenhandel (und warum die meisten Menschen scheitern)"
<br><br>
👉🏽 "Wie Du geheime Algorithmen findest und konkurrenzlos wirst"
<br><br>
👉🏽 "Wie Du unabhängig vom Auf und Ab an den Märkten wirst und dadurch immer Geld verdienst"
<br><br>
👉🏽 "Wie Du mit der besonderen Stop Loss Strategie deine Gewinne vervielfachst"
<br><br>
👉🏽 "Und weitere der besten Trading-Strategien aus den letzten 24 Jahren"
<br><br>
Bis später im Webinar! 😊
<br><br>
<img src="https://i.ibb.co/jrZMJ0G/Bildschirmfoto-2022-06-01-um-06-21-31-1.png"
     alt="Bildschirmfoto-2022-06-01-um-06-21-31-1" border="0">
</body>

</html>

