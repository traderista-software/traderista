<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <style>
        .button {
            border-radius: 2px;
        }

        .button a {
            padding: 8px 12px;
            border: 1px solid #DAA520;
            border-radius: 2px;
            font-family: Helvetica, Arial, sans-serif;
            font-size: 14px;
            color: #ffffff;
            text-decoration: none;
            font-weight: bold;
            display: inline-block;
        }
    </style>
</head>

<body>
<div>
    <p>Hey Traderista (ja, du gehörst jetzt zu uns 🔥)</p>
    <p>Du nimmst deinen finanziellen Erfolg in die eigene Hand nehmen und dafür feiere ich Dich!</p>
    <p>Ehrlich, es gibt nur wenige Menschen, die es tatsächlich durchziehen - obwohl die Mehrheit diese
        Verantwortung übernehmen möchte...</p>
    <p>Ich will dich dabei unterstützen!</p>
    <p>Und um dem auch Taten folgen zu lassen (so, wie Du es tust), gebe ich Dir einen 24h gratis Zugang!</p>
    <p>Du hast mit einem Klick Zugriff auf das größte und beste Paket von Traderista!</p>
    <p>Ich zeige Dir alle Trades, die für heute wichtig sind!</p>
    <br/>

    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="border-radius: 2px;" bgcolor="#DAA520">
                            <a href="{{$actionUrl}}" target="_blank"
                               style="padding: 8px 12px; border: 1px solid #DAA520;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">
                                Klick hier für Deinen 24h Gratis Zugang!
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br/>

    <p>Und wenn Du Fragen hast, melde Dich gern jederzeit hier oder über den Mitgliederbereich!</p>
    <p>Dein finanzieller Erfolg ist meine Mission!</p>
    <br/>
    <p>Bis gleich😊</p>

</div>
{{--<table>--}}
{{--    @component('emails/unsubscribe', ['url' => $unsubscribeUrl])--}}
{{--    @endcomponent--}}
{{--</table>--}}
<br>
</body>

</html>
