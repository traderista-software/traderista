<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
</head>

<body>
<div>
    <h3>Du willst wissen, ob Du Geld mit Trading verdienen kannst?</h3>
    <h3>Und Du willst Tipps, Tricks und Storys zu Börsenhandel, Geld verdienen, Erfolg, Wohlstand und richtig kuriose Inhalte?</h3>
    <h3>Dann darf ich Dir mehr dazu erzählen, sobald Du auf folgenden Button klickst… deutsche Gesetze halt</h3>

    <table>
        <tr>
            <td bgcolor="#ffffff" align="left">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#ffffff" align="center">
                            <table cellspacing="0" cellpadding="0">
                                <tr style="border-radius: 10px !important;">
                                    <td align="center" style="border-radius: 10px;" bgcolor="#DAA520"><a
                                            href="{{$actionUrl}}"
                                            target="_blank"
                                            style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; padding: 10px 20px; border-radius: 10px; border: 1px solid #FFA73B; display: inline-block;">
                                            Jetzt bestätigen</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <h3>Ich sende Dir deshalb anschließend ab und zu eine Nachricht und hoffe, dass das für dich in Ordnung ist
        😊</h3>
    <h3>Die erste Geschichte erzähle ich Dir morgen… und wie ich zum Traden gekommen bin. Damit Du siehst, dass das auch für Dich möglich sein wird! 🔥</h3>
    <h3>Wenn Du keine weiteren Mails erhalten möchtest, kannst Du Dich selbstverständlich jederzeit wieder abmelden.</h3>
    <h3>Den Link dafür findest Du am Ende jeder Mail. Falls Du Deine Mailadresse nicht auf meiner Webseite eingetragen hast und
        nicht wissen willst, wie viel Geld Du mit Trading verdienen kannst, kannst Du diese Mail einfach ignorieren.</h3>

    <br/>

    <h4>
        <strong>Wichtig:</strong> Es handelt sich dabei nicht um langweilige E-Mails oder nervigen Spam!
    </h4>

    <h4>
        Du erhältst von mir zu Deinem Buch weitere, multimediale und kostenfreie Inhalte, wie z.B.: Coachingsvideos, <br/>
        Audiotracks, interessante Updates, wertvollen und hochrelevanten Content, Tipps & Tricks, Infos zu relevanten <br/>
        Produkten und Events, Sonderaktionen und vieles mehr...

        <a href="https://traderista-trading.de/email-whitelist" target="_blank"
           data-saferedirecturl="https://www.google.com/url?q=https://traderista-trading.de/email-whitelist"><strong>
                <u>Bestätige jetzt Deine Email-Adresse, um kostenlos davon zu profitieren (klicken)!</u>
            </strong></a>
    </h4>

</div>
<table>
    @component('emails/unsubscribe', ['url' => $unsubscribeUrl])
    @endcomponent
</table>
<br>
</body>

</html>
