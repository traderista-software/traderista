<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

</head>

<body>
<p style="font-size: 16px">
    Vielen Dank, dass Du Dich für Traderista und Trading interessierst - schön, dass Du nun ein Traderista bist! 😊
    <br><br>
    Ich zeige Dir, wie Du erfolgreich Geld mit Trading verdienen kannst und Dein Leben veränderst.
    <br><br>
    Bei Traderista geht es nicht um reinen Profit und Geldgier… ich will eine echte Veränderung in Deinem Leben
    bewirken.
    <br><br>
    Wie ich das erreichen will, erfährst Du in diesem Video 😊
    <br><br>
    <a href="https://www.youtube.com/watch?v=kVVCbG0yNxQ&t=12s&ab_channel=TheDuomoInitiative">
        5 Minute Webinar
    </a>

    <br><br>
    Denn Einfachheit und Erfolg machen Dich zum Gewinner!
    <br><br>
    <a href="{{env('API_URL') . '/pricing' }}">Klicken Sie hier, um die Traderista-App jetzt 24 Stunden lang kostenlos
        zu nutzen!</a>
    <br>Ca 90% aller privaten Trader verlieren Geld an der Börse, weil sie kein erfolgreiches System haben. Deshalb
    haben wir bei Traderista ein sehr einfaches System entwickelt, das Tradern hilft, Trading-Kapital aufzubauen und zu
    einem Vermögen zu machen - alles mit nur 10 Minuten pro Tag.
    <br><br>
    <a href="{{env('API_URL') . '/pricing' }}">Sichere Dir für kurze Zeit mein Angebot, die täglichen Trading-Signale
        von Limitless
        Trading für nur 1€ im 1. Monat zu sichern!</a>
    <br><br>
    Dein Hubertus Justus Amadeus <br>

    P.S. Viel Spaß mit Deinem E-Book und beim Anwenden des Gelernten 🔥📈

</p>
<table>
    @component('emails/unsubscribe', ['url' => $unsubscribeUrl])
    @endcomponent
</table>
</body>

</html>
