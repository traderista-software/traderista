<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
</head>

<body>
<div>
    <h3>Du willst wissen, ob Du Geld mit Trading verdienen kannst?</h3>
    <h3>Und Du willst wissen, wie viel Geld Du mit dem einzigartigen Trading-MLM-System verdienen kannst?</h3>
    <h3>Du hast die Fragen im Quiz beantwortet und erh&auml;ltst Deine Ergebnisse sofort &ndash; ich darf sie Dir
        schicken, sobald Du auf diesen Button geklickt hast</h3>

    <table>
        <tr>
            <td bgcolor="#ffffff" align="left">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td bgcolor="#ffffff" align="center">
                            <table cellspacing="0" cellpadding="0">
                                <tr style="border-radius: 10px !important;">
                                    <td align="center" style="border-radius: 10px;" bgcolor="#DAA520"><a
                                            href="{{$actionUrl}}"
                                            target="_blank"
                                            style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-decoration: none; padding: 10px 20px; border-radius: 10px; border: 1px solid #FFA73B; display: inline-block;">
                                            Jetzt bestätigen</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <h3>Ich sende Dir auch anschlie&szlig;end ab und zu eine Nachricht und hoffe, dass das f&uuml;r dich in Ordnung ist
        😊</h3>
    <h3>Wenn Du keine weiteren Mails erhalten m&ouml;chtest, kannst Du Dich selbstverst&auml;ndlich jederzeit wieder
        abmelden.<br/>Den Link daf&uuml;r findest Du am Ende jeder Mail. Falls Du nicht an dem Quiz teilgenommen hast
        und nicht wissen willst, <br/>wie viel Geld Du mit Trading verdienen kannst, kannst Du diese Mail einfach
        ignorieren.</h3>
</div>
<table>
    @component('emails/unsubscribe', ['url' => $unsubscribeUrl])
    @endcomponent
</table>
<br>
</body>

</html>
