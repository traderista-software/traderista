<td bgcolor="#ffffff" align="center"
    style="padding: 20px 30px 30px 30px;">
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" style="border-radius: 3px;"
                bgcolor="#DAA520"><a
                    href="{{ $url }}" target="_blank" style="font-size: 20px;
                                                   font-family: Helvetica, Arial,
                                                   sans-serif; color: #ffffff;
                                                   text-decoration: none;
                                                   color: #ffffff;
                                                   text-decoration: none;
                                                   padding: 15px 25px;
                                                   border-radius: 2px; border: 1px solid #FFA73B;
                                                   display: inline-block;">
                    {{ $slot }}
                </a>
            </td>
        </tr>
    </table>
</td>
