<!DOCTYPE html>
<html lang="de" dir="ltr" mode="light">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="facebook-domain-verification" content="rwgejdr9ta2oi694iwspmz56ttqphm" />

    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    @if(env('APP_ENV') ==='production')
        <script id='pixel-script-poptin'
                src='https://cdn.popt.in/pixel.js?id=38a6fd8950462'
                async='true'></script>
    @else
        <script id='pixel-script-poptin'
                src='https://cdn.popt.in/pixel.js?id=5699997649332'
                async='true'></script>
    @endif


    {{--@if(env('app_env') === 'prod')--}}
<!-- Meta Pixel Code -->
    <!-- Meta Pixel Code -->
    <!-- Meta Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '860638822006498');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=860638822006498&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Meta Pixel Code -->
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=329788355767109&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Meta Pixel Code -->

    {{--@if(env('app_env') === 'prod')--}}
<!-- Hotjar Tracking Code for https://www.traderista-trading.de -->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {hjid: 2931424, hjsv: 6};
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
    {{--    @endif--}}

    <title>Traderista</title>
    <link rel="icon" href="{{ asset('loader.gif') }}">
</head>
<body>
<noscript>
    <strong>
        We're sorry but {{ env('APP_NAME') }} doesn't work properly without JavaScript enabled.
        Please enable it to continue.
    </strong>
</noscript>
<div id="app">

</div>

<script src="{{ mix('js/app.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script
    src="https://www.paypal.com/sdk/js?client-id={{env('PAYPAL_MODE') === 'sandbox' ? env('PAYPAL_SANDBOX_CLIENT_ID') : env('PAYPAL_LIVE_CLIENT_ID')}}&vault=true&intent=subscription"
    data-namespace="paypal_subscriptions"></script>
<script
    src="https://www.paypal.com/sdk/js?client-id={{env('PAYPAL_MODE') === 'sandbox' ? env('PAYPAL_SANDBOX_CLIENT_ID') : env('PAYPAL_LIVE_CLIENT_ID')}}&intent=capture&currency=EUR"
    data-namespace="paypal_orders"></script>
{{--<script--}}
{{--    src="https://www.paypal.com/sdk/js?client-id=AYcKwqWufXl1fEWJN1tGhwAwFNwAqH0k1Za2vak0Jlou2-9e1tn3FFzye-exl4-O3nVPlmuqBchJuss0&vault=true&intent=subscription"></script>--}}
<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function () {
        var mf = document.createElement("script");
        mf.type = "text/javascript";
        mf.defer = true;
        mf.src = "//cdn.mouseflow.com/projects/566a4dd5-c539-4bf2-935d-d7723d315412.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>

<script type="text/javascript" src="https://20339.webinaris.co/iframe/quellcode.min.js?v=4"></script>
<script type="text/javascript">wbnIframe.getInstance().init("wbn-25305-iframe")</script>
<script src="https://player.vimeo.com/api/player.js"></script>

</body>

</html>
