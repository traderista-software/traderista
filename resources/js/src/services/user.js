import {Auth} from "../config/auth";
import {apolloClient} from "../config/apollo";
import ME from '../graphql/queries/me.graphql';
import UPDATE_ACTIVE_SUBSCRIPTION from '../graphql/mutations/updateActiveSubscription.graphql';
import UPDATE_USER from '../graphql/mutations/updateUser.graphql';
import UPDATE_NOTIFICATION_FLAGS from '../graphql/mutations/updateNotificationFlags.graphql';
import SEND_PHONE_NUMBER_CODE from '../graphql/mutations/sendPhoneNumberCode.graphql';
import VERIFY_PHONE_NUMBER from '../graphql/mutations/verifyPhoneNumber.graphql';
import {AuthService} from "./auth";
import GET_USERS from "../graphql/queries/getUsers.graphql";
import GET_USER from "../graphql/queries/getUser.graphql";
import UPDATE_CAPITAL from '../graphql/mutations/updateCapital.graphql';
import USER_COUNT from '../graphql/queries/userCount.graphql';
import SEND_EMAIL_CHANGE_REQUEST from "../graphql/mutations/sendEmailChangeRequest.graphql";
import CHANGE_EMAIL from "../graphql/mutations/changeEmail.graphql";
import UPDATE_INDIVIDUAL_SUPPORT from "../graphql/mutations/updateIndividualSupport.graphql"
import UNSUBSCRIBE from "../graphql/mutations/unsubscribe.graphql"
import moment from "moment";

export const UserService = {

    async getProfile() {
        if (Auth.guestToken()) {
            return this.dummyUser()
        }

        return await apolloClient.query({
            query: ME,
        }).then(async (data) => {
            const profile = data['data']['me']
            Auth.setUser(profile)
            return profile
        }).catch(async err => {
            if (Auth.isAuthed()) {
                await AuthService.refreshToken()
                await this.getProfile()
            }
        })

    },

    async updateActiveSubscription(package_id, duration, paypal_subscription_id) {
        let userId = Auth.id()

        return await apolloClient.mutate({
            mutation: UPDATE_ACTIVE_SUBSCRIPTION,
            variables: {
                input: {
                    id: userId,
                    activeSubscription: {
                        create: {
                            package: {
                                connect: package_id
                            },
                            user: {
                                connect: userId
                            },
                            paypal_subscription_id: paypal_subscription_id,
                            duration: duration,
                            status: 'CONFIRMED'
                        }
                    }
                }
            }
        }).then(data => {
            Auth.setUser(data['data']['updateActiveSubscription'])
            return true
        }).catch(err => {
            return false
        })
    },

    async updateUser(user) {
        if (Auth.guestToken()) {
            return true
        }

        let userId = Auth.id()

        return await apolloClient.mutate({
            mutation: UPDATE_USER,
            variables: {
                user: {
                    id: userId,
                    name: user['name'],
                    surname: user['surname']
                }
            },
        }).then(async (data) => {
            const user = data['data']['updateUser']
            Auth.setUser(user)
            return true
        }).catch(err => {
            return false
        })
    },
    async updateCapital(capital) {
        if (Auth.guestToken()) {
            return true
        }

        let userId = Auth.user()['id']
        return await apolloClient.mutate({
            mutation: UPDATE_CAPITAL,
            variables: {
                input: {
                    id: userId,
                    capital: capital
                }
            },
        }).then(async (data) => {
            let user = Auth.user()
            user['capital'] = capital
            Auth.setUser(user)
            return true
        }).catch(err => {
            return false
        })
    },

    async updateNotificationFlags(values) {
        if (Auth.guestToken()) {
            return true
        }

        let userId = Auth.id()

        return await apolloClient.mutate({
            mutation: UPDATE_NOTIFICATION_FLAGS,
            variables: {
                input: {
                    id: userId,
                    smsNotification: values['smsNotification'],
                    emailNotification: values['emailNotification']
                }
            },
        }).then(async (data) => {
            let user = Auth.user()
            user['smsNotification'] = values['smsNotification']
            user['emailNotification'] = values['emailNotification']
            Auth.setUser(user)
            return true
        }).catch(err => {
            return false
        })
    },
    async sendPhoneNumberCode(phoneNumber) {
        if (Auth.guestToken()) {
            return true
        }

        return await apolloClient.mutate({
            mutation: SEND_PHONE_NUMBER_CODE,
            variables: {
                input: {
                    phone_number: phoneNumber,
                }
            },
        }).then(async (data) => {
            return true
        }).catch(err => {
            return false
        })
    },
    async verifyPhoneNumber(code) {
        if (Auth.guestToken()) {
            return true
        }

        return await apolloClient.mutate({
            mutation: VERIFY_PHONE_NUMBER,
            variables: {
                input: {
                    code: code,
                }
            },
        }).then(async (data) => {
            const phoneNumber = data['data']['verifyPhoneNumber']['phone_number']

            const user = Auth.user()
            user['phone_number'] = phoneNumber;
            user['phone_number_verified_at'] = moment(Date.now()).format("YYYY-MM-DD H:mm:ss");
            Auth.setUser(user)

            return true
        }).catch(err => {
            return false
        })
    },

    async getUsers(currentUsers, page) {
        return await apolloClient.query({
            query: GET_USERS,
            variables: {
                page: page
            }
        }).then(async (data) => {
                let response = data['data']['users']['data'];
                for (const el of response) {
                    if (el['subscriptions']) {
                        el['package_name'] = '';
                        for (var sub of el['subscriptions']) {
                            if (sub['status'] === 'CONFIRMED') {
                                el['package_name'] += sub['package']['parentPackage']['name'] + ' ' +
                                    sub['package']['name'] + ' | '
                            }
                            el['color_variant'] = sub['package']['color_variant']
                        }
                    }

                    // if (el['productLaunchSubscription']) {
                    //     var now = new Date();
                    //     var createdDate = new Date(el['cTimestamp']);
                    //     var diff = Math.floor((now - createdDate) / (1000 * 60 * 60 * 24)) + 1;
                    //
                    //     if (diff <= 4) {
                    //         el['extraNotes'] = " Product Launch Funnel Tag " + diff;
                    //     } else {
                    //         el['extraNotes'] = " Product Launch Funnel Tag finished";
                    //     }
                    // }
                }

                let result = []
                result['data'] = [...currentUsers, ...response]
                result['lastPage'] = data['data']['users']['paginatorInfo']['lastPage']

                return result
            }
        ).catch(err => {
            console.log(err)
            return []
        })
    },

    async getUser(id) {
        return await apolloClient.query({
            query: GET_USER,
            variables: {
                id: id
            }
        }).then(async (data) => {
            let user = data['data']['user'];

            if (user.subscriptions) {
                user.subscriptions.sort(function (a, b) {
                    return b.id - a.id
                })
            }

            return user;
        }).catch(err => {
            console.log(err)
            return {}
        })
    },

    async getNumUsers() {
        return await apolloClient.query({
            query: USER_COUNT,
        }).then(async (data) => {
            return data['data']['userCount'];
        }).catch(err => {
            console.log(err)
            return {}
        })
    },


    async requestEmailChange(email) {
        if (Auth.guestToken()) {
            return 'CODE_SENT'
        }

        return await apolloClient.mutate({
            mutation: SEND_EMAIL_CHANGE_REQUEST,
            variables: {
                email: email,
            }
        }).then(data => {
            return data['data']['sendEmailChangeRequest']['status']
        }).catch(err => {
            const response = err.graphQLErrors[0]

            if (response['extensions']['errors']['status']) {
                return response['extensions']['errors']['status']
            }

            return 'ERROR'
        })
    },

    async changeEmail(email, code) {
        if (Auth.guestToken()) {
            return true
        }

        return await apolloClient.mutate({
            mutation: CHANGE_EMAIL,
            variables: {
                input: {
                    email: email,
                    code: code,
                }
            }
        }).then(data => {
            return true
        }).catch(err => {
            return false
        })
    },

    async updateIndividualSupport() {
        let userId = Auth.id()

        return await apolloClient.mutate({
            mutation: UPDATE_INDIVIDUAL_SUPPORT,
            variables: {
                input: {
                    id: userId,
                    individualSupport: true,
                }
            },
        }).then(async (data) => {
            let user = Auth.user()
            user['individualSupport'] = true;
            Auth.setUser(user)
            return true
        }).catch(err => {
            return false
        })
    },
    async unsubscribeByToken(token) {
        return await apolloClient.mutate({
            mutation: UNSUBSCRIBE,
            variables: {
                token: token
            },
        }).then(async (data) => {
            return true
        }).catch(err => {
            return false
        })
    },
    dummyUser() {
        return {
            "id": "154",
            "name": "Gast",
            "email": "gast@traderista.com",
            "activeSubscription": null,
            "capital": "100000",
            "subscriptions": []
        }
    },
}
