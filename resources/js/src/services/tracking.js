import {event, pageview, purchase} from "vue-gtag";

export const TrackingService = {

    async pageView(screenName, route) {
        try {
            window.fbq('track', 'ViewContent');
            pageview(route);
        } catch (e) {
            console.log(e);
        }
    },

    async buttonClick(buttonName) {
        try {
            window.fbq('trackCustom', buttonName);
            event(buttonName, {
                'event_category': "Button Clicks"
            });
        } catch (e) {
        }
    },

    async completeRegistration() {
        try {
            window.fbq('track', 'CompleteRegistration');
            event('sign_up');
        } catch (e) {
        }
    },

    async generateLead() {
        try {
            window.fbq('track', 'Lead');
            event('generate_lead');
        } catch (e) {
        }
    },

    async login() {
        try {
            window.fbq('track', 'Login');
            event('login');
        } catch (e) {
        }
    },

    async purchase(value) {
        try {
            window.fbq('track', 'Purchase', {currency: "EUR", value: value});
            purchase({
                "value": value
            })
        } catch (e) {
        }
    },

    async initiateCheckout() {
        try {
            window.fbq('track', 'InitiateCheckout');
            event('begin_checkout');
        } catch (e) {
        }
    }
}
