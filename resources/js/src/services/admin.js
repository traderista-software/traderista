import {apolloClient} from "../config/apollo";
import APPROVE_CASH_OUT_REQUEST from '../graphql/mutations/approveCashOutRequest.graphql';
import CASH_OUT_REQUESTS from "../graphql/queries/cashOutRequests.graphql";
import ACTIVE_CASH_OUT_REQUESTS from "../graphql/queries/activeCashOutRequests.graphql";
import GET_GIVEN_REWARDS from "../graphql/queries/getGivenRewards.graphql";
import CREATE_REWARD from "../graphql/mutations/createReward.graphql";
import UPDATE_REWARD from "../graphql/mutations/updateReward.graphql";
import DELETE_REWARD from "../graphql/mutations/deleteReward.graphql";
import GET_ALL_BANNERS from "../graphql/queries/getBanners.graphql";
import DELETE_BANNER from "../graphql/mutations/deleteBanner.graphql"
import CREATE_BANNER from "../graphql/mutations/createBanner.graphql"
import {toBase64} from "../Utils/utils";

export const AdminService = {

    async approveCashOutRequest(id) {
        return await apolloClient.mutate({
            mutation: APPROVE_CASH_OUT_REQUEST,
            variables: {
                input: {
                    id: id
                }
            }
        }).then(async (data) => {
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async getCashOutRequestsHistory(currentRequests, page) {
        return await apolloClient.query({
            query: CASH_OUT_REQUESTS,
            variables: {
                page: page
            }
        }).then(async (data) => {
            let response = data['data']['cashOutRequests']['data']

            let result = []
            result['data'] = [...currentRequests, ...response]
            result['lastPage'] = data['data']['cashOutRequests']['paginatorInfo']['lastPage']

            return result
        }).catch(err => {
            console.log(err)
            return []
        })
    },

    async getActiveCashOutRequests() {
        return await apolloClient.query({
            query: ACTIVE_CASH_OUT_REQUESTS,
        }).then(async (data) => {
            return data['data']['activeCashOutRequests'];
        }).catch(err => {
            console.log(err)
            return []
        })
    },

    async getGivenRewards() {
        return await apolloClient.query({
            query: GET_GIVEN_REWARDS,
        }).then(async (data) => {
            return data['data']['rewards'];
        }).catch(err => {
            console.log(err)
            return []
        })
    },

    async createReward(reward) {
        return await apolloClient.mutate({
            mutation: CREATE_REWARD,
            variables: {
                input: {
                    user_id: parseInt(reward.user_id),
                    coin_amount: parseInt(reward.coin_amount),
                    note: reward.note,
                }
            }
        }).then(async (data) => {
            return data.data.createReward
        }).catch(err => {
            console.log(err)
            return null;
        })
    },
    async updateReward(reward) {
        return await apolloClient.mutate({
            mutation: UPDATE_REWARD,
            variables: {
                id: parseInt(reward.id),
                coin_amount: parseInt(reward.coin_amount),
                note: reward.note,
            }
        }).then(async (data) => {
            return data.data.updateReward
        }).catch(err => {
            console.log(err)
            return null;
        })
    },

    async deleteReward(id) {
        return await apolloClient.mutate({
            mutation: DELETE_REWARD,
            variables: {
                id: id,
            }
        }).then(async (data) => {
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async getAllBanners() {
        return await apolloClient.query({
            query: GET_ALL_BANNERS
        }).then(async (data) => {
            return data.data.banners;
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async deleteBanner(id) {
        return await apolloClient.mutate({
            mutation: DELETE_BANNER,
            variables: {
                id: id,
            }
        }).then(async (data) => {
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },


    async createBanner(banner) {
        var image = null;
        if (!banner.isVideo) {
            image = await toBase64(banner.image);
        }
        return await apolloClient.mutate({
            mutation: CREATE_BANNER,
            variables: {
                input: {
                    image: image,
                    status: banner.status,
                    priority: parseInt(banner.priority),
                    link: banner.link,
                    isVideo: (banner.isVideo === 'true'),
                    url: banner.url,
                }
            }
        }).then(async (data) => {
            return data.data.createBanner
        }).catch(err => {
            console.log(err)
            return null
        })
    },
}
