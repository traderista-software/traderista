import {apolloClient} from "../config/apollo";
import GET_EMAILS from "../graphql/queries/getEmailsForMMS.graphql"
import UPDATE_EMAIL from "../graphql/mutations/updateEmail.graphql"
import CREATE_EMAIL from "../graphql/mutations/createEmail.graphql"
import DELETE_EMAIL from "../graphql/mutations/deleteEmail.graphql"

import GET_MESSAGES from "../graphql/queries/getCoachingMessages.graphql"
import CREATE_MESSAGE from "../graphql/mutations/createMessage.graphql"
import UPDATE_MESSAGE from "../graphql/mutations/updateMessage.graphql"
import DELETE_MESSAGE from "../graphql/mutations/deleteMessage.graphql"

import GET_USER_INBOX_MESSAGES from "../graphql/queries/getUserInboxMessages.graphql"
import EMAIL_SENDING from "../graphql/queries/emailSending.graphql"
import UPDATE_USER_INBOX_MESSAGE_STATUS from "../graphql/mutations/updateInboxMessageStatus.graphql";
import SEND_COACHING_QUESTION_EMAIL from "../graphql/mutations/sendCoachingQuestionEmail.graphql";
import SEND_EMAIL_TEMPLATE from "../graphql/mutations/sendEmailTemplate.graphql";

import SEND_MAIL_FOR_ONE_DAY_ACCESS from "../graphql/mutations/sendMailForOneDayAccess.graphql"
import {Auth} from "../config/auth";

export const MailService = {

    async getEmails() {
        return await apolloClient.query({
            query: GET_EMAILS,
        }).then(async (data) => {
            return data.data.emails;
        }).catch(err => {
            console.log(err)
            return []
        })
    },


    async createEmail(email) {
        return await apolloClient.mutate({
            mutation: CREATE_EMAIL,
            variables: {
                input: {
                    name: email.name,
                    subject: email.subject,
                    text: email.text,
                    type: email.type.toUpperCase(),
                    timePeriodInDays: Number(email.timePeriodInDays),
                    orderNumber: Number(email.orderNumber)
                }
            }
        }).then(async (data) => {
            return data.data.createEmail
        }).catch(err => {
            console.log(err)
            return null
        })
    },

    async updateEmail(id, email) {
        return await apolloClient.mutate({
            mutation: UPDATE_EMAIL,
            variables: {
                id: id,
                name: email.name,
                subject: email.subject,
                text: email.text,
                type: email.type.toUpperCase(),
                timePeriodInDays: Number(email.timePeriodInDays),
                orderNumber: Number(email.orderNumber)
            }
        }).then(async (data) => {
            return data.data.updateEmail
        }).catch(err => {
            console.log(err)
            return null
        })
    },

    async deleteEmail(id) {
        return await apolloClient.mutate({
            mutation: DELETE_EMAIL,
            variables: {
                id: id,
            }
        }).then(async (data) => {
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async getInboxMessages() {
        return await apolloClient.query({
            query: GET_MESSAGES,
        }).then(async (data) => {
            return data.data.inboxMessages;
        }).catch(err => {
            console.log(err)
            return []
        })
    },
    async getUserInboxMessages() {
        return await apolloClient.query({
            query: GET_USER_INBOX_MESSAGES,
        }).then(async (data) => {
            return data.data.userInboxMessages;
        }).catch(err => {
            console.log(err)
            return []
        })
    },

    async createMessage(message) {
        return await apolloClient.mutate({
            mutation: CREATE_MESSAGE,
            variables: {
                input: {
                    title: message.title,
                    body: message.body,
                    day: parseInt(message.day),
                    priority: parseInt(message.priority)
                }
            }
        }).then(async (data) => {
            return data.data.createMessage
        }).catch(err => {
            console.log(err)
            return null
        })
    },


    async updateMessage(id, message) {
        return await apolloClient.mutate({
            mutation: UPDATE_MESSAGE,
            variables: {
                id: id,
                title: message.title,
                body: message.body,
                day: parseInt(message.day),
                priority: parseInt(message.priority)
            }
        }).then(async (data) => {
            return data.data.updateMessage
        }).catch(err => {
            console.log(err)
            return null
        })
    },

    async deleteMessage(id) {
        return await apolloClient.mutate({
            mutation: DELETE_MESSAGE,
            variables: {
                id: id,
            }
        }).then(async (data) => {
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async updateMessageStatus(messageId) {
        return await apolloClient.mutate({
            mutation: UPDATE_USER_INBOX_MESSAGE_STATUS,
            variables: {
                input: {
                    user_id: Auth.id(),
                    message_id: messageId,
                    status: "READ"
                }
            }
        }).then(async (data) => {
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async sendCoachingQuestionEmail(title, content) {
        return await apolloClient.mutate({
            mutation: SEND_COACHING_QUESTION_EMAIL,
            variables: {
                input: {
                    title: title,
                    content: content,
                }
            }
        }).then(async (data) => {
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async sendEmailTemplate(emailId, userTargets, subscriberTargets) {
        return await apolloClient.mutate({
            mutation: SEND_EMAIL_TEMPLATE,
            variables: {
                input: {
                    emailId: emailId,
                    userTargets: userTargets,
                    subscriberTargets: subscriberTargets,
                }
            }
        }).then(async (data) => {
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async getEmailSendingLogs(emailId) {
        return await apolloClient.query({
            query: EMAIL_SENDING,
            variables: {
                email_id: emailId
            }
        }).then(async (data) => {
            return data['data']['email_sending']
        }).catch(err => {
            console.log(err)
            return null
        })
    },
    async sendMailForOneDayTrial(email) {
        return await apolloClient.mutate({
            mutation: SEND_MAIL_FOR_ONE_DAY_ACCESS,
            variables: {
                input: email
            }
        }).then(async (response) => {
            return true;
        }).catch(async (e) => {
            return false;
        });
    }
}
