import {apolloClient} from "../config/apollo";
import GET_SUBSCRIBED_POSTS from "../graphql/queries/getSubscribedPosts.graphql";
import GET_ALL_POSTS_FOR_ADMIN from "../graphql/queries/getAllPostsForAdmin.graphql";
import CREATE_COMMENT from "../graphql/mutations/createComment.graphql";
import SEE_POST from "../graphql/mutations/seePost.graphql";
import DELETE_POST from "../graphql/mutations/deletePost.graphql";
import DELETE_COMMENT from "../graphql/mutations/deleteComment.graphql";
import GET_LATEST_POSTS from "../graphql/queries/getLatestTrades.graphql";
import ADD_POST from "../graphql/mutations/addPost.graphql";
import ENTER_TRADE from "../graphql/mutations/enterTrade.graphql";
import UPDATE_POST from "../graphql/mutations/updatePost.graphql";

import Store from "../store";
import {Auth} from "../config/auth";
import moment from "moment";

export const PostService = {

    async getSubscribedPosts(page) {
        if (Auth.guestToken()) {
            Store.commit('Setting/postsCommit', this.dummyPosts())
            return true
        }

        return await apolloClient.query({
            query: GET_SUBSCRIBED_POSTS,
            variables: {
                page: page
            }
        }).then(async (data) => {
            Store.commit('Setting/pushPostsCommit', data['data']['subscribedPosts'])
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async getLatestTradePosts() {
        if (Auth.guestToken()) {
            Store.commit('Setting/postsCommit', this.dummyPosts())
            return true
        }

        return await apolloClient.query({
            query: GET_LATEST_POSTS
        }).then(async (data) => {
            Store.commit('Setting/postsCommit', data['data']['latestTradePosts'])
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async getAllPostsForAdmin(page) {
        return await apolloClient.query({
            query: GET_ALL_POSTS_FOR_ADMIN,
            variables: {
                page: page
            }
        }).then(async (data) => {
            Store.commit('Setting/pushPostsCommit', data['data']['allPosts'])
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async deletePost(postId) {
        return await apolloClient.mutate({
            mutation: DELETE_POST,
            variables: {
                id: postId,
            }
        }).then(async (data) => {
            // Store.commit('Setting/deleteCommentCommit', {
            //     'postId': postId,
            //     'commentId': id,
            // })
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async createComment(postId, comment) {
        if (Auth.guestToken()) {
            Store.commit('Setting/postCommentCommit', this.dummyComment(comment, postId))
            return true
        }

        return await apolloClient.mutate({
            mutation: CREATE_COMMENT,
            variables: {
                input: {
                    content: comment,
                    date_time: moment(Date.now()).format("YYYY-MM-DD H:mm:ss"),
                    post: {
                        connect: postId,
                    },
                }
            }
        }).then(async (data) => {
            let response = data['data']['createComment'];
            response['post_id'] = postId;
            Store.commit('Setting/postCommentCommit', response)
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async deleteComment(postId, id) {
        if (Auth.guestToken()) {
            Store.commit('Setting/deleteCommentCommit', {
                'postId': postId,
                'commentId': id,
            })
            return true
        }

        return await apolloClient.mutate({
            mutation: DELETE_COMMENT,
            variables: {
                id: id,
            }
        }).then(async (data) => {
            Store.commit('Setting/deleteCommentCommit', {
                'postId': postId,
                'commentId': id,
            })
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async seePost(postId) {
        if (Auth.guestToken()) {
            return true
        }

        const userId = Auth.user()['id']

        return await apolloClient.mutate({
            mutation: SEE_POST,
            variables: {
                input: {
                    user: {
                        connect: userId
                    },
                    post: {
                        connect: postId
                    }
                }
            }
        }).then(async (data) => {
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async createPost(content, trade, stockType, tradeType, allUsers) {
        console.log(stockType, tradeType, allUsers)


        return await apolloClient.mutate({
                mutation: ADD_POST,
                variables: {
                    input: {
                        user: {
                            connect: Auth.id()
                        },
                        content: content,
                        allUsers: allUsers === "true",
                        date_time: moment(Date.now()).format("YYYY-MM-DD H:mm:ss"),
                        trade: trade,
                        stockType: stockType,
                        tradeType: tradeType,
                    }
                }
            }
        ).then((data) => {
            // Store.commit('Setting/addPostCommit', data['data']['createPost'])
            return true;
        }).catch((err) => {
            console.log(err)
            return false;
        })
    },
    async createPosts(posts) {
        for (var post of posts) {
            post.allUsers = post.allUsers === "true";
            post.date_time = moment(Date.now()).format("YYYY-MM-DD H:mm:ss");
            if (post.isShow) {
                post.trade.entryDate = moment(post.trade.entryDate).format("YYYY-MM-DD H:mm:ss");
                post.trade.company.company_symbol = post.trade.company.symbol.toUpperCase();
                post.trade.price = parseFloat(post.trade.price);
                post.trade.leverage = parseInt(post.trade.leverage);
                post.trade.takeProfit = parseFloat(post.trade.takeProfit);
                post.trade.stopLoss = parseFloat(post.trade.stopLoss);
                post.trade.amountPercentage = parseFloat(post.trade.amountPercentage);
                post.stockType = post.trade.stockType;
                post.tradeType = post.trade.tradeType;
            }
        }

        return await apolloClient.mutate({
                mutation: ADD_POST,
                variables: {
                    input: posts
                }
            }
        ).then((data) => {
            // Store.commit('Setting/addPostCommit', data['data']['createPost'])
            return true;
        }).catch((err) => {
            console.log(err)
            return false;
        })
    },

    async editPost(post) {
        return await apolloClient.mutate({
                mutation: UPDATE_POST,
                variables: {
                    input: {
                        id: parseInt(post.id),
                        allUsers: post.allUsers === "true" || post.allUsers,
                        tradeType: post.trade != null ? post.trade.tradeType : null,
                        stockType: post.trade != null ? post.trade.stockType : null,
                        content: post.content,
                        user: {
                            connect: Auth.id()
                        },
                        tradeId: post.trade != null ? post.trade.id : null,
                        trade: post.trade != null ? {
                            position: post.trade.position,
                            entryDate: moment(post.trade.entryDate).format("YYYY-MM-DD H:mm:ss"),
                            tradeType: post.trade.tradeType,
                            stockType: post.trade.stockType,
                            leverage: parseInt(post.trade.leverage),
                            price: parseFloat(post.trade.price),
                            amountPercentage: parseFloat(post.trade.amountPercentage),
                            stopLoss: parseFloat(post.trade.stopLoss),
                            takeProfit: parseFloat(post.trade.takeProfit),
                            company: {
                                symbol: post.trade.company.company_symbol,
                                company_symbol: post.trade.company.company_symbol,
                            },
                        } : null
                    }
                }
            }
        ).then((data) => {
            return true;
        }).catch((err) => {
            console.log(err)
            return false;
        });
    },

    async enterTrade(postId) {
        return await apolloClient.mutate({
                mutation: ENTER_TRADE,
                variables: {
                    input: {
                        user_id: Auth.id(),
                        post_id: postId
                    }
                }
            }
        ).then(async (data) => {
            // Store.commit('Setting/addPostCommit', data['data']['createPost'])
            await Store.dispatch('Setting/completeTradeAction', {
                id: postId,
                user_id: data['data']['enterTrade']['user_id'],
                cTimestamp: data['data']['enterTrade']['cTimestamp']
            });
            return true;
        }).catch((err) => {
            console.log(err)
            return false;
        })
    },


    dummyPosts() {
        return [{
            "id": "114",
            "content": "Dies ist mein zweiter Handel für euch (es ist nur ein Testhandel, kein echter).",
            "cTimestamp": "28 Aug 2021, 14:53",
            "author": {
                "name": "Hubertus",
                "surname": "Mertens"
            },
            "comments": [],
            "trade": {
                "id": "48",
                "position": "LONG",
                "entryDate": "2021-09-04",
                "tradeType": "DAILY",
                "stockType": "INDEX",
                "price": 250,
                "amountPercentage": 25,
                "stopLoss": 110,
                "takeProfit": 360,
                "leverage": 3,
                "status": "OPEN",
                "company": {
                    "symbol": "0",
                    "name": "Tesla",
                    "logo": null,
                    "company_symbol": "TSLA"
                }
            }
        },
            {
                "id": "113",
                "content": "Dies ist mein erster Handel für euch (es ist nur ein Testhandel, kein echter).",
                "cTimestamp": "28 Aug 2021, 14:52",
                "author": {
                    "name": "Hubertus",
                    "surname": "Mertens"
                },
                "comments": [],
                "trade": {
                    "id": "47",
                    "position": "LONG",
                    "entryDate": "2021-09-04",
                    "tradeType": "DAILY",
                    "stockType": "DEUTSCHE",
                    "price": 25,
                    "leverage": 2,
                    "amountPercentage": 15,
                    "stopLoss": 10,
                    "takeProfit": 45,
                    "status": "OPEN",
                    "company": {
                        "symbol": "DJI",
                        "name": "Dow Jones Industrial Average",
                        "logo": null,
                        "company_symbol": "DJI"
                    }
                }
            }]
    },
    dummyComment(comment, postId) {
        return {
            id: "1",
            content: comment,
            post_id: postId,
            author: {
                id: '1',
                name: 'Gast',
                surname: ''
            }
        };
    }
}
