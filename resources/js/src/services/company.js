import {apolloClient} from "../config/apollo";
import GET_COMPANIES_WITH_PAGINATION from "../graphql/queries/getAllCompaniesWithPagination.graphql";
import CREATE_COMPANY from "../graphql/mutations/createCompany.graphql";
import DELETE_COMPANY from "../graphql/mutations/deleteCompany.graphql";
import Store from "../store";

export const CompanyService = {

    async getCompanies(currentCompanies, page) {
        return await apolloClient.query({
            query: GET_COMPANIES_WITH_PAGINATION,
            variables: {
                page: page
            }
        }).then(async (data) => {
            let response = data['data']['allCompaniesWithPagination']['data'];
            let result = []
            result['data'] = [...currentCompanies, ...response]
            result['lastPage'] = data['data']['allCompaniesWithPagination']['paginatorInfo']['lastPage']

            return result
        }).catch(err => {
            console.log(err)
            return []
        })
    },

    async createCompany(company_symbol, name,stockType,symbol) {
        return await apolloClient.mutate({
                mutation: CREATE_COMPANY,
                variables: {
                    input: {
                        company_symbol: company_symbol,
                        name: name,
                        type: stockType,
                        symbol: symbol
                    }
                }
            }
        ).then((data) => {
            // Store.commit('Setting/addPostCommit', data['data']['createPost'])
            return true;
        }).catch((err) => {
            console.log(err)
            return false;
        })
    },

    async deleteCompany(symbol) {
        return await apolloClient.mutate({
            mutation: DELETE_COMPANY,
            variables: {
                symbol : symbol,
            }
        }).then(async (data) => {
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },
}
