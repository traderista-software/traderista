import {apolloClient} from "../config/apollo";
import GET_PACKAGE_BY_ID from '../graphql/queries/getPackageById.graphql';
import GET_PACKAGES from '../graphql/queries/getPackages.graphql';

export const PackageService = {

    async getPackages() {
        return await apolloClient.query({
            query: GET_PACKAGES,
        }).then(async (data) => {
            return data['data']['parentPackages']
        }).catch(err => {
            console.log(err)
            return [];
        })
    },


    async getPackage(id) {
        return await apolloClient.query({
            query: GET_PACKAGE_BY_ID,
            variables: {
                id: id
            }
        }).then(async (data) => {
            return data['data']['package']
        }).catch(err => {
            console.log(err)
            return undefined;
        })
    },
}
