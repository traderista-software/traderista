import {Auth} from "../config/auth";
import LOGIN from "../graphql/mutations/login.graphql"
import REFRESH_TOKEN from "../graphql/mutations/refreshToken.graphql"
import REGISTER from "../graphql/mutations/register.graphql"
import VERIFY_EMAIL from '../graphql/mutations/verifyEmail.graphql';
import SEND_VERIFICATION_EMAIL from '../graphql/mutations/sendVerificationEmail.graphql';
import FORGOT_PASSWORD from '../graphql/mutations/forgotPassword.graphql';
import SET_NEW_PASSWORD from '../graphql/mutations/setNewPassword.graphql';
import CHANGE_PASSWORD from '../graphql/mutations/changePassword.graphql';
import SEND_EMAIL_CHANGE_REQUEST from '../graphql/mutations/sendEmailChangeRequest.graphql';
import CHANGE_EMAIL from '../graphql/mutations/changeEmail.graphql';
import ACTIVATE_ONE_DAY_ACCESS from '../graphql/mutations/activateOneDayAccess.graphql';
import ME from "../graphql/queries/me.graphql"
import Store from "../store";

import {apolloClient, onLogin, onLogout} from "../config/apollo";

export const AuthService = {

    async login(user) {
        var response = await apolloClient.mutate({
            mutation: LOGIN,
            variables: {
                username: user.email,
                password: user.password,
                scope: user.role,
            },
        }).then(async (data) => {

            await Store.dispatch('Setting/authUserAction', {
                auth: true,
                user: data['data']['login']['user']
            })

            Auth.authenticate(data['data']['login'])
            Auth.deleteRegisteringEmail()
            await onLogin(apolloClient)

            var userData = await apolloClient.query({
                query: ME,
            });
            Auth.setUser(userData['data']['me']);

            return true
        }).catch(err => {
            console.log(err)
            return false
        })

        if (response === true) {
            if (user.activate_one_day_access) {
                await apolloClient.mutate({
                    mutation: ACTIVATE_ONE_DAY_ACCESS,
                    variables: {
                        input: {
                            id: Auth.id(),
                            token: user.token,
                            activate_one_day_access: true
                        }
                    }
                })
            }
            return true;
        } else {
            return false;
        }


    },

    async refreshToken() {
        const refresh_token = Auth.refreshToken()
        if (refresh_token !== undefined && refresh_token !== null) {
            return await apolloClient.mutate({
                mutation: REFRESH_TOKEN,
                variables: {
                    refresh_token: refresh_token,
                }
            }).then(async (data) => {
                Auth.authenticate(data['data']['refreshToken'])
                await onLogin(apolloClient)
                return data['data']['refreshToken']
            }).catch(err => {
                console.log(err)
                Auth.clear()
            })
        } else {
            Auth.clear()
        }
    },

    async register(user) {
        return await apolloClient.mutate({
            mutation: REGISTER,
            variables: {
                user: user
            }
        }).then(async (data) => {
            if (data['data'] !== undefined && data['data'] !== null) {
                let status = data['data']['register']['status']

                if (status === 'SUCCESS') {
                    await Store.dispatch('Setting/authUserAction', {
                        auth: true,
                        user: data['data']['register']['tokens']['user']
                    })

                    Auth.authenticate(data['data']['register']['tokens'])
                    Auth.setRegisteringEmail(user['email'])
                    await onLogin(apolloClient)
                }

                return status
            }
        }).catch(err => {
            const response = err.graphQLErrors[0]
            try {
                if (response['extensions']['validation']['input.email']) {
                    return 'EXISTS'
                }
            } catch (e) {
                return 'ERROR'
            }
            return 'ERROR';
        });
    },

    async logout() {
        Auth.clear();
        await Store.dispatch('Setting/resetStateOnLogout');
        await onLogout(apolloClient)
    },

    async verifyEmail(token) {
        return await apolloClient.mutate({
            mutation: VERIFY_EMAIL,
            variables: {
                token: {
                    token: token
                }
            }
        }).then(async data => {
            Auth.deleteRegisteringEmail()
            return true
        }).catch(err => {
            return false
        })
    },

    async sendVerificationEmail() {
        return await apolloClient.mutate({
            mutation: SEND_VERIFICATION_EMAIL,
            variables: {
                email: {
                    email: Auth.registeringEmail()
                }
            }
        }).then(async data => {
            return true
        }).catch(err => {
            return false
        })
    },

    async forgotPassword(email) {
        return await apolloClient.mutate({
            mutation: FORGOT_PASSWORD,
            variables: {
                email: {
                    email: email
                }
            }
        }).then(data => {
            return data['data']['forgotPassword']['status'] === 'EMAIL_SENT'
        }).catch(err => {
            return false
        })
    },

    async resetPassword(email, token, password) {
        return await apolloClient.mutate({
            mutation: SET_NEW_PASSWORD,
            variables: {
                input: {
                    email: email,
                    token: token,
                    password: password,
                    password_confirmation: password,
                }
            }
        }).then(data => {
            return data['data']['updateForgottenPassword']['status'] === 'PASSWORD_UPDATED'
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async changePassword(oldPassword, newPassword, newPasswordConfirmed) {
        return await apolloClient.mutate({
            mutation: CHANGE_PASSWORD,
            variables: {
                input: {
                    old_password: oldPassword,
                    password: newPassword,
                    password_confirmation: newPasswordConfirmed
                }
            }
        }).then(data => {
            return data['data']['updatePassword']['status'] === 'PASSWORD_UPDATED'
        }).catch(err => {
            return false
        })
    },
}
