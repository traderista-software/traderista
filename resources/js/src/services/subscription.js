import {apolloClient} from "../config/apollo";
import CREATE_SUBSCRIPTION from '../graphql/mutations/createSubscription.graphql';
import DELETE_SUBSCRIPTION from '../graphql/mutations/deleteSubscription.graphql';
import {Auth} from "../config/auth";
import router from '../router'
import {areTheSame} from "../Utils/utils";
import Store from "../store";

export const SubscriptionService = {

    async createSubscription(id, type, subscriptionId, cancelOptions, dualPackageState) {
        if (Auth.isUserAuthed()) {
            return await apolloClient.mutate({
                mutation: CREATE_SUBSCRIPTION,
                variables: {
                    input: {
                        id: id,
                        type: type,
                        subscription_id: subscriptionId,
                    }
                }
            }).then(async (data) => {
                var commit = true;
                if (dualPackageState !== null) {

                    var packagesToBeBought = dualPackageState.packagesToBeBought;
                    var packagesBought = dualPackageState.packagesBought;
                    packagesBought.push(id);

                    if (!areTheSame(packagesBought, packagesToBeBought)) {
                        commit = false;

                        await Store.dispatch('Setting/dualPackageStateAction', {
                            packagesToBeBought: packagesToBeBought,
                            packagesBought: packagesBought
                        });
                    }
                }

                if (commit) {
                    Auth.setUser(data['data']['createUserSubscription']);
                    if (cancelOptions !== null && cancelOptions !== undefined && cancelOptions.cancel) {
                        this.deleteSubscription(cancelOptions.currentSubId).then((success) => {
                            return true;
                        });
                    }
                }
                return true
            }).catch(err => {
                console.log(err)
                return false
            })
        } else {
            router.push({name: 'auth1.sign-in1'}).then()
        }
    },
    async deleteSubscription(id) {
        return await apolloClient.mutate({
            mutation: DELETE_SUBSCRIPTION,
            variables: {
                id: id
            }
        }).then(async (data) => {
            Auth.setUser(data['data']['deleteUserSubscription'])
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },
}
