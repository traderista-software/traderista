import {apolloClient} from "../config/apollo";
import SUBSCRIBE_TO_PRODUCT_LAUNCH from "../graphql/mutations/updateProductLaunchFunnelSubscription.graphql"
import {Auth} from "../config/auth";
import GET_FUNNEL_VIDEOS from "../graphql/queries/getProductFunnelVideos.graphql"

export const FunnelService = {

    async subscribeToProductFunnel() {
        return await apolloClient.mutate({
            mutation: SUBSCRIBE_TO_PRODUCT_LAUNCH,
            variables: {
                input: {
                    user_id: parseInt(Auth.id()),
                    product_video_id: 1
                }
            }
        }).then(async (data) => {
            return data.data.addProductVideo;
        }).catch(err => {
            console.log(err);
        })
    },

    async getFunnelVideos() {
        return await apolloClient.query({
            query: GET_FUNNEL_VIDEOS,
        }).then(async (data) => {
            return data.data.productFunnelVideos;
        }).catch(err => {
            return []
        })
    }
}
