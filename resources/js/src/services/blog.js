import {apolloClient} from "../config/apollo";
import ADD_COMMENT_TO_BLOG from "../graphql/mutations/addCommentToBlogPost.graphql"
import Store from "../store";
import {Auth} from "../config/auth";
import GET_BLOG_ARTICLE from "../graphql/queries/getBlogPost.graphql";
import GET_BLOG_POSTS from "../graphql/queries/getBlogPosts.graphql";
import moment from "moment";
import DELETE_COMMENT from "../graphql/mutations/deleteBlogPostComment.graphql";
import DELETE_BLOG_POST from "../graphql/mutations/deleteBlogPost.graphql";

export const BlogService = {

    async getBlogPosts(page) {
        return await apolloClient.query({
            query: GET_BLOG_POSTS,
            variables: {
                "page": page
            }
        }).then(async (data) => {
            return data.data.blogPosts;
        }).catch(err => {
            console.log(err)
            return false
        })
    },


    async getBlogPost(blogPostId) {
        return await apolloClient.query({
            query: GET_BLOG_ARTICLE,
            variables: {
                "id": blogPostId
            }
        }).then(async (data) => {
            Store.commit('Setting/activeBlogPostCommit', data['data']['blogPost'])
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },

    async createBlogComment(blogPostId, comment) {
        return await apolloClient.mutate({
            mutation: ADD_COMMENT_TO_BLOG,
            variables: {
                input: {
                    content: comment,
                    blogPost: {
                        connect: blogPostId,
                    },
                    user: {
                        connect: Auth.id()
                    },
                    date_time: moment(Date.now()).format("YYYY-MM-DD H:mm:ss"),
                }
            }
        }).then(async (data) => {
            let response = data['data']['addCommentToBlogPost'];
            response['blog_post_id'] = blogPostId;
            Store.commit('Setting/activeBlogPostCommentCommit', response)
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },


    async deleteBlogPostComment(id) {
        return await apolloClient.mutate({
            mutation: DELETE_COMMENT,
            variables: {
                id: id,
            }
        }).then(async (data) => {
            Store.commit('Setting/deleteBlogPostCommentCommit', {
                'commentId': id,
            })
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },
    async deleteBlogPost(id) {
        return await apolloClient.mutate({
            mutation: DELETE_BLOG_POST,
            variables: {
                id: id
            }
        }).then(async (data) => {
            return true
        }).catch(err => {
            console.log(err)
            return false
        })
    },


}
