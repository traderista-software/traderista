import {apolloClient} from "../config/apollo";
import GET_LEADS from '../graphql/queries/getLeads.graphql';

export const LeadService = {

    async loadLeads(currentLeads, page) {
        return await apolloClient.query({
            query: GET_LEADS,
            variables: {
                page: page
            }
        }).then(async (data) => {
            let result = []
            result['data'] = [...currentLeads, ...data['data']['leads']['data']]
            result['lastPage'] = data['data']['leads']['paginatorInfo']['lastPage']
            return result
        }).catch(err => {
            console.log(err)
            return []
        })
    },
}
