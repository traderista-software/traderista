export function capitalizeFirstLetter(string) {
    if (string === undefined || string === null) {
        return null
    }
    return string.charAt(0).toUpperCase() + string.slice(1)
}

export function toStringWithDecimalization(price, hasCurrency = true) {
    let i;
    let decimalUnits = 2;
    if (price == null) {
        return '0' + ' €';
    }

    let s = price.toString();
    if (decimalUnits >= s.length) {
        for (i = 0;
             i < decimalUnits - s.length;
             i++) {
            s = '0' + s;
        }
        return '0.' + s + ' €';
    }

    let res = '';
    for (i = 0; i < s.length; i++) {
        if (i === s.length - decimalUnits) {
            res += '.';
        }
        res += s[i];
    }
    return res + (hasCurrency ? ' €' : '');
}

export function toStringWithoutDecimalization(price, hasCurrency = true) {
    return toStringWithDecimalization(price).split('.')[0] + (hasCurrency ? ' €' : '');
}

export function areTheSame(packagesTobeBought, packagesBought) {
    return packagesTobeBought.sort().toString() === packagesBought.sort().toString();
}

export function toBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}
