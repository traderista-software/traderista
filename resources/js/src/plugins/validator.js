import * as rules from 'vee-validate/dist/rules'
import { extend, ValidationObserver, ValidationProvider } from 'vee-validate'
import { setInteractionMode } from 'vee-validate'
import Vue from 'vue'
import i18n from "../i18n";

Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule])
})

Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)

extend('min', {
    validate(value, args) {
        return value.length >= args.length;
    },
    params: ['length'],
    message: (field, param) => i18n.t('validation.passwordValidation', [field, param['length']])
})

extend('required', {
    message: (field, param) => i18n.t('validation.required', [field])
})

extend('email', {
    message: (field, param) => i18n.t('validation.email')
})

setInteractionMode('passive')
