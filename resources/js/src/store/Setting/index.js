import getters from './getters'
import actions from './actions'
import mutations from './mutations'
import GlobalSearchUser from '../../FackApi/json/GlobalSearch'
import GlobalSearchAdmin from '../../FackApi/json/GlobalSearchAdmin'
import {Auth} from "../../config/auth";

let userBookmarks = [
    {
        title: 'Home',
        link: {name: 'dashboard.home-1'},
        is_icon_class: true,
        icon: 'ri-home-4-line'
    },
    {
        title: 'Posts',
        link: {name: 'app.posts'},
        is_icon_class: true,
        icon: 'ri-question-answer-line'
    },
    {
        title: 'Referrals',
        link: {name: 'app.referrals'},
        is_icon_class: true,
        icon: 'ri-terminal-window-line'
    },
    {
        title: 'Packages',
        link: {name: 'app.packages'},
        is_icon_class: true,
        icon: 'ri-profile-line'
    },
    {
        title: 'Profile',
        link: {name: 'app.profile'},
        is_icon_class: true,
        icon: 'ri-file-user-line'
    },
];

let adminBookmarks = [
    {
        title: "Posts",
        is_icon_class: true,
        icon: "ri-question-answer-line",
        link: {
            "name": "admin.posts"
        }
    },
    {
        title: "Subscribers",
        is_icon_class: true,
        icon: "ri-file-list-line",
        link: {
            "name": "admin.subscribers"
        }
    },
    {
        title: "Customers",
        is_icon_class: true,
        icon: "ri-profile-line",
        link: {
            "name": "admin.customers"
        }
    },
    {
        title: "CashOutRequests",
        is_icon_class: true,
        icon: "ri-price-tag-line",
        link: {
            "name": "admin.cash-out-requests"
        }
    },
    {
        title: "AddBlogPost",
        is_icon_class: true,
        icon: "ri-edit-line",
        link: {
            "name": "admin.new-blog-post"
        }
    }
]

const state = {
    horizontalMenu: false,
    miniSidebarMenu: false,
    lang: {title: 'Deutsch', value: 'de', image: require('../../assets/images/small/flag-07.png')},
    langOption: [
        {title: 'Deutsch', value: 'de', image: require('../../assets/images/small/flag-07.png')},
        {title: 'English', value: 'en', image: require('../../assets/images/small/flag-01.png')}
    ],
    colors: [
        {primary: '#DAA520', primaryLight: '#edeac7', bodyBgLight: '#efeefd', bodyBgDark: '#1d203f'},
        {primary: '#e07af3', primaryLight: '#f37ab7', bodyBgLight: '#f7eefd', bodyBgDark: '#1d203f'},
        {primary: '#6475c7', primaryLight: '#7abbf3', bodyBgLight: '#eaf5ff', bodyBgDark: '#1d203f'},
        {primary: '#c76464', primaryLight: '#DAA520', bodyBgLight: '#fff8ea', bodyBgDark: '#1d203f'},
        {primary: '#c764ad', primaryLight: '#de8ba9', bodyBgLight: '#ffeaf5', bodyBgDark: '#1d203f'},
        {primary: '#64c7ac', primaryLight: '#a3f37a', bodyBgLight: '#f0ffea', bodyBgDark: '#1d203f'},
        {primary: '#8ac764', primaryLight: '#dbf37a', bodyBgLight: '#f7ffea', bodyBgDark: '#1d203f'}
    ],
    authUser: null,
    referralCode: null,
    posts: [],
    activeBlogPost: null,
    globalSearch: Auth.isAdminAuthed() ? GlobalSearchAdmin : GlobalSearchUser,
    bookmark: Auth.isAdminAuthed() ? adminBookmarks : userBookmarks,
    activePage: {
        name: 'Dashboard',
        breadCrumb: [
            {
                html: '<i class="ri-home-4-line mr-1 float-left"></i>Home',
                to: {name: 'dashboard.home-1'}
            },
            {
                text: '',
                href: '#'
            }
        ]
    },
    dualPackageState: null,
    layoutMode: {
        dark: false,
        rtl: false
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
