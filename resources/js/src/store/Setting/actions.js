export default {
    horizontalMenuAction(context, payload) {
        if (context.state.horizontalMenu) {
            context.commit('horizontalMenuCommit', false)
        } else {
            context.commit('horizontalMenuCommit', true)
        }
    },
    miniSidebarAction(context, payload) {
        return new Promise((resolve, reject) => {
            if (context.state.miniSidebarMenu) {
                context.commit('miniSidebarCommit', false)
                resolve(false)
            } else {
                context.commit('miniSidebarCommit', true)
                resolve(true)
            }
        })
    },

    resetStateOnLogout(context) {
        context.commit('resetStateCommit');
    },
    authUserAction(context, payload) {
        context.commit('authUserCommit', payload)
    },
    dualPackageStateAction(context, payload) {
        context.commit('dualPackageState', payload);
    },
    referralCodeAction(context, payload) {
        context.commit('referralCodeCommit', payload)
    },
    postsAction(context, payload) {
        context.commit('postsCommit', payload)
    },
    postAction(context, payload) {
        context.commit('postCommit', payload)
    },
    pinPostAction(context, payload) {
        context.commit('pinPostCommit', payload)
    },
    completeTradeAction(context, payload) {
        context.commit('completeTradeCommit', payload)
    },
    postCommentAction(context, payload) {
        context.commit('postCommentCommit', payload)
    },

    activeBlogPostAction(context, payload) {
        context.commit('activeBlogPostCommit', payload)
    },

    activeBlogPostCommentAction(context, payload) {
        context.commit('activeBlogPostCommentCommit', payload)
    },

    activePageAction(context, payload) {
        context.commit('activePageCommit', payload)
    },
    addBookmarkAction(context, payload) {
        context.commit('addBookmarkCommit', payload)
    },
    removeBookmarkAction(context, payload) {
        context.commit('removeBookmarkCommit', payload)
    },
    setLangAction(context, payload) {
        context.commit('setLangCommit', payload)
    },
    layoutModeAction(context, payload) {
        if (payload.dark) {
            document.querySelector('body').classList.add('dark')
        } else {
            document.querySelector('body').classList.remove('dark')
        }

        document.getElementsByTagName('html')[0].setAttribute('dir', payload.rtl ? 'rtl' : 'ltr')
        context.commit('layoutModeCommit', {
            dark: payload.dark,
            rtl: payload.rtl
        })
    }
}
