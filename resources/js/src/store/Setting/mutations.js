export default {
    horizontalMenuCommit(state, data) {
        state.horizontalMenu = data
    },
    miniSidebarCommit(state, data) {
        state.miniSidebarMenu = data
    },
    authUserCommit(state, data) {
        state.authUser = data
    },
    dualPackageState(state, data) {
        state.dualPackageState = data;
    },
    referralCodeCommit(state, data) {
        state.referralCode = data
    },
    postsCommit(state, data) {
        state.posts = data
    },
    pushPostsCommit(state, data) {
        for (let i = 0; i < data.length; i++) {
            let flag = false
            for (let j = 0; j < state.posts.length; j++) {
                if (Number(state.posts[j].id) === Number(data[i].id)) {
                    flag = true
                    break
                }
            }
            if (!flag) {
                state.posts.push(data[i])
            }
        }
    },
    postCommit(state, data) {
        const i = state.posts.indexOf((el) => el.id === data.id)
        if (i > -1) {
            state.posts[i] = data
        }
    },
    postCommentCommit(state, data) {
        for (let i = 0; i < state.posts.length; i++) {
            if (Number(state.posts[i].id) === Number(data['post_id'])) {
                state.posts[i].comments.push(data)
                break;
            }
        }
    },
    deleteCommentCommit(state, data) {
        for (let i = 0; i < state.posts.length; i++) {
            if (Number(state.posts[i].id) === Number(data['postId'])) {
                for (let j = 0; j < state.posts[i].comments.length; j++) {
                    if (Number(state.posts[i].comments[j].id) === Number(data['commentId'])) {
                        state.posts[i].comments.splice(j, 1)
                        break
                    }
                }
            }
        }
    },

    activeBlogPostCommit(state, data) {
        state.activeBlogPost = data;
    },

    activeBlogPostCommentCommit(state, data) {
        state.activeBlogPost.blogComments.push(data);
    },

    deleteBlogPostCommentCommit(state, data) {
        for (let j = 0; j < state.activeBlogPost.blogComments.length; j++) {
            if (Number(state.activeBlogPost.blogComments[j].id) === Number(data['commentId'])) {
                state.activeBlogPost.blogComments.splice(j, 1)
                break
            }
        }
    },

    activePageCommit(state, data) {
        state.activePage = data
    },
    addBookmarkCommit(state, data) {
        state.bookmark.push(data)
    },
    removeBookmarkCommit(state, data) {
        const book = state.bookmark.findIndex(item => item.link.name === data.link.name)
        if (book !== -1) {
            state.bookmark.splice(book, 1)
        }
    },
    setLangCommit(state, data) {
        state.lang = data
    },
    layoutModeCommit(state, data) {
        state.layoutMode = data
    },
    pinPostCommit(state, data) {
        for (let i = 0; i < state.posts.length; i++) {
            if (Number(state.posts[i].id) === Number(data['id'])) {
                state.posts[i].isPinned = data['isPinned'];
                break;
            }
        }
    },
    completeTradeCommit(state, data) {
        for (let i = 0; i < state.posts.length; i++) {
            if (Number(state.posts[i].id) === Number(data['id'])) {
                state.posts[i].user_trades.push({user_id: data['user_id'], cTimestamp: data['cTimestamp']});
                break;
            }
        }
    },
    resetStateCommit(state) {
        state.posts = [];
        state.referralCode = null;
        state.authUser = null;
    }
}
