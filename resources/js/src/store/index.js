import Vue from 'vue'
import Vuex from 'vuex'
import Setting from './Setting/index'
import {Auth} from "../config/auth";
import {UserService} from "../services/user";

Vue.use(Vuex)

const debug = process.env.APP_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        Setting
    },
    state: {},
    mutations: {},
    actions: {
        init({commit, getters}) {
            if (Auth.isAuthed() && getters['Setting/authUserState'] === null) {
                if (Auth.user() !== undefined && Auth.user() != null) {
                    commit('Setting/authUserCommit', Auth.user())
                }
                UserService.getProfile().then(r => {
                })
            }
        }
    },
    getters: {},
    strict: debug
})
