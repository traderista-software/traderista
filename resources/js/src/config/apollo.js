import Vue from 'vue'
import VueApollo from 'vue-apollo'
import {createApolloClient, restartWebsockets} from 'vue-cli-plugin-apollo/graphql-client'
import {Auth, AUTH_TOKEN} from "./auth"
import {TokenRefreshLink} from "apollo-link-token-refresh"
import {ApolloLink, fromPromise} from "apollo-link"
import {onError} from "apollo-link-error"
import {AuthService} from "../services/auth"

// Install the vue plugin
Vue.use(VueApollo)

// Http endpoint
const httpEndpoint = process.env.MIX_VUE_APP_GRAPHQL_HTTP

const throwServerError = function (response, result, message) {
    const error = new Error(message);
    error.response = response;
    error.statusCode = response.status;
    error.result = result;
    throw error;
};

const refreshExpiredTokenLink = new TokenRefreshLink({
    isTokenValidOrUndefined: () => {
        return Auth.isTokenValid()
    },
    fetchAccessToken: () => {
        // TODO should be read from env file
        const body = {
            "grant_type": "refresh_token",
            "client_id": process.env.MIX_API_CLIENT_ID,
            "client_secret": process.env.MIX_API_CLIENT_SECRET,
            "refresh_token": Auth.refreshToken()
        }

        const encodedBody = Object
            .keys(body)
            .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(body[key]))
            .join('&');

        return fetch(process.env.MIX_API_ENDPOINT, {
            method: 'POST',
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: encodedBody
        })
    },
    accessTokenField: 'access_token',
    handleFetch: response => {
    },
    handleResponse: (operation, accessTokenField) => response => {
        return response
            .text()
            .then(function (bodyText) {
                if (typeof bodyText !== 'string' || !bodyText.length) {
                    return bodyText || '';
                }
                try {
                    return JSON.parse(bodyText);
                } catch (err) {
                    var parseError = err;
                    parseError.response = response;
                    parseError.statusCode = response.status;
                    parseError.bodyText = bodyText;
                    return Promise.reject(parseError);
                }
            })
            .then(function (parsedBody) {
                if (response.status >= 300) {
                    throwServerError(response, parsedBody, "Response not successful: Received status code " + response.status);
                }

                Auth.authenticate(parsedBody)

                return parsedBody;
            });
    },
    handleError: err => {
        // console.log(err)
        // Auth.clear()
    }
})

const errorLink = onError(
    ({graphQLErrors, networkError, operation, forward}) => {
        if (graphQLErrors) {
            for (let err of graphQLErrors) {
                // console.log(err)
                switch (err.extensions.code) {
                    // TODO handle unauthenticated error from api
                    // case "UNAUTHENTICATED":
                    //     return fromPromise(
                    //         AuthService.refreshToken().catch((error) => {
                    //             return;
                    //         })
                    //     )
                    //         .filter((value) => Boolean(value))
                    //         .flatMap((accessToken) => {
                    //             const oldHeaders = operation.getContext().headers;
                    //             // modify the operation context with a new token
                    //             operation.setContext({
                    //                 headers: {
                    //                     ...oldHeaders,
                    //                     authorization: `Bearer ${accessToken}`,
                    //                 },
                    //             });
                    //
                    //             // retry the request, returning the new observable
                    //             return forward(operation);
                    //         });
                }
            }
        }
    }
)

// Config
const defaultOptions = {
    httpEndpoint,
    wsEndpoint: null,
    tokenName: AUTH_TOKEN,
    persisting: false,
    websocketsOnly: false,
    ssr: false,
    getAuth: () => {
        if (Auth.isAuthed()) {
            return `Bearer ${Auth.accessToken()}`
        } else if(Auth.guestToken()) {
            return Auth.guestToken()
        }
    },
    link: ApolloLink.from([
        refreshExpiredTokenLink,
        errorLink
    ])
}

export const {apolloClient, wsClient} = createApolloClient({
    ...defaultOptions,
    ...{},
})

export function createProvider() {
    return new VueApollo({
        defaultClient: apolloClient,
        defaultOptions: {
            $query: {
                fetchPolicy: 'cache-and-network',
            },
        },
        errorHandler(error) {
            // console.log(error);
            // console.log('%cError', 'background: red; color: white; padding: 2px 4px; border-radius: 3px; font-weight: bold;', error.message)
        },
    })
}

export async function onLogin(apolloClient) {
    if (apolloClient.wsClient) restartWebsockets(apolloClient.wsClient)
    try {
        await apolloClient.resetStore()
    } catch (e) {
        // console.log('%cError on cache reset (login)', 'color: orange;', e.message)
    }
}

export async function onLogout(apolloClient) {
    if (apolloClient.wsClient) restartWebsockets(apolloClient.wsClient)
    try {
        await apolloClient.resetStore()
    } catch (e) {
        // console.log('%cError on cache reset (logout)', 'color: orange;', e.message)
    }
}
