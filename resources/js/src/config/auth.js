import jwt_decode from "jwt-decode";
import Store from "../store";
import {AuthService} from "../services/auth";
import {UserService} from "../services/user";

export const AUTH_TOKEN = 'access_token'

export const Auth = {
    // role constants and functions
    Role: {
        Guest: 'guest',
        User: 'user',
        Admin: 'admin',
    },

    isUserAuthed() {
        return this.isAuthed() && this.currentRole() === this.Role.User
    },
    isAdminAuthed() {
        return this.isAuthed() && this.currentRole() === this.Role.Admin
    },
    isAuthed() {
        return this.accessToken() !== undefined && this.accessToken() !== null &&
            this.refreshToken() !== undefined && this.refreshToken() !== null;
    },
    authenticate(authPayload) {
        this.setRefreshToken(authPayload['refresh_token'])

        let accessToken = authPayload['access_token']
        this.setAccessToken(accessToken)

        let decodedAccessToken = jwt_decode(accessToken)
        this.setExpiry(decodedAccessToken['exp'])

        this.setRole(decodedAccessToken['scopes'][0])

        this.setUser(authPayload['user'])

        this.deleteGuestToken()
    },
    clear() {
        this.deleteRefreshToken()
        this.deleteAccessToken()
        this.deleteRole()
        this.deleteExpiry()
        this.deleteUser()
        this.deleteRegisteringEmail()
        this.deleteGuestToken()
        this.deleteSurveyObjects()
    },

    setRole(role) {
        localStorage.setItem('role', role)
    },
    currentRole() {
        return localStorage.getItem('role')
    },
    deleteRole() {
        localStorage.removeItem('role')
    },

    // Refresh token functions
    setRefreshToken(refreshToken) {
        localStorage.setItem('refresh_token', refreshToken)
    },
    refreshToken() {
        return localStorage.getItem('refresh_token');
    },
    deleteRefreshToken() {
        localStorage.removeItem('refresh_token')
    },

    // Access token functions
    setAccessToken(accessToken) {
        localStorage.setItem('access_token', accessToken)
    },
    accessToken() {
        return localStorage.getItem('access_token');
    },
    deleteAccessToken() {
        localStorage.removeItem('access_token')
    },
    isTokenValid() {
        if (!this.isAuthed()) {
            return true
        }
        if (this.expiry) {
            const now = Date.now();
            return now - 30 * 1000 <= now + Number(this.expiry)
        }
        return false;
    },

    // Expiry time functions
    setExpiry(expiry) {
        localStorage.setItem('expires_in', expiry)
    },
    expiry: localStorage.getItem('expires_in'),
    deleteExpiry() {
        localStorage.removeItem('expires_in')
    },

    // User functions
    setUser(user) {
        if (user !== undefined && user != null) {
            Store.commit('Setting/authUserCommit', user)
            localStorage.setItem('user', JSON.stringify(user))
        }
    },
    user() {
        if (Auth.isAuthed()) {
            return JSON.parse(localStorage.getItem('user'))
        }
        return UserService.dummyUser()
    },
    id() {
        return this.user() !== undefined && this.user() !== null ? this.user()['id'] : null;
    },
    deleteUser() {
        localStorage.removeItem('user')
    },

    // Registering email functions
    setRegisteringEmail(email) {
        localStorage.setItem('registering_email', email)
    },
    registeringEmail() {
        return localStorage.getItem('registering_email')
    },
    deleteRegisteringEmail() {
        localStorage.removeItem('registering_email')
    },

    // Guest
    setGuestToken(token) {
        if (!Auth.isAuthed()) {
            localStorage.setItem('guest_token', token)
            this.setRole(Auth.Role.Guest)
        }
    },
    guestToken() {
        return localStorage.getItem('guest_token')
    },
    deleteGuestToken() {
        localStorage.removeItem('guest_token')
    },
    deleteSurveyObjects() {
        localStorage.removeItem('surveyResults');
        localStorage.removeItem('recommendedPackageId');
    }
}
