// DEBUG profile flag
export const debug = process.env.NODE_ENV !== 'production'

// PROD profile flag
export const prod = process.env.NODE_ENV === 'production'
