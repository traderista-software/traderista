import Vue from 'vue'
import VideoEmbed from "v-video-embed/src";
import '@babel/polyfill'
import 'mutationobserver-shim'
import './Utils/fliter'
import App from './App.vue'
import router from './router'
import store from './store'
import Raphael from 'raphael/raphael'
import './plugins'
import AlgoliaComponents from 'vue-instantsearch'
import i18n from './i18n'
import './directives'
import FileManager from 'laravel-file-manager'
import {createProvider} from "./config/apollo"
import VueClipboard from 'vue-clipboard2'
import VueGtag from "vue-gtag";
import VueCookieAcceptDecline from 'vue-cookie-accept-decline';

global.Raphael = Raphael
Vue.component('vue-cookie-accept-decline', VueCookieAcceptDecline)

Vue.use(AlgoliaComponents)
Vue.use(VideoEmbed)
Vue.use(FileManager, {store})
Vue.use(VueClipboard)

Vue.config.productionTip = false

Vue.use(VueGtag,
    {
        appName: "Traderista Trading",
        config: {id: "G-YX6T43C90L"},
        // bootstrap: false,
    }, router);

let vm = new Vue({
    router,
    store,
    i18n,
    apolloProvider: createProvider(),
    render: h => h(App),
}).$mount('#app')

window.vm = vm
