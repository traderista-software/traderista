import Vue from 'vue'
import VueRouter from 'vue-router'
import {Auth} from "../config/auth"
import Store from '../store/index'

// Website static pages
const LandingPage = () => import('../views/Website/LandingPage.vue')
const AlternativeLandingPage = () => import('../views/Website/AlternativeLandingPage.vue')
const PricingPage = () => import('../views/Website/PricingPage.vue')
const PartnerPage = () => import('../views/Website/PartnerPage.vue')
const AgbPage = () => import('../views/Website/AgbPage.vue')
const CookiePolicy = () => import('../views/Website/CookiePolicy.vue')
const MissionPage = () => import('../views/Website/MissionPage.vue')
const ContactPage = () => import ('../views/Website/ContactPage')
const ImpressumPage = () => import ('../views/Website/ImpressumPage')
const WebinarPage = () => import('../views/Website/WebinarPage')
const WebinarPage2 = () => import('../views/Website/WebinarPage2')
const EmailWhiteListPage = () => import('../views/Website/EmailWhitelistPage')
const TradingStrategyPage = () => import('../views/Website/TradingStrategyPage')

// Blog
const BlogPageList = () => import ('../views/Website/BlogPageList')
const BlogArticlePage = () => import ('../views/Website/BlogArticlePage')

// Survey
const SurveyFunnelPage = () => import ('../views/Website/SurveyFunnelPage')
const LeadGeneratorPage = () => import('../views/Website/LeadGeneratorPage.vue')
const OptInMailPage = () => import('../views/Website/OptInMailPage')
const SubscriptionOptInMailPage = () => import('../views/Website/SubscriptionOptInMailPage')
const EmailOptInSuccess = () => import('../views/Website/EmailOptInSuccess')


// exit popup

const ExitPopupConversionPage = () => import('../views/Website/ExitPopupConversionPage')

// Funnel results
const FunnelResultPage = () => import ('../views/Website/FunnelResultPage')
const CartFunnelPage = () => import ('../views/Website/CartFunnelPage')
const ProductFunnelPage = () => import ('../views/Website/ProductFunnelPage')
const ApplicationFunnelPage = () => import ('../views/Website/ApplicationFunnelPage')
const EbookFunnelPage = () => import('../views/Website/EbookFunnelPage')
const WebinarOfferPage = () => import('../views/Website/WebinarOfferPage')
const EbookDownloadPage = () => import('../views/Website/EbookDownloadPage')
const EbookExtendedDownloadPage = () => import('../views/Website/EbookExtendedDownloadPage')
const WebinarFunnel = () => import('../views/Website/WebinarFunnel')
const WebinarSuccessRegistration = () => import('../views/Website/WebinarSuccessRegistration')
const StarterPackageOffer = () => import('../views/Website/StarterPackageOffer')
const VipPlusOffer = () => import('../views/Website/VipPlusPackagePage')
const GrundKursPage = () => import('../views/Website/GrundkursPage')
const TradersCoachingPage = () => import('../views/Website/TradersCoachingPage')

// Thank you
const ThankYouPage = () => import('../views/Website/ThankYouPage.vue')
const UnsubscribeConfirmation = () => import('../views/Website/UnsubscribeConfirmation')

// User App
const UserHome = () => import('../views/UserApp/UserHome')
const UserPosts = () => import('../views/UserApp/UserPosts')
const ReferralsPage = () => import('../views/UserApp/ReferralsPage')
const CoachingPage = () => import('../views/UserApp/CoachingPage')
const UserPackagesPage = () => import('../views/UserApp/UserPackagesPage')
const ProfileEdit = () => import('../views/UserApp/ProfileEditPage')
const UserVideoPage = () => import('../views/UserApp/UserVideoPage')

//Admin App
const SubscribersPage = () => import('../views/AdminApp/SubscribersPage')
const UsersPage = () => import('../views/AdminApp/UsersPage')
const UserInfoPage = () => import('../views/AdminApp/UserInfoPage')
const CashOutRequestsPage = () => import('../views/AdminApp/CashOutRequestsPage')
const AdminPosts = () => import('../views/AdminApp/AdminPosts')
const CompaniesPage = () => import('../views/AdminApp/CompaniesPage')
const AddBlogPostPage = () => import("../views/AdminApp/AddBlogPostPage")
const MailCampaignPage = () => import("../views/AdminApp/MailCampaignPage")
const CoachingCampaignPage = () => import("../views/AdminApp/CoachingCampaignPage")
const RewardsPage = () => import("../views/AdminApp/RewardsPage")
const AdminPackagesPage = () => import("../views/AdminApp/PackagesPage")
const BlogListAdmin = () => import("../views/AdminApp/BlogListAdmin")
const BannersPage = () => import("../views/AdminApp/BannersPage")

/* Layouts */
const Layout2 = () => import('../layouts/Layout2.vue')

// Auth user
const AuthLayout = () => import('../layouts/AuthLayouts/AuthLayout')
const SignIn1 = () => import('../views/AuthPages/Default/SignIn1')
const SignUp1 = () => import('../views/AuthPages/Default/SignUp1')
const SignInAdmin = () => import('../views/AuthPages/Default/SignInAdmin')
const RecoverPassword1 = () => import('../views/AuthPages/Default/RecoverPassword1')
const LockScreen1 = () => import('../views/AuthPages/Default/LockScreen1')
const ConfirmMail1 = () => import('../views/AuthPages/Default/ConfirmMail1')
const Callback = () => import('../views/AuthPages/Default/Callback')
const EmailVerification = () => import('../views/AuthPages/Default/EmailVerification')
const NewPassword = () => import('../views/AuthPages/Default/NewPassword')

const ErrorPage = () => import('../views/Website/ErrorPage')

Vue.use(VueRouter)

const adminChildRoute = (prop) => [
    {
        path: 'subscribers',
        name: prop + '.subscribers',
        meta: {
            auth: true, name: 'Subscribers', authorize: [Auth.Role.Admin]
        },
        component: SubscribersPage
    },
    {
        path: 'customers',
        name: prop + '.customers',
        meta: {auth: true, name: 'UsersPage', authorize: [Auth.Role.Admin]},
        component: UsersPage
    },
    {
        path: 'blog',
        name: prop + '.blog',
        meta: {auth: true, name: 'BlogPage', authorize: [Auth.Role.Admin]},
        component: BlogListAdmin
    },
    {
        path: 'new-blog-post',
        name: prop + '.new-blog-post',
        meta: {auth: true, name: 'New Blog Post', authorize: [Auth.Role.Admin]},
        component: AddBlogPostPage
    },
    {
        path: 'posts',
        name: prop + '.posts',
        meta: {auth: true, name: 'Posts', authorize: [Auth.Role.Admin]},
        component: AdminPosts
    },
    // {
    //     path: 'new-post',
    //     name: prop + '.new-post',
    //     meta: {auth: true,name: 'New Post'},
    //     component: AddPostPage
    // },
    {
        path: 'customers/:id',
        name: prop + '.customer',
        meta: {auth: true, name: 'UserInfoPage', authorize: [Auth.Role.Admin]},
        component: UserInfoPage,
        props: route => ({default: true, id: route.params.id})
    },
    {
        path: 'cash-out-requests',
        name: prop + '.cash-out-requests',
        meta: {auth: true, name: 'CashOutRequestsPage', authorize: [Auth.Role.Admin]},
        component: CashOutRequestsPage,
    },
    {
        path: 'companies',
        name: prop + '.companies',
        meta: {auth: true, name: 'Companies', authorize: [Auth.Role.Admin]},
        component: CompaniesPage,
    },
    {
        path: 'mail-campaign',
        name: prop + '.mail-campaign',
        meta: {auth: true, name: 'MailCampaign', authorize: [Auth.Role.Admin]},
        component: MailCampaignPage,
    },
    {
        path: 'coaching',
        name: prop + '.coaching-campaign',
        meta: {auth: true, name: 'CoachingCampaign', authorize: [Auth.Role.Admin]},
        component: CoachingCampaignPage,
    },
    {
        path: 'rewards',
        name: prop + '.rewards',
        meta: {auth: true, name: 'RewardsPage', authorize: [Auth.Role.Admin]},
        component: RewardsPage,
    },
    {
        path: 'packages',
        name: prop + '.packages',
        meta: {auth: true, name: 'Packages', authorize: [Auth.Role.Admin]},
        component: AdminPackagesPage,
    },
    {
        path: 'banners',
        name: prop + '.banners',
        meta: {auth: true, name: 'Banners', authorize: [Auth.Role.Admin]},
        component: BannersPage
    }

]

const appChildRoute = (prop) => [
    {
        path: 'home',
        name: 'dashboard.home-1',
        meta: {auth: true, name: 'Home 1', authorize: [Auth.Role.User, Auth.Role.Guest]},
        component: UserHome
    },
    {
        path: 'posts',
        name: prop + '.posts',
        meta: {auth: true, name: 'Posts', authorize: [Auth.Role.User, Auth.Role.Guest]},
        component: UserPosts
    },
    {
        path: 'packages',
        name: prop + '.packages',
        meta: {auth: true, name: 'Packages', authorize: [Auth.Role.User, Auth.Role.Guest]},
        props: (route) => ({fromRegister: route.query.fromRegister}),
        component: UserPackagesPage
    },
    {
        path: 'profile',
        name: prop + '.profile',
        meta: {auth: true, name: 'Profile', authorize: [Auth.Role.User, Auth.Role.Guest]},
        component: ProfileEdit
    },
    {
        path: 'coaching',
        name: prop + '.coaching',
        meta: {auth: true, name: 'CoachingCampaign', authorize: [Auth.Role.User]},
        component: CoachingPage,
    },
    {
        path: 'referrals',
        name: prop + '.referrals',
        meta: {auth: true, name: 'Referrals Page', authorize: [Auth.Role.User, Auth.Role.Guest]},
        component: ReferralsPage
    },
    {
        path: 'video',
        name: prop + '.video',
        meta: {auth: true, name: 'Video Page', authorize: [Auth.Role.User, Auth.Role.Guest]},
        component: UserVideoPage
    },
    {
        path: '/app/blog',
        name: prop + '.blog-page',
        meta: {
            auth: false, name: 'Blog',
            hasNav: false,
            authorize: [Auth.Role.User, Auth.Role.Guest]
        },
        component: BlogPageList
    },
    {
        path: '/app/blog/:id',
        name: prop + '.blog-article-page',
        meta: {
            auth: false, name: 'BlogArticle',
            hasNav: false,
            authorize: [Auth.Role.User, Auth.Role.Guest]
        },
        component: BlogArticlePage
    },
    {
        path: '/app/workshop',
        name: prop + '.free-workshop',
        meta: {
            auth: true, name: 'Free Workshop',
            hasNav: false,
            authorize: [Auth.Role.User, Auth.Role.Guest]
        },
        component: ProductFunnelPage
    },

]

const authChildRoutes = (prop) => [
    {
        path: 'sign-in',
        name: prop + '.sign-in1',
        meta: {auth: true},
        component: SignIn1,
        props: true,
    },
    {
        path: 'sign-up',
        name: prop + '.sign-up1',
        meta: {auth: true},
        component: SignUp1
    },
    {
        path: 'forgot-password',
        name: prop + '.forgot-password',
        meta: {auth: true},
        component: RecoverPassword1
    },
    {
        path: 'reset-password',
        name: prop + 'reset-password',
        meta: {auth: true},
        component: NewPassword
    },
    {
        path: 'lock-screen',
        name: prop + '.lock-screen1',
        meta: {auth: true},
        component: LockScreen1
    },
    {
        path: 'confirm-mail',
        name: prop + '.confirm-mail1',
        meta: {auth: true},
        component: ConfirmMail1
    },
    {
        path: 'admin/sign-in',
        name: prop + '.admin-sign-in',
        meta: {auth: false},
        component: SignInAdmin
    },
    {
        path: 'email-verify',
        name: prop + '.email-verify',
        meta: {auth: true},
        component: EmailVerification
    }
]

const routes = [
    {
        path: '/',
        name: 'landing-page',
        meta: {
            auth: false,
            hasNav: true
        },
        component: TradingStrategyPage
    },
    {
        path: '/trading-strategy',
        name: 'trading-strategy',
        meta: {
            auth: false,
            hasNav: true
        },
        component: TradingStrategyPage
    },
    {
        path: '/traders',
        name: 'traders',
        meta: {
            auth: false,
            hasNav: true
        },
        component: LandingPage
    },
    {
        path: '/landing-page-new',
        name: 'alternative-landing-page',
        meta: {
            auth: false,
            hasNav: true
        },
        component: LandingPage
    },
    {
        path: '/e-book',
        name: 'e-book',
        meta: {
            auth: false,
            hasNav: false
        },
        component: EbookFunnelPage
    },
    {
        path: '/webinar-offer',
        name: 'webinar-offer',
        meta: {
            auth: false,
            hasNav: false
        },
        component: WebinarOfferPage
    },
    {
        path: '/e-book-download',
        name: 'e-book-download',
        meta: {
            auth: false,
            hasNav: false
        },
        component: EbookDownloadPage
    },
    {
        path: '/e-book-extended',
        name: 'e-book-extended-download',
        meta: {
            auth: false,
            hasNav: false
        },
        component: EbookExtendedDownloadPage
    },
    {
        path: '/webinar-free',
        name: 'webinar-free',
        meta: {
            auth: false,
            hasNav: false,
        },
        component: WebinarFunnel
    },
    {
        path: '/webinar-success',
        name: 'webinar-success',
        meta: {
            auth: false,
            hasNav: false,
        },
        component: WebinarSuccessRegistration
    },
    {
        path: '/vip-offer',
        name: 'vip-offer',
        meta: {
            auth: false,
            hasNav: false,
        },
        component: VipPlusOffer
    },
    {
        path: '/starter-offer',
        name: 'starter-offer',
        meta: {
            auth: false,
            hasNav: false,
        },
        component: StarterPackageOffer
    },
    {
        path: '/grundkurs-wahrer-wohlstand',
        name: 'grundkurs-wahrer-wohlstand',
        meta: {
            auth: false,
            hasNav: true,
        },
        component: GrundKursPage
    },
    {
        path: '/traders-most-efficient-coaching',
        name: 'traders-most-efficient-coaching',
        meta: {
            auth: false,
            hasNav: true,
        },
        component: TradersCoachingPage
    },
    {
        path: '/blog',
        name: 'blog-page',
        meta: {
            auth: false, name: 'Blog',
            hasNav: true
        },
        component: BlogPageList
    },
    {
        path: '/survey',
        name: 'survey',
        meta: {
            auth: false,
            name: 'Survey Funnel',
            hasNav: true
        },
        component: SurveyFunnelPage
    },
    {
        path: '/result',
        name: 'survey-result',
        meta: {
            auth: false,
            name: 'Funnel Result Page',
            hasNav: true
        },
        props: true,
        component: FunnelResultPage
    },
    {
        path: '/annual-discount',
        name: 'cart-funnel',
        meta: {
            auth: false,
            name: 'Cart Funnel',
            hasNav: true
        },
        props: (route) => ({packageId: route.query.packageId}),
        component: CartFunnelPage
    },
    {
        path: '/free-workshop',
        name: 'product-funnel',
        meta: {
            auth: false,
            name: 'Product Funnel',
            hasNav: true
        },
        component: ProductFunnelPage
    },
    {
        path: '/coaching-offer',
        name: 'application-funnel',
        meta: {
            auth: false,
            name: 'Application Funnel',
            hasNav: true
        },
        component: ApplicationFunnelPage
    },
    {
        path: '/blog/:id',
        name: 'blog-article-page',
        meta: {
            auth: false, name: 'BlogArticle',
            hasNav: true
        },
        component: BlogArticlePage
    },
    {
        path: '/pricing',
        name: 'pricing-page',
        meta: {
            auth: false,
            hasNav: true
        },
        component: PricingPage
    },
    {
        path: '/partner',
        name: 'partner-page',
        meta: {
            auth: false,
            hasNav: true
        },
        component: PartnerPage
    },
    {
        path: '/agb',
        name: 'agb-page',
        meta: {
            auth: false,
            hasNav: true
        },
        component: AgbPage
    },
    {
        path: '/cookie-policy',
        name: 'cookie-policy',
        meta: {
            auth: false,
            hasNav: true
        },
        component: CookiePolicy
    },
    {
        path: '/mission',
        name: 'our-mission-page',
        meta: {
            auth: false,
            hasNav: true
        },
        component: MissionPage
    },
    {
        path: '/lead-generator',
        name: 'lead-generator-page',
        meta: {
            auth: false,
            hasNav: true
        },
        props: true,
        component: LeadGeneratorPage
    },
    {
        path: '/opt-in-mail',
        name: 'opt-in-mail',
        meta: {
            auth: false,
            hasNav: true
        },
        props: true,
        component: OptInMailPage
    },
    {
        path: '/subscription-opt-in-mail',
        name: 'subscription-opt-in-mail',
        meta: {
            auth: false,
            hasNav: true
        },
        props: true,
        component: SubscriptionOptInMailPage
    },
    {
        path: '/email-opt-in/success',
        name: 'email-opt-in-success',
        meta: {
            auth: false,
            hasNav: true
        },
        props: (route) => ({token: route.query.token, type: route.query.type, redirect: route.query.redirect}),
        component: EmailOptInSuccess
    },
    {
        path: '/thank-you',
        name: 'thank-you-page',
        meta: {
            auth: false,
            hasNav: true
        },
        component: ThankYouPage
    },
    {
        path: '/contact',
        name: 'contact',
        meta: {
            auth: false,
            hasNav: true
        },
        component: ContactPage
    },
    {
        path: '/impressum',
        name: 'impressum',
        meta: {
            auth: false,
            hasNav: true
        },
        component: ImpressumPage
    },

    {
        path: '/app',
        name: 'app',
        component: Layout2,
        meta: {auth: true},
        children: appChildRoute('app'),
        props: {isAdmin: false}
    },
    {
        path: '/admin',
        name: 'admin',
        component: Layout2,
        meta: {auth: true},
        children: adminChildRoute('admin'),
        props: {isAdmin: true}
    },
    {
        path: '/auth',
        name: 'auth1',
        component: AuthLayout,
        meta: {auth: true},
        children: authChildRoutes('auth1')
    },
    // {
    //     path: '/app',
    //     name: 'app',
    //     component: Layout2,
    //     meta: {auth: true},
    //     children: appChildRoute('app')
    // },
    {
        path: '/callback',
        name: 'callback',
        meta: {auth: false},
        component: Callback
    },
    {
        path: '/webinar',
        name: 'webinar',
        meta: {
            auth: false,
            hasNav: true
        },
        component: WebinarPage
    },
    {
        path: '/webinar-2',
        name: 'webinar-2',
        meta: {
            auth: false,
            hasNav: true
        },
        component: WebinarPage2
    },
    {
        path: '/email-whitelist',
        name: 'email-whitelist',
        meta: {
            auth: false,
            hasNav: true
        },
        component: EmailWhiteListPage
    },
    // and finally the default route, when none of the above matches:
    {
        path: "*", component: ErrorPage
    },
    {
        path: '/emails/unsubscribe',
        name: 'email-unsubscribe',
        meta: {
            auth: false,
            hasNav: false,
        },
        props: (route) => ({token: route.query.token}),
        component: UnsubscribeConfirmation
    },
    {
        path: '/exit-popup-conversion',
        name: 'exit-popup-conversion',
        meta: {
            auth: false,
            hasNav: true
        },
        component: ExitPopupConversionPage
    }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.MIX_SENTRY_DSN_INDEX,
    routes
})

router.beforeEach((to, from, next) => {
    const publicPages = ['/auth/sign-in', '/auth/sign-up', '/auth/confirm-mail', '/auth/email-verify',
        '/auth/forgot-password', '/auth/reset-password', '/auth/admin/sign-in'];

    if (to.query['referral_code']) {
        Store.commit('Setting/referralCodeCommit', to.query['referral_code'])
        localStorage.setItem('referralCode', to.query['referral_code'])
    }

    if (to.query['guest_token']) {
        Auth.setGuestToken(to.query['guest_token'])
    }

    const authorize = to.meta['authorize'];
    if (authorize) {
        const currentRole = Auth.currentRole();
        if (authorize.length && !authorize.includes(currentRole)) {
            if (currentRole === Auth.Role.Admin) {
                return next({name: 'admin.posts'});
            } else if (currentRole === Auth.Role.User || currentRole === Auth.Role.Guest) {
                return next({name: 'dashboard.home-1'})
            } else {
                if (authorize.includes(Auth.Role.Admin)) {
                    return next('/auth/admin/sign-in')
                } else if (authorize.includes(Auth.Role.User)) {
                    return next('/auth/sign-in')
                }
            }
        }
    }

    return next()
})

export default router
